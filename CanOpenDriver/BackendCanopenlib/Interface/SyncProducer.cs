﻿using System;
using DatalinkEngineering.CANopen;

namespace BackendCanopenlib32.Interface
{
    public class SyncProducer : IDisposable
    {
        #region Fields

        private readonly SyncProducer_NET _SyncProducer;

        #endregion Fields

        #region Properties

        public bool Enabled { get; private set; } = false;
        public ushort Period { get; private set; } = 1000;
        public uint SyncCobID { get; private set; } = 0x80;

        #endregion Properties

        public SyncProducer()
        {
            _SyncProducer = new SyncProducer_NET();

            _SyncProducer.setSyncCOBID(SyncCobID);
            _SyncProducer.setTransmissionPeriodTime(Period);
        }

        #region Methods

        public void Configure(ushort Period, uint SyncCobID, bool Enabled)
        {
            void throwIfError(CanOpenStatus status, string text)
            {
                if (status != CanOpenStatus.CANOPEN_OK)
                {
                    throw new CanError(text, status);
                }
            }

            CanOpenStatus err;

            err = _SyncProducer.startPeriodicTransmission(false);
            throwIfError(err, "Can't disable SYNC producer");
            this.Enabled = false;

            _SyncProducer.setSyncCOBID(SyncCobID);
            throwIfError(err, "Can't set SYNC COB-ID");
            this.SyncCobID = SyncCobID;

            _SyncProducer.setTransmissionPeriodTime(Period);
            throwIfError(err, "Can't set SYNC period");
            this.Period = Period;

            if (Enabled)
            {
                _SyncProducer.startPeriodicTransmission(Enabled);
                throwIfError(err, "Can't enable SYNC producer");
                this.Enabled = Enabled;
            }
        }

        public CanOpenStatus canHardwareConnect(int connectionId, int speed)
            => _SyncProducer.canHardwareConnect(connectionId, speed);

        public void Dispose() => _SyncProducer.Dispose();

        #endregion Methods
    }
}