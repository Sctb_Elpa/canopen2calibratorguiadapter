﻿using System;
using DatalinkEngineering.CANopen;

namespace BackendCanopenlib32.Interface
{
    public class EmergencyListener
    {
        #region Delegates

        public delegate void EmergencyEventEvent(object sender, byte nodeId, 
            uint errorRegister, ushort emcyErrorCode, byte[] manufacturerSpec);

        #endregion Delegates

        #region Fields

        public event EmergencyEventEvent OnEMCY;

        #endregion Fields

        public static CanOpenStatus EMCYRessived(object obj, byte nodeId, 
            ushort emcyErrorCode, byte errorRegister, byte[] manufacturerSpecificErrorField)
        {
            if (obj is EmergencyListener _this)
            {
                _this.OnEMCY?.Invoke(_this, nodeId, errorRegister, emcyErrorCode, manufacturerSpecificErrorField);
                return CanOpenStatus.CANOPEN_OK;
            }

            return CanOpenStatus.CANOPEN_OBJECT_MISSMATCH;
        }
    }
}