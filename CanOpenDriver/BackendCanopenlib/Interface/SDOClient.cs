﻿using System;
using System.Linq;
using BackendCanopenlib32.HalperExtantions;
using DatalinkEngineering.CANopen;
using Utils.OD;

namespace BackendCanopenlib32.Interface
{
    public class SDOClient
    {
        #region Fields

        private readonly ClientSDO_NET Sdo;

        #endregion Fields

        #region Constructors

        public SDOClient(ClientSDO_NET clientSDO_NET)
        {
            Sdo = clientSDO_NET;
        }

        #endregion Constructors

        #region Methods

        public SDOClient Connect(byte NodeID)
        {
            var err = Sdo.connect(NodeID);
            if (err != CanOpenStatus.CANOPEN_OK)
            {
                throw new CanError($"Can't connect to node {NodeID}", err);
            }
            return this;
        }

        public void BlockWriteSDOIndex(ODIndex index, byte[] data)
        {
            var crc = CRC.crc16_ccit_0(data);

            var error = Sdo.objectWriteBlock(index.Index, index.Subindex, crc, data, (uint)data.Length, out _);
            if (error != CanOpenStatus.CANOPEN_OK)
            {
                throw new CanError($"Failed to write {index}", error);
            }
        }

        public byte[] ReadBytesSDOIndex(ushort index, byte subindex = 0, int max_size = 1024)
            => ReadBytesSDOIndex(new ODIndex() { Index = index, Subindex = subindex }, max_size);

        public byte[] ReadBytesSDOIndex(ODIndex index, int max_size = 1024)
        {
            byte[] val = new byte[max_size];
            var err = Sdo.objectRead(index.Index, index.Subindex, val, out uint valid, out _);
            if (err != CanOpenStatus.CANOPEN_OK || valid > val.Length)
            {
                throw new CanError($"Failed to read {index}", err);
            }
            return val.Take((int)valid).ToArray();
        }

        public T ReadSDOIndex<T>(ushort index, byte subindex) where T : struct
            => ReadSDOIndex<T>(new ODIndex() { Index = index, Subindex = subindex });

        public T ReadSDOIndex<T>(ODIndex index) where T : struct => (default(T)) switch
        {
            byte ub => (T)ReadByte(index),
            sbyte sb => (T)(object)unchecked((sbyte)(byte)ReadByte(index)),
            ushort us => (T)ReadUShort(index),
            short ss => (T)(object)unchecked((short)(ushort)ReadUShort(index)),
            uint ui => (T)ReadUInt(index),
            int si => (T)(object)unchecked((int)(uint)ReadUInt(index)),
            UInt64 ul => (T)ReadULong(index),
            Int64 sl => (T)(object)unchecked((long)(ulong)ReadULong(index)),
            bool b => (T)(object)Convert.ToBoolean(ReadByte(index)),
            float f => (T)(object)BitConverter.Int32BitsToSingle(unchecked((int)(uint)ReadUInt(index))),
            double d => (T)(object)BitConverter.Int64BitsToDouble(unchecked((long)(ulong)ReadULong(index))),

            _ => throw new CanError($"Type {typeof(T)} not supported by backend"),
        };

        public void WriuteSDOIndex<T>(ODIndex index, T v) where T : struct
        {
            switch (v)
            {
                case byte ub: WriteByte(index, ub); break;
                case sbyte sb: WriteByte(index, unchecked((byte)sb)); break;
                case ushort us: WriteUShort(index, us); break;
                case short ss: WriteUShort(index, unchecked((ushort)ss)); break;
                case uint ui: WriteUInt(index, ui); break;
                case int si: WriteUInt(index, unchecked((uint)si)); break;
                case UInt64 ul: WriteULong(index, ul); break;
                case Int64 sl: WriteULong(index, unchecked((ulong)sl)); break;
                case bool b: WriteByte(index, Convert.ToByte(b)); break;
                case float f: WriteUInt(index, unchecked((uint)BitConverter.SingleToInt32Bits(f))); break;
                case double d: WriteULong(index, unchecked((ulong)BitConverter.DoubleToInt64Bits(d))); break;

                default: throw new CanError($"Type {typeof(T)} not supported by backend");
            }
        }

        public void WriteSDOIndex<T>(ushort index, byte subindex, T v) where T : struct
            => WriuteSDOIndex(new ODIndex { Index = index, Subindex = subindex }, v);

        public void BlockWriteSDOIndex(ushort index, byte subindex, byte[] vs)
            => BlockWriteSDOIndex(new ODIndex { Index = index, Subindex = subindex }, vs);

        //---------------------------------------------------------------------------------

        private void Throw_if_error(ODIndex index, CanOpenStatus status, bool write)
        {
            string rw = write ? "write" : "read";
            if (status != CanOpenStatus.CANOPEN_OK)
            {
                throw new CanError($"Failed to {rw} {index} {status}", status);
            }
        }

        private object ReadByte(ODIndex index)
        {
            var err = Sdo.objectRead(index.Index, index.Subindex, out byte val, out _);
            Throw_if_error(index, err, false);
            return val;
        }

        private object ReadUShort(ODIndex index)
        {
            var err = Sdo.objectRead(index.Index, index.Subindex, out ushort val, out _);
            Throw_if_error(index, err, false);
            return val;
        }

        private object ReadUInt(ODIndex index)
        {
            var err = Sdo.objectRead(index.Index, index.Subindex, out uint val, out _);
            Throw_if_error(index, err, false);
            return val;
        }

        private object ReadULong(ODIndex index)
        {
            var err = Sdo.objectRead(index.Index, index.Subindex, out ulong val, out _);
            Throw_if_error(index, err, false);
            return val;
        }

        private void WriteByte(ODIndex index, byte value)
        {
            var err = Sdo.objectWrite(index.Index, index.Subindex, value, out _);
            Throw_if_error(index, err, true);
        }

        private void WriteUShort(ODIndex index, ushort value)
        {
            var err = Sdo.objectWrite(index.Index, index.Subindex, value, out _);
            Throw_if_error(index, err, true);
        }

        private void WriteUInt(ODIndex index, uint value)
        {
            var err = Sdo.objectWrite(index.Index, index.Subindex, value, out _);
            Throw_if_error(index, err, true);
        }

        private void WriteULong(ODIndex index, ulong value)
        {
            var err = Sdo.objectWrite(index.Index, index.Subindex, value, out _);
            Throw_if_error(index, err, true);
        }

        #endregion Methods
    }
}