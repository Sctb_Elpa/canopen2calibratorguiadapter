﻿using DatalinkEngineering.CANopen;
using System.IO;

namespace BackendCanopenlib32.Interface
{
	public class CanError : IOException
	{
		#region Fields

		public CanOpenStatus code { get; private set; }

		#endregion Fields

		#region Constructors

		public CanError(string text, CanOpenStatus code = CanOpenStatus.CANOPEN_ERROR) : base(text)
		{
			this.code = code;
		}

		#endregion Constructors

		#region Properties

		public override string Message => $"{base.Message} ({code})";

		#endregion Properties
	}
}