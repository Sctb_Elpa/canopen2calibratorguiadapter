﻿using System;
using DatalinkEngineering.CANopen;

namespace BackendCanopenlib32.Interface
{
    public class PDOListener : IDisposable
    {
        #region Fields

        private readonly ReceivePDO_NET client = new ReceivePDO_NET();

        private bool m_enabled = true;

        #endregion Fields

        #region Constructors

        public PDOListener(uint cobId)
        {
            client.setCobid(cobId);
        }

        #endregion Constructors

        #region Delegates

        public delegate void PDOEvent(object sender, ushort CobId, byte[] data);

        #endregion Delegates

        #region Properties

        public Action<uint, byte[]> Callback { get; set; }

        public bool Enabled
        {
            get => m_enabled; set
            {
                /*
                if (m_enabled && !value)
                {
                    client.canHardwareDisconnect();
                }
                */
                m_enabled = value;
            }
        }

        #endregion Properties

        #region Methods

        public static CanOpenStatus PDORessived(object obj, uint id, byte[] data, byte len)
        {
            if ((obj is PDOListener _this) && _this.Enabled)
            {
                _this.Callback?.Invoke(id, data);
                return CanOpenStatus.CANOPEN_OK;
            }

            return CanOpenStatus.CANOPEN_MSG_NOT_PROCESSED;
        }

        public void Dispose()
        {
            client.registerReceivePdoMessageCallBack(null, null);
            client.canHardwareDisconnect();
        }

        public CanOpenStatus TryConnect(int connectionId, int speed)
        {
            var res = client.canHardwareConnect(connectionId, speed);
            if (res == CanOpenStatus.CANOPEN_OK)
            {
                client.registerReceivePdoMessageCallBack(this, PDORessived);
            }
            return res;
        }

        public void Update(uint cobId, Action<uint, byte[]> callback)
        {
            client.setCobid(cobId);
            Callback = callback;
        }

        #endregion Methods
    }
}