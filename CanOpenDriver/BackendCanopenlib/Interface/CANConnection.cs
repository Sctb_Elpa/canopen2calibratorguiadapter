﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using DatalinkEngineering.CANopen;
using Utils;
using Utils.LSS;
using Utils.NMT;
using Utils.OD;

namespace BackendCanopenlib32.Interface
{
    public class CANConnection : IDisposable
    {
        #region Fields

        private readonly ConcurrentDictionary<object, Action<byte, NMTState>> heartbeatSubscribers
            = new ConcurrentDictionary<object, Action<byte, NMTState>>();

        private readonly ConcurrentDictionary<object, PDOListener> PDOSubscribers
            = new ConcurrentDictionary<object, PDOListener>();

        private readonly ConcurrentDictionary<object, Action<byte, uint, ushort, byte[]>> EMCYSubscribers
            = new ConcurrentDictionary<object, Action<byte, uint, ushort, byte[]>>();

        private readonly ODIndex TestNodePresentIndex = new ODIndex { Index = 0x1000 };
        private ClientSDO_NET clientSDO = null;
        private HeartbeatListener HBlistener = null;
        private LSS_Master_NET lSS_Master = null;
        private LSSFastScanScaner lSSFastScanScaner = null;
        private NMT_Master_NET NMT_Master = null;
        private SDOClient SDOClient = null;
        private EmcyServer_NET EMCYServer = null;
        private EmergencyListener EMCYListener = null;

        private uint m_Retrys = 2;
        private uint m_Timeout = 100;

        #endregion Fields

        #region Properties

        public int ConnectionId { get; internal set; } = 0;
        public bool IsConnected { get; internal set; } = false;
        public bool IsFastScanInProgress => lSSFastScanScaner.IsFastScanInProgress;
        public int Speed { get; internal set; } = 0;
        public uint Retrys
        {
            get => m_Retrys;
            set
            {
                if (value < 1) {
                    m_Retrys = 1; 
                } else { 
                    m_Retrys = value; 
                }
            }
        }

        public uint Timeout
        {
            get => m_Timeout;
            set {
                if (value < 10)
                {
                    m_Timeout = 10;
                }
                else
                {
                    m_Timeout = value;
                }
            }
        }

        public SyncProducer SyncProducer { get; private set; } = null;

        #endregion Properties

        #region Methods

        public CanOpenStatus Connect(int bus_id)
        {
            CanOpenStatus result;

            if (IsConnected)
            {
                Disconnect();
            }
            ConnectionId = bus_id;

            NMT_Master = new NMT_Master_NET();
            result = NMT_Master.canHardwareConnect(ConnectionId, Speed);
            if (result != CanOpenStatus.CANOPEN_OK)
            {
                Disconnect();
                return result;
            }

            lSS_Master = new LSS_Master_NET();
            result = lSS_Master.canHardwareConnect(ConnectionId, Speed);
            if (result != CanOpenStatus.CANOPEN_OK)
            {
                Disconnect();
                return result;
            }

            clientSDO = new ClientSDO_NET();
            result = clientSDO.canHardwareConnect(ConnectionId, Speed);
            if (result != CanOpenStatus.CANOPEN_OK)
            {
                Disconnect();
                return result;
            }

            clientSDO.setReadObjectTimeout(Timeout * 2);
            clientSDO.setWriteObjectTimeout(Timeout * 4);
            clientSDO.setNodeResponseTimeout(Timeout);

            SDOClient = new SDOClient(clientSDO);

            lSSFastScanScaner = new LSSFastScanScaner();
            result = lSSFastScanScaner.canHardwareConnect(ConnectionId, Speed);
            if (result != CanOpenStatus.CANOPEN_OK)
            {
                Disconnect();
                return result;
            }

            SyncProducer = new SyncProducer();
            result = SyncProducer.canHardwareConnect(ConnectionId, Speed);
            if (result != CanOpenStatus.CANOPEN_OK)
            {
                Disconnect();
                return result;
            }

            EMCYServer = new EmcyServer_NET();
            result = EMCYServer.canHardwareConnect(ConnectionId, Speed);
            if (result != CanOpenStatus.CANOPEN_OK)
            {
                Disconnect();
                return result;
            }

            HBlistener = new HeartbeatListener();
            NMT_Master.registerNodeStateCallback(HeartbeatListener.HeartbeatRessived, HBlistener);
            HBlistener.OnHeartbeat += OnHeartbeat;

            EMCYListener = new EmergencyListener();
            EMCYServer.registerEmcyServerMessageCallBack(EMCYListener, EmergencyListener.EMCYRessived);
            EMCYListener.OnEMCY += OnEMCY;

            IsConnected = true;

            return CanOpenStatus.CANOPEN_OK;
        }

        public void Disconnect()
        {
            if (SyncProducer != null)
            {
                SyncProducer.Dispose();
                SyncProducer = null;
            }

            if (HBlistener != null)
            {
                if (NMT_Master != null)
                {
                    NMT_Master.registerNodeStateCallback(null, null);
                }
                HBlistener = null;
            }

            if (lSS_Master != null)
            {
                lSS_Master.Dispose();
                lSS_Master = null;
            }
            if (NMT_Master != null)
            {
                NMT_Master.Dispose();
                NMT_Master = null;
            }
            if (clientSDO != null)
            {
                SDOClient = null;
                clientSDO.Dispose();
                clientSDO = null;
            }
            if (lSSFastScanScaner != null)
            {
                lSSFastScanScaner.Dispose();
                lSS_Master = null;
				lSSFastScanScaner = null;
			}
            if (EMCYServer != null)
            {
				EMCYServer.Dispose();
				EMCYServer = null;
			}

			lock (PDOSubscribers)
            {
                PDOSubscribers.Clear();
            }

			EMCYListener = null;

			IsConnected = false;
        }

        public void Dispose() => Disconnect();

        public async Task<UniqueLSSId> FindDeviceAsync(UniqueLSSId work_id, LSSFastScanScaner.ReportProgressAsync p)
            => await lSSFastScanScaner.FindDevicePartialyKnownAsync(work_id, p);

        public async Task<UniqueLSSId> FindDeviceAsync(UniqueLSSId work_id, CancellationToken InCancellationToken,
            CancellationTokenSource OutCancellationToken, LSSFastScanScaner.ReportProgress p)
            => await lSSFastScanScaner.FindDevicePartialyKnownAsync(work_id, InCancellationToken, OutCancellationToken, p);

        public byte FindUnusedNodeID()
        {
            for (byte id = 1; id < Constants.CANNodeIDMax; ++id)
            {
                try
                {
                    _ = SDOClient.Connect(id).ReadSDOIndex<uint>(TestNodePresentIndex);
                }
                catch (CanError)
                {
                    return id;
                }
            }
            throw new CanError("All Node ID's are occupied");
        }

        public CanOpenStatus LSSApplyBusSpeed() => lSS_Master.activateBitTimingParameters(0);

        public void LSSConfiguration(UniqueLSSId uniqueLSSId)
        {
            static void throwIfNotOk(CanOpenStatus status)
            {
                if (status != CanOpenStatus.CANOPEN_OK)
                {
                    throw new CanError("LSS Switch state selective failed", status);
                }
            }

            throwIfNotOk(lSS_Master.switchModeSelectiveVendorId(uniqueLSSId.VendorID));
            throwIfNotOk(lSS_Master.switchModeSelectiveProductCode(uniqueLSSId.ProductID));
            throwIfNotOk(lSS_Master.switchModeSelectiveRevisionNumber(uniqueLSSId.RevisionNumber));
            throwIfNotOk(lSS_Master.switchModeSelectiveSerialNumber(uniqueLSSId.SerialNumber));
        }

        public CanOpenStatus LSSConfigureBusSpeed(byte speed_grade)
            => lSS_Master.configureBitTimingParamteres(0, speed_grade, out _, out _);

        public byte LSSConfigureNodeID(byte nodeID)
        {
            if (nodeID == 0)
            {
                nodeID = FindUnusedNodeID();
            }
            var result = lSS_Master.configureNodeId(nodeID, out _, out _);
            if (result != CanOpenStatus.CANOPEN_OK)
            {
                throw new CanError($"Failed to assign Node ID {nodeID}", result);
            }
            return nodeID;
        }

        public CanOpenStatus LSSSaveSettings() => lSS_Master.storeConfiguration(out _, out _);

        public void LSSSwitchStateGlobal(bool IsConfigurationState)
            => lSS_Master.switchModeGlobal(IsConfigurationState ? (byte)1 : (byte)0);

        public CanOpenStatus NMT_SendCommand(byte targetNodeID, NMTCommandCode command)
            => command switch
            {
                NMTCommandCode.GOTO_OPERTIONAL => NMT_Master.nodeStart(targetNodeID),
                NMTCommandCode.GOTO_PRE_OPERATIONAL => NMT_Master.nodePreoperational(targetNodeID),
                NMTCommandCode.GOTO_STOPPED => NMT_Master.nodeStop(targetNodeID),
                NMTCommandCode.RESET_NODE => NMT_Master.nodeReset(targetNodeID),
                NMTCommandCode.RESET_COMMUNICATION => NMT_Master.nodeResetCommunication(targetNodeID),
                _ => CanOpenStatus.CANOPEN_CMD_SPEC_UNKNOWN_OR_INVALLD,
            };

        public void RegisterEMCYConsumer(object id, Action<byte, uint, ushort, byte[]> callback)
            => EMCYSubscribers.TryAdd(id, callback);

        public void RegisterHeartbeatConsumer(object id, Action<byte, NMTState> callback)
            => heartbeatSubscribers.TryAdd(id, callback);

        public CanOpenStatus RegisterPDOConsumer(object id, uint CobId, Action<uint, byte[]> callback)
        {
            lock (PDOSubscribers)
            {
                if (PDOSubscribers.TryGetValue(id, out PDOListener existing_listener))
                {
                    existing_listener.Update(CobId, callback);
                    existing_listener.Enabled = true;
                    return CanOpenStatus.CANOPEN_OK;
                }
                else
                {
                    // new listener
                    var listener = new PDOListener(CobId) { Callback = callback };
                    var res = listener.TryConnect(ConnectionId, Speed);
                    if (res == CanOpenStatus.CANOPEN_OK)
                    {
                        PDOSubscribers.TryAdd(id, listener);
                    }
                    return res;
                }
            }
        }

        //--------------------------------------------------------------------------------------------

        public T SDORead<T>(byte traget_node, ushort Index, byte subindex) where T : struct
        {
            lock (SDOClient)
            {
                SDOClient.Connect(traget_node);

                var retry = Retrys;
                while (true)
                {
                    try
                    {
                        return SDOClient.ReadSDOIndex<T>(Index, subindex);
                    }
                    catch (CanError ex)
                    {
                        if (ex.code != CanOpenStatus.CANOPEN_TIMEOUT || --retry == 0)
                        {
                            throw ex;
                        }
                    }
                }
            }
        }

        public byte[] SDOReadByteArray(byte targetNodeID, ushort index, byte subindex)
        {
            lock (SDOClient)
            {
                SDOClient.Connect(targetNodeID);

                var retry = Retrys;
                while (true)
                {
                    try
                    {
                        return SDOClient.ReadBytesSDOIndex(index, subindex, 1024 * 1024);
                    }
                    catch (CanError ex)
                    {
                        if (ex.code != CanOpenStatus.CANOPEN_TIMEOUT || --retry == 0)
                        {
                            throw ex;
                        }
                    }
                }
            }
        }

        public void SDOWrite<T>(byte traget_node, ushort Index, byte subindex, T value) where T : struct
        {
            lock (SDOClient)
            {
                SDOClient.Connect(traget_node);

                var retry = Retrys;
                while (true)
                {
                    try
                    {
                        SDOClient.WriteSDOIndex(Index, subindex, value);
                        return;
                    }
                    catch (CanError ex)
                    {
                        if (ex.code != CanOpenStatus.CANOPEN_TIMEOUT || --retry == 0)
                        {
                            throw ex;
                        }
                    }
                }
            }
        }

        public void SDOWriteByteArray(byte target_node, ushort index, byte subindex, byte[] vs)
        {
            lock (SDOClient)
            {
                SDOClient.Connect(target_node);

                var retry = Retrys;
                while (true)
                {
                    try
                    {
                        SDOClient.BlockWriteSDOIndex(index, subindex, vs);
                        return;
                    }
                    catch (CanError ex)
                    {
                        if (ex.code != CanOpenStatus.CANOPEN_TIMEOUT || --retry == 0)
                        {
                            throw ex;
                        }
                    }
                }
            }
        }

        //---------------------------------------------------------------------------------------------

        public void UnRegisterHeartbeatConsumer(object id) => heartbeatSubscribers.TryRemove(id, out _);

        public void UnRegisterPDOConsumer(object id)
        {
            lock (PDOSubscribers)
            {
                if (PDOSubscribers.TryGetValue(id, out PDOListener subscriber))
                {
                    // do not remove, just disable
                    subscriber.Enabled = false;
                }
            }
        }

        public void UnRegisterEMCYConsumer(object id) => EMCYSubscribers.TryRemove(id, out _);

        private void OnHeartbeat(object _, byte node_id, NMTState state)
            => heartbeatSubscribers.ForEach(item => item.Value.Invoke(node_id, state));

        private void OnEMCY(object sender, byte nodeId, uint errorRegister, ushort emcyErrorCode, byte[] manufacturerSpec)
            => EMCYSubscribers.ForEach(item => item.Value.Invoke(nodeId, errorRegister, emcyErrorCode, manufacturerSpec));

        #endregion Methods
    }
}