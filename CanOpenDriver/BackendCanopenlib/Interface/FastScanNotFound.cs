﻿using System.IO;

namespace BackendCanopenlib32.Interface
{
	public class FastScanNotFound : IOException
	{
		#region Constructors

		public FastScanNotFound() : base("No unconfigured devices found") { }

		#endregion Constructors
	}
}