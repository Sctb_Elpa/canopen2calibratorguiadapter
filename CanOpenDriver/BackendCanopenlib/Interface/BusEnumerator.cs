﻿using System;
using System.Collections.Generic;
using DatalinkEngineering.CANopen;

namespace BackendCanopenlib32.Interface
{
    internal static class BusEnumerator
    {
        private static readonly object enumeration_lock = new object();
        private static bool isEnumerationInProgress = false;

        public static bool IsEnumerationInProgress
        {
            get
            {
                lock (enumeration_lock)
                {
                    return isEnumerationInProgress;
                }
            }
            private set
            {
                lock (enumeration_lock)
                {
                    isEnumerationInProgress = value;
                }
            }
        }

        public static IEnumerable<int> EnumerateCanopenlib32Buses()
        {
            IsEnumerationInProgress = true;
            try
            {
                var client = new CanMonitor_NET();
                for (byte i = 0; i < byte.MaxValue; ++i)
                {
                    var res = client.canHardwareConnect(i, 50000);
                    client.canHardwareDisconnect();

                    if (res == CanOpenStatus.CANOPEN_OK)
                    {
                        yield return i;
                    }
                    else
                    {
                        IsEnumerationInProgress = false;
                        yield break;
                    }
                }
            }
            finally
            {
                IsEnumerationInProgress = false;
            }
        }
    }
}