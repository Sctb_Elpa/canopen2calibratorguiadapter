﻿using DatalinkEngineering.CANopen;
using Utils.NMT;

namespace BackendCanopenlib32.Interface
{
    public class HeartbeatListener
    {
        #region Delegates

        public delegate void HeartbeatEvent(object sender, byte node_id, NMTState state);

        #endregion Delegates

        #region Fields

        public event HeartbeatEvent OnHeartbeat;

        #endregion Fields

        #region Methods

        public static CanOpenStatus HeartbeatRessived(object obj, byte node_id, byte state)
        {
            if (obj is HeartbeatListener _this)
            {
                _this.OnHeartbeat?.Invoke(_this, node_id, (NMTState)state);
                return CanOpenStatus.CANOPEN_OK;
            }

            return CanOpenStatus.CANOPEN_OBJECT_MISSMATCH;
        }

        #endregion Methods
    }
}