using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using BackendCanopenlib32.Interface;
using DatalinkEngineering.CANopen;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using HalperExtantions;
using Microsoft.Extensions.Logging;
using Ru.Sctbelpa.CanopenDriver;
using Utils.LSS;
using CanError = Ru.Sctbelpa.CanopenDriver.CanError;
using ValueType = Ru.Sctbelpa.CanopenDriver.SDOReadRequest.Types.enValueType;

namespace GrpcService
{
    public class ConnectionService : Connection.ConnectionBase
    {
        #region Fields

        private readonly ILogger<ConnectionService> _logger;
        private readonly CANConnection Connection;
        private readonly Dictionary<ValueType, MethodInfo> methodCache = new Dictionary<ValueType, MethodInfo>();

        #endregion Fields

        #region Constructors

        public ConnectionService(CANConnection connection, ILogger<ConnectionService> logger)
        {
            Connection = connection;
            _logger = logger;
        }

        #endregion Constructors

        #region Methods

        public override Task<ConnectionStatus> ConnectBus(ConnectionRequest request, ServerCallContext context)
        {
            _logger.LogInformation($"Connecting to {request.Id} at {request.Speed}");

            Connection.Speed = request.Speed.ToCanBusSpeedvalue();
            var error = Connection.Connect(request.Id);

            var status = PrivConnectionStatus();
            if (error != CanOpenStatus.CANOPEN_OK)
            {
                var msg = $"Failed to open port ({error})";
                _logger.LogError(msg);
                status.Error = new CanError
                {
                    Message = msg
                };
            }

            return Task.FromResult(status);
        }

        public override Task<ConnectionStatus> DisconnectBus(Empty request, ServerCallContext context)
        {
            _logger.LogInformation($"Disconnecting from CAN bus...");

            Connection.Disconnect();
            return Task.FromResult(PrivConnectionStatus());
        }

        public override async Task GetConnectionList(Empty request,
            IServerStreamWriter<CANConnectionID> responseStream, ServerCallContext context)
        {
            _logger.LogInformation("Enumerating CAN busses...");

            if (!BusEnumerator.IsEnumerationInProgress)
            {
                try
                {
                    foreach (var busID in BusEnumerator.EnumerateCanopenlib32Buses())
                    {
                        _logger.LogDebug($"Found bus id={busID}");
                        await responseStream.WriteAsync(new CANConnectionID
                        {
                            ID = busID,
                            Uri = busID.ToBackendBusID(),
                            Features = ConnectionFetures.HasBaudRateSelect
                        });
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Failed to get device list ({ex.Message})");
                }
            }
            else
            {
                _logger.LogError("CAN bus enumeration allready in progress!");
            }
        }

        public override Task<ConnectionStatus> IsConnected(Empty request, ServerCallContext context)
            => Task.FromResult(PrivConnectionStatus());

        public override Task<Result> LSSApplyBusSpeed(Empty request, ServerCallContext context)
        {
            var stopwatch = Stopwatch.StartNew();
            _logger.LogWarning("LSS apply bus speed");

            var res = Connection.LSSApplyBusSpeed();
            var msg = $"Failed to apply Bus Speed ({res})";

            logerror(res, msg);

            return BuildResult(res, msg, stopwatch.ElapsedMilliseconds);
        }

        public override Task<Result> LSSConfigureBusSpeed(BusSpeedConfig request, ServerCallContext context)
        {
            var stopwatch = Stopwatch.StartNew();
            var res = Connection.LSSConfigureBusSpeed((byte)request.Speed);
            var msg = $"Failed to configure Bus Speed ({res})";

            logerror(res, msg);

            return BuildResult(res, msg, stopwatch.ElapsedMilliseconds);
        }

        public override Task<NodeIDResult> LSSFindUnusedNodeID(Empty request, ServerCallContext context)
        {
            var stopwatch = Stopwatch.StartNew();
            try
            {
                var nodeID = Connection.FindUnusedNodeID();
                return Task.FromResult(new NodeIDResult
                {
                    NodeID = nodeID,
                    ExecutuinTimeMs = stopwatch.ElapsedMilliseconds
                });
            }
            catch (BackendCanopenlib32.Interface.CanError ex)
            {
                _logger.LogError(ex.Message);
                return Task.FromResult(new NodeIDResult
                {
                    Error = new CanError
                    {
                        Message = ex.Message
                    },
                    ExecutuinTimeMs= stopwatch.ElapsedMilliseconds
                });
            }
        }

        public override Task<NodeIDResult> LSSConfigureNodeID(NodeIDConfig request, ServerCallContext context)
        {
            var stopwatch = Stopwatch.StartNew();
            try
            {
                var nodeID = Connection.LSSConfigureNodeID((byte)request.NodeID);

                return Task.FromResult(new NodeIDResult
                {
                    NodeID = nodeID,
                    ExecutuinTimeMs = stopwatch.ElapsedMilliseconds
                });
            }
            catch (BackendCanopenlib32.Interface.CanError ex)
            {
                _logger.LogError(ex.Message);
                return Task.FromResult(new NodeIDResult
                {
                    Error = new CanError
                    {
                        Message = ex.Message
                    },
                    ExecutuinTimeMs = stopwatch.ElapsedMilliseconds
                });
            }
        }

        public override Task<Result> LSSSaveSettings(Empty request, ServerCallContext context)
        {
            var stopwatch = Stopwatch.StartNew();
            var res = Connection.LSSSaveSettings();
            var msg = $"Failed to save LSS configuration ({res})";

            logerror(res, msg);

            return BuildResult(res, msg, stopwatch.ElapsedMilliseconds);
        }

        public override async Task LSSStartFastScan(LSSID request, IServerStreamWriter<ScanProgress> responseStream,
            ServerCallContext context)
        {
            if (!Connection.IsFastScanInProgress)
            {
                _logger.LogWarning("LSS fast scan started...");

                var buf = new BufferBlock<ScanProgress>();
                CancellationTokenSource scanStopped = new CancellationTokenSource();

                var work_id = request.ToUniqLSSID();
                var scanTask = Task.Run<UniqueLSSId>(() => Connection.FindDeviceAsync(work_id, context.CancellationToken,
                    scanStopped, (byte lss_bit_check, byte lss_sub) =>
                  {
                      var msg = new ScanProgress
                      {
                          Progress = new ScanProgress.Types.ScanProgressStateus
                          {
                              Field = (ScanProgress.Types.ScanProgressStateus.Types.FieldID)lss_sub,
                              BitN = lss_bit_check
                          }
                      };
                      _logger.LogDebug($"Fast scan: {msg.Progress.Field}:{lss_bit_check}");
                      buf.Post(msg);
                  }));

                while (true)
                {
                    try
                    {
                        await responseStream.WriteAsync(await buf.ReceiveAsync(scanStopped.Token));
                    }
                    catch (TaskCanceledException)
                    {
                        break;
                    }
                }

                if (context.CancellationToken.IsCancellationRequested)
                {
                    Connection.LSSSwitchStateGlobal(false);
                    _logger.LogWarning("Fast scan was cancled");
                    return;
                }

                try
                {
                    work_id = await scanTask;
                    if (work_id.Valid())
                    {
                        await responseStream.WriteAsync(new ScanProgress
                        {
                            LssResult = work_id.ToPB_LSSID()
                        });

                        _logger.LogInformation($"Found unconfigured device {work_id}");
                    }
                    else
                    {
                        var msg = "Fast scan Was canceled";
                        await responseStream.WriteAsync(new ScanProgress
                        {
                            ErrorMessage = new CanError
                            {
                                Message = "Fast scan Was canceled"
                            }
                        });

                        _logger.LogWarning(msg);
                    }
                    Connection.LSSSwitchStateGlobal(false);
                }
                catch (BackendCanopenlib32.Interface.FastScanNotFound ex)
                {
                    _logger.LogWarning(ex.Message);
                    await responseStream.WriteAsync(new ScanProgress
                    {
                        ErrorMessage = new CanError
                        {
                            Message = ex.Message
                        }
                    });
                }
                catch (BackendCanopenlib32.Interface.CanError ex)
                {
                    _logger.LogError(ex.Message);
                    await responseStream.WriteAsync(new ScanProgress
                    {
                        ErrorMessage = new CanError
                        {
                            Message = ex.Message
                        }
                    });
                }
            }
            else
            {
                var msg = "Fast scan allready in progress";

                _logger.LogError(msg);

                await responseStream.WriteAsync(new ScanProgress
                {
                    ErrorMessage = new CanError
                    {
                        Message = msg
                    }
                });
            }
        }

        public override Task<Result> LSSSwitchState(LSSState request, ServerCallContext context)
        {
            var stopwatsh = new Stopwatch();
            try
            {
                if (request.TargetNodeID == null || request.State == LSSState.Types.StateCode.Waiting)
                {
                    bool conf = request.State == LSSState.Types.StateCode.Configuration;
                    Connection.LSSSwitchStateGlobal(conf);
                    _logger.LogInformation($"Switched global LSS state to {(conf ? "Configuration" : "Waiting")}");
                }
                else
                {
                    var lssid = request.TargetNodeID.ToUniqLSSID();
                    Connection.LSSConfiguration(lssid);
                    _logger.LogInformation($"Device {lssid} switched to Configuration state");
                }
                return Task.FromResult(new Result() { ExecutuinTimeMs = stopwatsh.ElapsedMilliseconds });
            }
            catch (BackendCanopenlib32.Interface.CanError ex)
            {
                _logger.LogError($"SwitchState exception: {ex.Message}");

                return Task.FromResult(new Result
                {
                    Error = new CanError
                    {
                        Message = ex.Message
                    },
                    ExecutuinTimeMs = stopwatsh.ElapsedMilliseconds
                });
            }
        }

        public override Task<Result> NMTSendCommand(NMTCommand request, ServerCallContext context)
        {
            var stopwatch = Stopwatch.StartNew();
            var res = Connection.NMT_SendCommand((byte)request.TargetNodeID, request.Command.ToNMTCommandCode());
            var msg = $"Failed to send NMT command ({res})";

            logerror(res, msg);

            return BuildResult(res, msg, stopwatch.ElapsedMilliseconds);
        }

        private MethodInfo GetReader(ValueType type)
        {
            MethodInfo method;
            if (!methodCache.TryGetValue(type, out method))
            {
                method = typeof(CANConnection)
                    .GetMethod("SDORead")
                    .MakeGenericMethod(type.ToType());
                methodCache[type] = method;
            }
            return method;
        }

        public override Task<SDOReadResult> SDOReadIndex(SDOReadRequest request, ServerCallContext context)
        {
            var stopwatch = Stopwatch.StartNew();

            var result = new SDOReadResult();
            var Value = new SDOIndexValue();

            try
            {
                if (request.ValueType == ValueType.Bytes)
                {
                    Value.Bytes = Google.Protobuf.ByteString.CopyFrom(
                            Connection.SDOReadByteArray((byte)request.TargetNodeID,
                                (ushort)request.Index.Index, (byte)request.Index.Subindex));
                }
                else
                {
                    MethodInfo method = GetReader(request.ValueType);
                    var arg = new object[] { (byte)request.TargetNodeID, (ushort)request.Index.Index, (byte)request.Index.Subindex };

                    switch (request.ValueType)
                    {
                        case ValueType.ValueU8:
                            Value.ValueU8 = To<uint>(method.Invoke(Connection, arg));
                            break;

                        case ValueType.ValueU16:
                            Value.ValueU16 = To<uint>(method.Invoke(Connection, arg));
                            break;

                        case ValueType.ValueU32:
                            Value.ValueU32 = To<uint>(method.Invoke(Connection, arg));
                            break;

                        case ValueType.ValueU64:
                            Value.ValueU64 = To<UInt64>(method.Invoke(Connection, arg));
                            break;

                        case ValueType.ValueS8:
                            Value.ValueS8 = To<int>(method.Invoke(Connection, arg));
                            break;

                        case ValueType.ValueS16:
                            Value.ValueS16 = To<int>(method.Invoke(Connection, arg));
                            break;

                        case ValueType.ValueS32:
                            Value.ValueS32 = To<int>(method.Invoke(Connection, arg));
                            break;

                        case ValueType.ValueS64:
                            Value.ValueS64 = To<Int64>(method.Invoke(Connection, arg));
                            break;

                        case ValueType.ValueBool:
                            Value.ValueBool = To<bool>(method.Invoke(Connection, arg));
                            break;

                        case ValueType.ValueFloat:
                            Value.ValueFloat = To<float>(method.Invoke(Connection, arg));
                            break;

                        case ValueType.ValueDouble:
                            Value.ValueDouble = To<double>(method.Invoke(Connection, arg));
                            break;
                    }
                }

                result.Value = Value;
                result.ExecutuinTimeMs = stopwatch.ElapsedMilliseconds;
                _logger.LogDebug($"[Node {request.TargetNodeID}] <{request.Index.Index:X}sub{request.Index.Subindex}> -> {Value} ({result.ExecutuinTimeMs} ms)");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException is BackendCanopenlib32.Interface.CanError cex)
                {
                    _logger.LogError($"Failed to read data ({request.ValueType}) from {request.Index.Index:X}sub{request.Index.Subindex:X}, {ex.Message}");

                    result.Error = new CanError
                    {
                        Message = ex.Message
                    };
                    result.ExecutuinTimeMs = stopwatch.ElapsedMilliseconds;
                }
                else
                { 
                    _logger.LogError($"Unknown exception {ex.Message}");

                    result.Error = new CanError
                    {
                        Message = ex.Message
                    };
                    result.ExecutuinTimeMs = stopwatch.ElapsedMilliseconds;
                }
            }

            return Task.FromResult(result);
        }

        public override Task<Result> SDOWriteIndex(SDOWriteRequest request, ServerCallContext context)
        {
            var stopwatch = Stopwatch.StartNew();
            var result = new Result();

            byte target_node = (byte)request.TargetNodeID;
            ushort Index = (ushort)request.DestIndex.Index;
            byte Subindex = (byte)request.DestIndex.Subindex;

            try
            {
                switch (request.Value.ValueCase)
                {
                    case SDOIndexValue.ValueOneofCase.ValueU8:
                        Connection.SDOWrite(target_node, Index, Subindex, (byte)request.Value.ValueU8);
                        break;

                    case SDOIndexValue.ValueOneofCase.ValueU16:
                        Connection.SDOWrite(target_node, Index, Subindex, (ushort)request.Value.ValueU16);
                        break;

                    case SDOIndexValue.ValueOneofCase.ValueU32:
                        Connection.SDOWrite(target_node, Index, Subindex, request.Value.ValueU32);
                        break;

                    case SDOIndexValue.ValueOneofCase.ValueU64:
                        Connection.SDOWrite(target_node, Index, Subindex, request.Value.ValueU64);
                        break;

                    case SDOIndexValue.ValueOneofCase.ValueS8:
                        Connection.SDOWrite(target_node, Index, Subindex, (sbyte)request.Value.ValueS8);
                        break;

                    case SDOIndexValue.ValueOneofCase.ValueS16:
                        Connection.SDOWrite(target_node, Index, Subindex, (short)request.Value.ValueS16);
                        break;

                    case SDOIndexValue.ValueOneofCase.ValueS32:
                        Connection.SDOWrite(target_node, Index, Subindex, request.Value.ValueS32);
                        break;

                    case SDOIndexValue.ValueOneofCase.ValueS64:
                        Connection.SDOWrite(target_node, Index, Subindex, request.Value.ValueS64);
                        break;

                    case SDOIndexValue.ValueOneofCase.Bytes:
                        Connection.SDOWriteByteArray(target_node, Index, Subindex, request.Value.Bytes.ToByteArray());
                        break;

                    case SDOIndexValue.ValueOneofCase.ValueBool:
                        Connection.SDOWrite(target_node, Index, Subindex, request.Value.ValueBool);
                        break;

                    case SDOIndexValue.ValueOneofCase.ValueFloat:
                        Connection.SDOWrite(target_node, Index, Subindex, request.Value.ValueFloat);
                        break;

                    case SDOIndexValue.ValueOneofCase.ValueDouble:
                        Connection.SDOWrite(target_node, Index, Subindex, request.Value.ValueDouble);
                        break;

                    case SDOIndexValue.ValueOneofCase.None:
                        _logger.LogError("Attempt to SDO write empty data");
                        result.Error = new CanError
                        {
                            Message = "Empty value to write"
                        };
                        break;
                }
                result.ExecutuinTimeMs = stopwatch.ElapsedMilliseconds;
                _logger.LogDebug($"[Node {request.TargetNodeID}]: <{Index:X}sub{Subindex}> <- {request.Value} ({result.ExecutuinTimeMs} ms)");
            }
            catch (BackendCanopenlib32.Interface.CanError ex)
            {
                _logger.LogError($"SDOWriteIndex exception: {ex.Message}");
                result.Error = new CanError
                {
                    Message = ex.Message
                };
                result.ExecutuinTimeMs = stopwatch.ElapsedMilliseconds;
            }

            return Task.FromResult(result);
        }

        public override async Task SubscribeHeartbeat(Empty request,
            IServerStreamWriter<HeartbeatItem> responseStream, ServerCallContext context)
        {
            var buf = new BufferBlock<HeartbeatItem>();

            Connection.RegisterHeartbeatConsumer(request, (byte NodeID, Utils.NMT.NMTState State) =>
            {
                _logger.LogDebug($"Heartbeat from {NodeID}, State: {State}");
                buf.Post(new HeartbeatItem
                {
                    DateTimeStamp = Timestamp.FromDateTime(DateTime.UtcNow),
                    NodeID = NodeID,
                    State = (NodeState)State
                });
            });

            _logger.LogInformation("Client have just subscrubed to Heartbeat messages");

            // Keep the stream open so we can continue writing new Messages as they are pushed
            while (true)
            {
                try
                {
                    await responseStream.WriteAsync(await buf.ReceiveAsync(context.CancellationToken));
                }
                catch (TaskCanceledException)
                {
                    break;
                }
            }

            _logger.LogInformation("Client have just unsubscrubed from Heartbeat messages");

            Connection.UnRegisterHeartbeatConsumer(request);
        }

        public override async Task PDOSubscribe(PDOSubscribeInfo request, IServerStreamWriter<PDOMessage> responseStream,
            ServerCallContext context)
        {
            var buf = new BufferBlock<PDOMessage>();

            var sr = Connection.RegisterPDOConsumer(request, request.CobId, (uint CobId, byte[] data) =>
            {
                _logger.LogDebug($"PDO with COB-ID: {CobId}, data: {BitConverter.ToString(data)}");
                buf.Post(new PDOMessage
                {
                    CobId = CobId,
                    Data = Google.Protobuf.ByteString.CopyFrom(data)
                });
            });

            if (sr != CanOpenStatus.CANOPEN_OK)
            {
                _logger.LogError("Failed to subscribe to PDO message!");
                return;
            }

            _logger.LogInformation($"Client have just subscrubed to PDO messages with COB-ID={request.CobId}");

            // Keep the stream open so we can continue writing new Messages as they are pushed
            while (true)
            {
                try
                {
                    await responseStream.WriteAsync(await buf.ReceiveAsync(context.CancellationToken));
                }
                catch (TaskCanceledException)
                {
                    break;
                }
            }

            _logger.LogInformation($"Client have just unsubscrubed from PDO messages with COB-ID={request.CobId}");

            Connection.UnRegisterPDOConsumer(request);
        }

        public override Task<SyncConfigResult> GetSyncConfig(Empty request, ServerCallContext context)
        {
            var syncProdusser = Connection.SyncProducer;
            return Task.FromResult(new SyncConfigResult
            {
                Value = new SyncConfig
                {
                    CobID = syncProdusser.SyncCobID,
                    SyncPeriod = syncProdusser.Period,
                    Enabled = syncProdusser.Enabled
                }
            });
        }

        public override Task<Result> SetSyncConfig(SyncConfig request, ServerCallContext context)
        {
            var stopwatch = Stopwatch.StartNew();
            try
            {
                Connection.SyncProducer.Configure((ushort)request.SyncPeriod, request.CobID, request.Enabled);
                return Task.FromResult(new Result() { ExecutuinTimeMs = stopwatch.ElapsedMilliseconds });
            }
            catch (BackendCanopenlib32.Interface.CanError ex)
            {
                return Task.FromResult(new Result
                {
                    Error = new CanError
                    {
                        Message = ex.Message
                    },
                    ExecutuinTimeMs = stopwatch.ElapsedMilliseconds
                });
            }
        }

        public override async Task SubscribeEmergency(Empty request, IServerStreamWriter<EMCYItem> responseStream,
            ServerCallContext context)
        {
            var buf = new BufferBlock<EMCYItem>();

            Connection.RegisterEMCYConsumer(request, (nodeId, errorRegister, emcyErrorCode, manufacturerSpec) =>
            {
                _logger.LogDebug($"EMCY from Node {nodeId}: E=0b{Convert.ToString(errorRegister, 2)}, " +
                    $"C=0x{emcyErrorCode:X4}, M={BitConverter.ToString(manufacturerSpec)}");
                buf.Post(new EMCYItem
                {
                    NodeId = nodeId,
                    Timestamp = Timestamp.FromDateTime(DateTime.UtcNow),
                    ErrorRegister = errorRegister,
                    EmcyErrorCode = emcyErrorCode,
                    ManufacturerSpec = Google.Protobuf.ByteString.CopyFrom(manufacturerSpec)
                });
            });

            _logger.LogInformation($"Client have just subscrubed to EMCY messages");

            // Keep the stream open so we can continue writing new Messages as they are pushed
            while (true)
            {
                try
                {
                    await responseStream.WriteAsync(await buf.ReceiveAsync(context.CancellationToken));
                }
                catch (TaskCanceledException)
                {
                    break;
                }
            }

            _logger.LogInformation($"Client have just unsubscrubed from EMCY messages");

            Connection.UnRegisterEMCYConsumer(request);
        }

        private static Task<Result> BuildResult(CanOpenStatus res, string msg, long elapsed_ms)
            => Task.FromResult(new Result
            {
                Error = res != CanOpenStatus.CANOPEN_OK
                                ? new CanError
                                {
                                    Message = msg
                                } : null,
                ExecutuinTimeMs = elapsed_ms
            });

        private void logerror(CanOpenStatus status, string msg)
        {
            if (status != CanOpenStatus.CANOPEN_OK)
            {
                _logger.LogError(msg);
            }
        }

        private ConnectionStatus PrivConnectionStatus() => new ConnectionStatus
        {
            Connection = new CANConnectionID
            {
                ID = Connection.ConnectionId,
                Uri = Connection.ConnectionId.ToBackendBusID(),
                Features = ConnectionFetures.HasBaudRateSelect
            },
            Speed = Connection.Speed.ToCanBusSpeedCode(),
            Connected = Connection.IsConnected,
        };

        private T To<T>(object obj) => (T)Convert.ChangeType(obj, typeof(T));

        #endregion Methods
    }
}