﻿using System.Collections.Generic;
using Ru.Sctbelpa.CanopenDriver;
using Utils;
using Utils.LSS;
using ValueType = Ru.Sctbelpa.CanopenDriver.SDOReadRequest.Types.enValueType;

namespace GrpcService
{
    internal static class Halper
    {
        private static readonly Dictionary<ValueType, System.Type> typemap =
            new Dictionary<ValueType, System.Type> {
                { ValueType.ValueU8, typeof(System.Byte) },
                { ValueType.ValueU16, typeof(System.UInt16) },
                { ValueType.ValueU32, typeof(System.UInt32) },
                { ValueType.ValueU64, typeof(System.UInt64) },

                { ValueType.ValueS8, typeof(System.SByte) },
                { ValueType.ValueS16, typeof(System.Int16) },
                { ValueType.ValueS32, typeof(System.Int32) },
                { ValueType.ValueS64, typeof(System.Int64) },

                { ValueType.ValueBool, typeof(System.Boolean) },
                { ValueType.ValueFloat, typeof(System.Single) },
                { ValueType.ValueDouble, typeof(System.Double) },

                { ValueType.Bytes, typeof(System.Byte[]) },
            };

        public static CanBusSpeed ToCanBusSpeedCode(this int src)
            => (CanBusSpeed)StaticData.speedMap.Forward[src];

        public static int ToCanBusSpeedvalue(this CanBusSpeed src)
            => StaticData.speedMap.Reverse[(byte)src];

        public static string ToBackendBusID(this int busID)
            => $"candle://{busID}";

        public static UniqueLSSId ToUniqLSSID(this LSSID lssid) => new UniqueLSSId
        {
            VendorID = lssid.VendorID,
            ProductID = lssid.ProductID,
            RevisionNumber = lssid.Revision,
            SerialNumber = lssid.Serial,

            KnownMask = (UniqueLSSId.KnownBits)lssid.KnownFields,
        };

        public static LSSID ToPB_LSSID(this UniqueLSSId lssid) => new LSSID
        {
            VendorID = lssid.VendorID,
            ProductID = lssid.ProductID,
            Revision = lssid.RevisionNumber,
            Serial = lssid.SerialNumber,

            KnownFields = (LSSID.Types.KnownFieldsID)lssid.KnownMask
        };

        public static System.Type ToType(this ValueType type) => typemap[type];
    }
}