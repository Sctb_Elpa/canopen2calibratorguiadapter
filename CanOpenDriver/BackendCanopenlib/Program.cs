using CommandLine;
using Common;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace GrpcService
{
    internal class Options
    {
        [Option("grpc-port", Required = false, HelpText = "gRPC port")]
        public int? GRpcPort { get; set; }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = CreateHostBuilder(args);
            if (builder == null)
            {
                return;
            }

            builder.Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Parser.Default.ParseArguments<Options>(args).MapResult(o =>
            {
                return Host.CreateDefaultBuilder(args)
                    .ConfigureWebHostDefaults(webBuilder =>
                    {
                        webBuilder.UseStartup<Startup>();
                        if (o.GRpcPort.HasValue)
                        {
                            webBuilder.UseUrls($"http://*:{o.GRpcPort}");
                        }
                        else
                        {
                            webBuilder.UseUrls($"http://*:{Defaults.DefaultDriverPort}");
                        }
                    });
            },
            _ => null);
        }
    }
}