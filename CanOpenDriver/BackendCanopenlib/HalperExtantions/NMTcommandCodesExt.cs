﻿namespace HalperExtantions
{
    public static class NMTcommandCodesExt
    {
        public static Ru.Sctbelpa.CanopenDriver.NMTCommand.Types.NMTCommandCode ToNMTCommandCode(this Utils.NMT.NMTCommandCode src) 
            => (Ru.Sctbelpa.CanopenDriver.NMTCommand.Types.NMTCommandCode)src;

        public static Utils.NMT.NMTCommandCode ToNMTCommandCode(this Ru.Sctbelpa.CanopenDriver.NMTCommand.Types.NMTCommandCode src)
            => (Utils.NMT.NMTCommandCode)src;
    }
}
