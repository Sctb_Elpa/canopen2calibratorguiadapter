﻿using System;

namespace BackendCanopenlib32.HalperExtantions
{
	public static class CRC
	{
		private static Crc16Ccitt mgr = new Crc16Ccitt(InitialCrcValue.Zeros);

		public static ushort crc16_ccit_0(byte[] data)
		{
			var bytes = mgr.ComputeChecksumBytes(data);
			return BitConverter.ToUInt16(bytes, 0);
		}
	}
}