﻿using System;
using System.Diagnostics;
using System.IO;

namespace BackendPython
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var py_dir = Path.Combine(
                Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), 
                "py");

            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,

                    FileName = "python",
                    Arguments = Path.Combine(py_dir, "BackendAsync.py") + 
                        (args.Length > 0 
                            ? " " + string.Join(' ', args) 
                            : string.Empty),

                    WorkingDirectory = py_dir
                }, 
                //EnableRaisingEvents = true
            };

            process.ErrorDataReceived += (sendingProcess, errorLine) => Console.Error.WriteLine(errorLine.Data);
            process.OutputDataReceived += (sendingProcess, dataLine) => Console.WriteLine(dataLine.Data);

            process.Start();
            process.BeginErrorReadLine();
            process.BeginOutputReadLine();

            System.Console.WriteLine("Python backend started");

            // kill Python process on CTRL+C
            Console.CancelKeyPress += (s, e) => {
                process.Kill(true);
            };

            process.WaitForExit();

            System.Console.WriteLine("Python backend finished");
        }
    }
}