#!/usr/bin/env python3

import argparse
import logging
import asyncio

from signal import SIGINT, SIGTERM

import grpc
import Backend_pb2_grpc

from CanOpenConnectionServicerAsync import CanOpenConnectionServicerAsync

# Manual:
# https://github.com/lidizheng/proposal/blob/grpc-python-async-api/L58-python-async-api.md


async def serve():
    try:
        log = logging.getLogger("serve")
        log.setLevel(logging.INFO)
        parser = argparse.ArgumentParser(description='pathon-canopen to gRPC server')

        parser.add_argument('--grpc-port', dest='port', type=int, required=False, default=1889,
                            help="gRPC port to listen")
        args = parser.parse_args()

        server = grpc.aio.server()
        server.add_insecure_port('[::]:{}'.format(args.port))
        Backend_pb2_grpc.add_ConnectionServicer_to_server(CanOpenConnectionServicerAsync(asyncio.get_event_loop()),
                                                          server)
        await server.start()
        log.info(f'Python CanOpen server listening on port {args.port}')
        await server.wait_for_termination()
    except asyncio.CancelledError:
        pass


if __name__ == '__main__':
    logging.basicConfig()

    asyncio.run(serve())

    # loop = asyncio.get_event_loop() # deprecated
    loop = asyncio.get_event_loop()
    #main_task = asyncio.ensure_future(serve())
    for signal in [SIGINT, SIGTERM]:
        loop.add_signal_handler(signal, main_task.cancel)
    try:
        loop.run_until_complete(main_task)
    finally:
        loop.close()
