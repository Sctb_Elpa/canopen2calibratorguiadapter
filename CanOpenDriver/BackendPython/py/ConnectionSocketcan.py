import asyncio
import struct
import threading

import canopen
import canopen.lss

import Connection

from BusEnumerator import enumerate_socketcan_buses
from CanError import CanError
from MyNetwork import MyNetwork


class ConnectionSocketcan(Connection.Connection):
    connection_type = 'socketcan'
    fake_cell_name = 'fake_index'

    def __init__(self, loop, channel, hb_consumers, emcy_consumers, bus_id=0):
        super().__init__(hb_consumers, emcy_consumers)
        self.loop = loop
        self._fast_scan_in_progress = False
        self.channel = channel
        self.bus_id = bus_id

        self.sdo_lock = threading.Lock()

        self.network = MyNetwork()
        self.network.connect(channel=channel, bustype=ConnectionSocketcan.connection_type)

        self.network.on_heartbeat = self._on_heartbeat
        self.network.on_emcy = self._on_emcy

    def _on_heartbeat(self, node, state, timestamp):
        for _, v in self._hb_consumers.items():
            self._put_queue_thread_safe(v, (node, state, timestamp))

    def _on_emcy(self, node, register, err_code, manufacturer_data, timestamp):
        for _, v in self._emcy_consumers.items():
            self._put_queue_thread_safe(v, (node, register, err_code, manufacturer_data, timestamp))

    def _put_queue_thread_safe(self, q, argument):
        self.loop.call_soon_threadsafe(q.put_nowait, argument)

    def is_connected(self):
        return True

    def dump_connection_status(self):
        return self.bus_id, Connection.Connection.build_connection_uri(
            ConnectionSocketcan.connection_type, self.channel), 0

    @staticmethod
    def enumerate():
        return enumerate_socketcan_buses()

    async def fast_scan(self, initial_lss, scan_progress, scan_result, scan_error):
        if self._fast_scan_in_progress:
            raise AssertionError()

        self._fast_scan_in_progress = True

        res, lss_id = await self.__fs_iml(initial_lss, scan_progress)
        if not res:
            await scan_error('No unconfigured devices found')
        else:
            await scan_result(*lss_id)
            self.network.lss.send_switch_state_global(0)  # restore waiting state

        self._fast_scan_in_progress = False
        return res

    def find_unused_node_id(self):
        for node_id in range(1, 128):
            node = self.network.add_node(node_id)
            try:
                _ = node.sdo.upload(0x1000, 0)
            except canopen.sdo.SdoCommunicationError:
                return node_id
            finally:
                node.remove_network()

        raise CanError('All nodes are used')

    def lss_switch_state(self, target_node_lss_id, state):
        if target_node_lss_id and state == 1:
            if not self.network.lss.send_switch_state_selective(target_node_lss_id.VendorID,
                                                                target_node_lss_id.ProductID,
                                                                target_node_lss_id.Revision,
                                                                target_node_lss_id.Serial):
                raise CanError('send_switch_state_selective() failed')
        else:
            self.network.lss.send_switch_state_global(self.network.lss.CONFIGURATION_STATE
                                                      if state == 1 else self.network.lss.WAITING_STATE)

    def lss_configure_node_id(self, node_id):
        if node_id == 0:
            node_id = self.find_unused_node_id()

        try:
            self.network.lss.configure_node_id(node_id)
        except canopen.lss.LssError as ex:
            raise CanError(str(ex))

        return node_id

    def lss_configure_bus_speed(self, speed_grade):
        try:
            self.network.lss.configure_bit_timing(speed_grade)
        except canopen.lss.LssError as ex:
            raise CanError(str(ex))

    def apply_bus_speed(self):
        try:
            self.network.lss.activate_bit_timing(0)
        except canopen.lss.LssError as ex:
            raise CanError(str(ex))

    def lss_save_settings(self):
        try:
            self.network.lss.store_configuration()
        except canopen.lss.LssError as ex:
            raise CanError(str(ex))

    def nmt_command(self, target_node_id, command):
        try:
            self.network.send_message(0, [command, target_node_id])
        except Exception as ex:
            raise CanError(str(ex))

    def get_sync_config(self):
        return self.network.sync.cob_id, int(self.network.sync.period * 1000) if self.network.sync.period else 1000, \
               self._is_sync_enabled()

    def set_sync_config(self, cob_id, period, enabled):
        if self._is_sync_enabled():
            self.network.sync.stop()
            self.network.sync._task = None

        self.network.sync.cob_id = cob_id
        self.network.sync.period = period / 1000.0

        if enabled:
            self.network.sync.start()

    def sdo_read(self, value, value_type, node_id, index):
        node = self.network.add_node(node_id)

        with self.sdo_lock:
            raw_data = node.sdo.upload(index.Index, index.Subindex)

        if value_type == 0:  # ValueU8
            value.ValueU8, = struct.unpack('<B', raw_data)
        elif value_type == 2:  # ValueU16
            value.ValueU16, = struct.unpack('<H', raw_data)
        elif value_type == 3:  # ValueU32
            value.ValueU32, = struct.unpack('<I', raw_data)
        elif value_type == 4:  # ValueU64
            value.ValueU32, = struct.unpack('<Q', raw_data)
        elif value_type == 10:  # ValueS8
            value.ValueS8, = struct.unpack('<b', raw_data)
        elif value_type == 11:  # ValueS16
            value.ValueS16, = struct.unpack('<h', raw_data)
        elif value_type == 12:  # ValueS32
            value.ValueS32, = struct.unpack('<i', raw_data)
        elif value_type == 13:  # ValueS64
            value.ValueS64, = struct.unpack('<q', raw_data)
        elif value_type == 14:  # ValueBool
            r, = struct.unpack('<B', raw_data)
            value.ValueS8 = r != 0
        elif value_type == 15:  # ValueFloat
            value.ValueFloat, = struct.unpack('<f', raw_data)
        elif value_type == 16:  # ValueDouble
            value.ValueDouble, = struct.unpack('<d', raw_data)
        elif value_type == 20:  # Bytes
            value.Bytes = raw_data
        else:
            node.remove_network()
            raise CanError(f'Unsupported value type {value_type}')

        node.remove_network()

    def sdo_write(self, node_id, index, value):
        node = self.network.add_node(node_id)

        value_type = value.WhichOneof("Value")

        if value_type == "ValueU8":
            data = struct.pack('<B', value.ValueU8)
        elif value_type == "ValueU16":
            data = struct.pack('<H', value.ValueU16)
        elif value_type == "ValueU32":
            data = struct.pack('<I', value.ValueU32)
        elif value_type == "ValueU64":
            data = struct.pack('<Q', value.ValueU64)
        elif value_type == "ValueS8":
            data = struct.pack('<b', value.ValueS8)
        elif value_type == "ValueS16":
            data = struct.pack('<h', value.ValueS16)
        elif value_type == "ValueS32":
            data = struct.pack('<i', value.ValueS32)
        elif value_type == "ValueS64":
            data = struct.pack('<q', value.ValueS64)
        elif value_type == "ValueBool":
            data = struct.pack('<B', 1 if value.ValueBool else 0)
        elif value_type == "ValueFloat":
            data = struct.pack('<f', value.ValueFloat)
        elif value_type == "ValueDouble":
            data = struct.pack('<d', value.ValueDouble)
        elif value_type == "Bytes":
            data = bytearray(value.Bytes)
        else:
            node.remove_network()
            raise CanError(f'Unsupported value type {value_type}')

        with self.sdo_lock:
            node.sdo.download(index.Index, index.Subindex, data)

        node.remove_network()

    def subscribe(self, cob_id, callback):
        self.network.subscribe(cob_id, callback)

    def unsubscribe(self, cob_id, callback):
        self.network.unsubscribe(cob_id, callback)

    def __exit__(self):
        self.network.disconnect()

    # ---------------------------------------------------------

    def _is_sync_enabled(self):
        return self.network.sync._task is not None

    async def __fs_iml(self, initial_lss, scan_progress=None):
        def _is_sub_known(sub):
            return initial_lss.KnownFields & (1 << sub) != 0

        def __send_fast_scan_message(id_number, bit_checker, lss_sub, lss_next):
            return self.network.lss._LssMaster__send_fast_scan_message(id_number, bit_checker, lss_sub, lss_next)

        lss_id = [initial_lss.VendorID, initial_lss.ProductID, initial_lss.Revision, initial_lss.Serial]
        lss_sub = 0
        lss_next = 0

        if __send_fast_scan_message(lss_id[0], 128, lss_sub, lss_next):
            lss_bit_check = 0
            await asyncio.sleep(0.01)
            while lss_sub < 4:
                if not _is_sub_known(lss_sub):
                    lss_bit_check = 32
                    while lss_bit_check > 0:
                        lss_bit_check -= 1

                        if scan_progress:
                            await scan_progress(lss_bit_check, lss_sub)

                        if not __send_fast_scan_message(lss_id[lss_sub], lss_bit_check, lss_sub, lss_next):
                            lss_id[lss_sub] |= 1 << lss_bit_check

                        await asyncio.sleep(0.01)

                lss_next = (lss_sub + 1) & 3
                if not __send_fast_scan_message(lss_id[lss_sub], lss_bit_check, lss_sub, lss_next):
                    return False, None

                await asyncio.sleep(0.01)

                # Now the next 32 bits will be scanned
                lss_sub += 1

            # Now lss_id contains the entire 128 bits scanned
            return True, lss_id

        return False, None
