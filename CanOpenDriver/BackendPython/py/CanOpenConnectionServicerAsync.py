import re
import logging
import asyncio

import grpc
import Backend_pb2
import Backend_pb2_grpc

from Connection import Connection
from ConnectionSocketcan import ConnectionSocketcan
from CanError import CanError

import google.protobuf.empty_pb2
import google.protobuf.internal.well_known_types

Drivers = {
    ConnectionSocketcan.connection_type: ConnectionSocketcan
}


class CanOpenConnectionServicerAsync(Backend_pb2_grpc.ConnectionServicer):
    """Provides methods that implement functionality of route guide server."""

    def __init__(self, loop):
        self.loop = loop
        self.connection = None
        self.heartbeat_consumers = {}
        self.emcy_consumers = {}
        self.log = logging.getLogger("CanOpenConnection")

    @staticmethod
    def _enumerate_connections():
        i = 0

        # socketcan
        for bus in ConnectionSocketcan.enumerate():
            c = Backend_pb2.CANConnectionID()
            c.ID = i
            c.Uri = Connection.build_connection_uri(ConnectionSocketcan.connection_type, bus)
            c.Features = Backend_pb2.ConnectionFetures.NONE
            i += 1

            yield c

    # ------------------------------------------------------

    def _connect_to(self, bus_uri, bus_id):
        m = re.search(r'(.*)://(.*)', bus_uri)
        if not m:
            raise CanError('Bus Uri={} is invalid'.format(bus_uri))
        self.connection = Drivers[m[1]](self.loop, m[2], self.heartbeat_consumers, self.emcy_consumers, bus_id)

    # ------------------------------------------------------

    async def GetConnectionList(self,
                                empty_request,
                                context: grpc.aio.ServicerContext
                                ) -> None:
        for dev in CanOpenConnectionServicerAsync._enumerate_connections():
            await context.write(dev)

    def IsConnected(self, request, context):
        status = Backend_pb2.ConnectionStatus()
        status.connected = self.connection.is_connected() if self.connection else False
        status.Speed = 0

        connection_state = Backend_pb2.CANConnectionID()
        if self.connection:
            connection_state.ID, connection_state.Uri, connection_state.Features = \
                self.connection.dump_connection_status()
        else:  # Заглушка
            connection_state.ID = 0
            connection_state.Uri = ""
            connection_state.Features = Backend_pb2.ConnectionFetures.NONE

        status.Connection.CopyFrom(connection_state)

        return status

    def ConnectBus(self, request, context):
        buses = CanOpenConnectionServicerAsync._enumerate_connections()

        ex = None
        try:
            while True:
                b = next(buses)
                if b.ID == request.Id:
                    break
            self._connect_to(b.Uri, b.ID)
            self.log.info(f'Connecting to {b.Uri}')
        except StopIteration:
            self.log.error(f'Unknown bus id {request.Id}')
        except IOError as ex:  # this not worked
            pass

        res = self.IsConnected(request, context)
        if ex:
            res.Error.CopyFrom(res.Error.CopyFrom(self.__ex2error(ex)))

        return res

    def DisconnectBus(self, request, context):
        if self.connection:
            self.connection = None

        self.log.info('Disconnect from CAN bus')
        return self.IsConnected(request, context)

    async def SubscribeHeartbeat(self,
                                 empty_request,
                                 context: grpc.aio.ServicerContext
                                 ) -> None:
        q = self.__crete_async_queue()
        self.heartbeat_consumers[context.peer()] = q
        self.log.info(f'Client {context.peer()} just subscribed to heartbeat messages')

        while True:
            try:
                msg = Backend_pb2.HeartbeatItem()
                msg.NodeID, msg.State, timestamp = await q.get()
                msg.dateTimeStamp.FromNanoseconds(int(timestamp *
                                                      google.protobuf.internal.well_known_types._NANOS_PER_SECOND))
                await context.write(msg)
            except grpc.RpcError:
                break

        del self.heartbeat_consumers[context.peer()]
        self.log.info(f'Client {context.peer()} just unsubscribed from heartbeat messages')

    async def LSSStartFastScan(self,
                               lssid: Backend_pb2.LSSID,
                               context: grpc.aio.ServicerContext
                               ) -> None:
        def build_result_msg():
            return Backend_pb2.ScanProgress()

        async def _report_scan_progress(bit_n, field):
            msg = build_result_msg()

            progress = Backend_pb2.ScanProgress.ScanProgressStateus()
            progress.Field = field
            progress.BitN = bit_n

            msg.Progress.CopyFrom(progress)

            await context.write(msg)

        async def _report_scan_result(vid, pid, rev, serial):
            msg = build_result_msg()

            lssid = Backend_pb2.LSSID()
            lssid.VendorID = vid
            lssid.ProductID = pid
            lssid.Revision = rev
            lssid.Serial = serial
            lssid.KnownFields = Backend_pb2.LSSID.KnownFieldsID.KNOWN_VID | \
                                Backend_pb2.LSSID.KnownFieldsID.KNOWN_PID | \
                                Backend_pb2.LSSID.KnownFieldsID.KNOWN_REV | \
                                Backend_pb2.LSSID.KnownFieldsID.KNOWN_SER

            msg.LssResult.CopyFrom(lssid)

            await context.write(msg)

        async def _report_scan_error(error):
            msg = build_result_msg()

            errmsg = Backend_pb2.CanError()
            errmsg.Message = error

            msg.ErrorMessage.CopyFrom(errmsg)
            self.log.error(f'Fast scan error: {error}')

            await context.write(msg)

        self.log.warning(f'Client {context.peer()} starts Fast Scan...')
        try:
            await self.connection.fast_scan(lssid, _report_scan_progress, _report_scan_result, _report_scan_error)
            self.log.warning(f'Fast Scan finished')
        except AssertionError:
            self.log.error(f'Fast Scan finished')

            msg = build_result_msg()

            msg.BusyReject.CopyFrom(google.protobuf.empty_pb2.Empty())
            self.log.error(f'Fast scan eAlready in progress')

            await context.write(msg)

    def LSSFindUnusedNodeID(self, request, context):
        res = Backend_pb2.NodeIDResult()
        try:
            res.NodeID = self.connection.find_unused_node_id()
        except CanError as ex:
            self.log.error(ex)
            res.Error.CopyFrom(self.__ex2error(ex))

        return res

    def LSSSwitchState(self, request, context):
        res = Backend_pb2.Result()

        try:
            self.connection.lss_switch_state(request.TargetNodeID, request.state)
        except CanError as ex:
            self.log.error(ex)
            res.Error.CopyFrom(self.__ex2error(ex))

        return res

    def LSSConfigureNodeID(self, request, context):
        res = Backend_pb2.NodeIDResult()

        try:
            res.NodeID = self.connection.lss_configure_node_id(request.NodeID)
        except CanError as ex:
            self.log.error(ex)
            res.Error.CopyFrom(self.__ex2error(ex))

        return res

    def LSSSaveSettings(self, request, context):
        res = Backend_pb2.Result()
        try:
            self.connection.lss_save_settings()
        except CanError as ex:
            self.log.error(ex)
            res.Error.CopyFrom(self.__ex2error(ex))

        return res

    def LSSConfigureBusSpeed(self, request, context):
        res = Backend_pb2.Result()
        try:
            self.connection.lss_configure_bus_speed(request.Speed)
        except CanError as ex:
            self.log.error(ex)
            res.Error.CopyFrom(self.__ex2error(ex))

        return res

    def LSSApplyBusSpeed(self, request, context):
        res = Backend_pb2.Result()

        try:
            self.connection.apply_bus_speed()
        except CanError as ex:
            self.log.error(ex)
            res.Error.CopyFrom(self.__ex2error(ex))

        return res

    def GetSyncConfig(self, empty_request, context):
        res = Backend_pb2.SyncConfigResult()

        config = Backend_pb2.SyncConfig()
        config.CobID, config.SyncPeriod, config.Enabled = self.connection.get_sync_config()
        self.log.info(f'{context.peer()} reads SYNC config')

        res.Value.CopyFrom(config)
        return res

    def SetSyncConfig(self, request: Backend_pb2.SyncConfig, context):
        res = Backend_pb2.Result()

        try:
            self.connection.set_sync_config(request.CobID, request.SyncPeriod, request.Enabled)
        except Exception as ex:
            self.log.error(f'Failed to set sync config: {ex}')
            res.Error.CopyFrom(self.__ex2error(ex))

        return res

    def NMTSendCommand(self, request, context):
        command_code_map = {
            Backend_pb2.NMTCommand.NMTCommandCode.GOTO_OPERTIONAL: 1,
            Backend_pb2.NMTCommand.NMTCommandCode.GOTO_PRE_OPERATIONAL: 128,
            Backend_pb2.NMTCommand.NMTCommandCode.GOTO_STOPPED: 2,
            Backend_pb2.NMTCommand.NMTCommandCode.RESET_NODE: 129,
            Backend_pb2.NMTCommand.NMTCommandCode.RESET_COMMUNICATION: 130,
        }

        res = Backend_pb2.Result()

        try:
            self.connection.nmt_command(request.TargetNodeID, command_code_map[request.Command])
        except CanError as ex:
            self.log.error(ex)
            res.Error.CopyFrom(self.__ex2error(ex))

        return res

    def SDOReadIndex(self, request, context):
        res = Backend_pb2.SDOReadResult()

        try:
            self.connection.sdo_read(res.Value, request.ValueType, request.TargetNodeID, request.Index)
        except CanError as ex:
            self.log.error(
                f"Failed to read data ({request.ValueType}) from {request.Index.Index:X}sub{request.Index.Subindex}, {ex}")
            res.Error.CopyFrom(self.__ex2error(ex))
        except Exception as ex:
            self.log.error(f"SDO read: Unknown exception {ex}")
            res.Error.CopyFrom(self.__ex2error(ex))

        return res

    def SDOWriteIndex(self, request: Backend_pb2.SDOWriteRequest, context):
        res = Backend_pb2.Result()

        try:
            self.connection.sdo_write(request.TargetNodeID, request.DestIndex, request.Value)
        except CanError as ex:
            self.log.error(
                f"Failed to write {request.ValueType} to {request.Index.Index:X}sub{request.Index.Subindex}, {ex}")
            res.Error.CopyFrom(self.__ex2error(ex))
        except Exception as ex:
            self.log.error(f"SDO write: unknown exception {ex}")
            res.Error.CopyFrom(self.__ex2error(ex))

        return res

    async def SubscribeEmergency(self,
                                 empty,
                                 context: grpc.aio.ServicerContext
                                 ) -> None:
        q = self.__crete_async_queue()
        self.emcy_consumers[context.peer()] = q
        self.log.info(f'Client {context.peer()} just subscribed to emergency messages')

        while True:
            try:
                msg = Backend_pb2.EMCYItem()
                msg.NodeId, msg.ErrorRegister, msg.EmcyErrorCode, msg.ManufacturerSpec, timestamp = await q.get()

                msg.Timestamp.FromNanoseconds(int(timestamp *
                                                  google.protobuf.internal.well_known_types._NANOS_PER_SECOND))
                await context.write(msg)
            except grpc.RpcError:
                break

        del self.emcy_consumers[context.peer()]
        self.log.info(f'Client {context.peer()} just unsubscribed to emergency messages')

    async def PDOSubscribe(self,
                           request: Backend_pb2.PDOSubscribeInfo,
                           context: grpc.aio.ServicerContext
                           ) -> None:
        q = self.__crete_async_queue()

        def __pdo_rx_callback(can_id, data, timestamp):
            self.loop.call_soon_threadsafe(q.put_nowait, data)

        self.connection.subscribe(request.CobId, __pdo_rx_callback)
        self.log.info(f'Client {context.peer()} just subscribed to PDO messages with COB-ID={request.CobId}')

        while True:
            try:
                msg = Backend_pb2.PDOMessage()
                msg.CobId = request.CobId
                msg.Data = bytes(await q.get())

                await context.write(msg)
            except grpc.RpcError:
                break

        self.connection.unsubscribe(request.CobId, __pdo_rx_callback)
        self.log.info(f'Client {context.peer()} just unsubscribed from PDO messages with COB-ID={request.CobId}')

    @staticmethod
    def __crete_async_queue():
        #return asyncio.Queue(loop=asyncio.get_event_loop())
        return asyncio.Queue()

    @staticmethod
    def __ex2error(exception):
        err = Backend_pb2.CanError()
        err.Message = str(exception)
        return err
