
class CanError(IOError):
    def __init__(self, text):
        super(CanError, self).__init__(text)
