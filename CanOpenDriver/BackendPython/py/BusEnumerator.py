
import netifaces
import re


def enumerate_socketcan_buses():
    expr = re.compile(".*can.*")

    return filter(lambda _if: expr.match(_if) is not None, netifaces.interfaces())
