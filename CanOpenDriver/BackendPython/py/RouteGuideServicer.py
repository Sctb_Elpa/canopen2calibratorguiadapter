import re
import queue

import Backend_pb2
import Backend_pb2_grpc

from Connection import Connection
from ConnectionSocketcan import ConnectionSocketcan
from CanError import CanError

Drivers = {
    ConnectionSocketcan.connection_type: ConnectionSocketcan
}


class RouteGuideServicer(Backend_pb2_grpc.ConnectionServicer):
    """Provides methods that implement functionality of route guide server."""

    def __init__(self):
        self.connection = None
        self.heartbeat_consumers = {}

    @staticmethod
    def _enumerate_connections():
        i = 0

        # socketcan
        for bus in ConnectionSocketcan.enumerate():
            c = Backend_pb2.CANConnectionID()
            c.ID = i
            c.Uri = Connection.build_connection_uri(ConnectionSocketcan.connection_type, bus)
            c.Features = Backend_pb2.ConnectionFetures.NONE
            i += 1

            yield c

    # ------------------------------------------------------

    def _connect_to(self, bus_uri, bus_id):
        m = re.search(r'(.*)://(.*)', bus_uri)
        if not m:
            raise CanError('Bus Uri={} is invalid'.format(bus_uri))
        self.connection = Drivers[m[1]](m[2], self.heartbeat_consumers, bus_id)

    # ------------------------------------------------------

    def GetConnectionList(self, request, context):
        return RouteGuideServicer._enumerate_connections()

    def IsConnected(self, request, context):
        status = Backend_pb2.ConnectionStatus()
        status.connected = self.connection.is_connected() if self.connection else False
        status.Speed = 0

        connection_state = Backend_pb2.CANConnectionID()
        if self.connection:
            connection_state.ID, connection_state.Uri, connection_state.Features = \
                self.connection.dump_connection_status()
        else:  # Заглушка
            connection_state.ID = 0
            connection_state.Uri = ""
            connection_state.Features = Backend_pb2.ConnectionFetures.NONE

        status.Connection.CopyFrom(connection_state)

        return status

    def SubscribeHeartbeat(self, request, context):
        if not self.connection:
            return

        q = queue.Queue()

        self.heartbeat_consumers[context] = q
        while True:
            try:
                msg = q.get(block=True, timeout=0.01)

                hbmsg = Backend_pb2.HeartbeatItem()
                hbmsg.dateTimeStamp = None
                hbmsg.NodeID = msg.NodeID
                hbmsg.State = Backend_pb2.NodeState(msg.state)
                yield hbmsg
            except queue.Empty:
                pass
            except Exception:
                break

        self.heartbeat_consumers.pop(context)

    def ConnectBus(self, request, context):
        buses = RouteGuideServicer._enumerate_connections()

        try:
            while True:
                b = next(buses)
                if b.ID == request.Id:
                    break
            self._connect_to(b.Uri, b.ID)
        except StopIteration:
            pass

        return self.IsConnected(request, context)
