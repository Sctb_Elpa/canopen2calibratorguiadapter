
import struct

import canopen
import canopen.emcy


class MyNetwork(canopen.Network):
    def __init__(self):
        super().__init__()
        self.on_heartbeat = None
        self.on_emcy = None

    def notify(self, can_id, data, timestamp):
        self.notify_if_emcy(can_id, data, timestamp)
        self.notify_if_heartbeat(can_id, data, timestamp)
        super().notify(can_id, data, timestamp)

    def notify_if_emcy(self, can_id, data, timestamp):
        if self.on_emcy and self.__get_service(can_id) == 0x80 and len(data) == 8:
            code, register, data = canopen.emcy.EMCY_STRUCT.unpack(data)
            self.on_emcy(self.__get_node_id(can_id), register, code, data, timestamp)

    def notify_if_heartbeat(self, can_id, data, timestamp):
        if self.on_heartbeat and self.__get_service(can_id) == 0x700:  # is heartbeat msg?
            new_state, = struct.unpack_from("B", data)
            self.on_heartbeat(self.__get_node_id(can_id), new_state & 0x7F, timestamp)

    @staticmethod
    def __get_service(can_id):
        return can_id & 0x780

    @staticmethod
    def __get_node_id(can_id):
        return can_id & 0x7F

