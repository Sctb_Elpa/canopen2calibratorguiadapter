from abc import abstractmethod


class Connection(object):
    def __init__(self, hb_consumers, emcy_consumers):
        self._hb_consumers = hb_consumers
        self._emcy_consumers = emcy_consumers

    @abstractmethod
    def is_connected(self):
        raise NotImplementedError()

    @abstractmethod
    def dump_connection_status(self):
        raise NotImplementedError()

    @staticmethod
    def build_connection_uri(driver, channel):
        return '{}://{}'.format(driver, channel)

    @abstractmethod
    def fast_scan(self, initial_lss, scan_progress, scan_result, scan_error):
        raise NotImplementedError()

    @abstractmethod
    def find_unused_node_id(self):
        raise NotImplementedError()

    @abstractmethod
    def lss_switch_state(self, target_node_lss_id, state):
        raise NotImplementedError()

    @abstractmethod
    def lss_configure_node_id(self, node_id):
        raise NotImplementedError()

    @abstractmethod
    def lss_configure_bus_speed(self, speed_grade):
        raise NotImplementedError()

    @abstractmethod
    def apply_bus_speed(self):
        raise NotImplementedError()

    @abstractmethod
    def lss_save_settings(self):
        raise NotImplementedError()

    @abstractmethod
    def nmt_command(self, target_node_id, command):
        raise NotImplementedError()

    @abstractmethod
    def get_sync_config(self):
        raise NotImplementedError()

    @abstractmethod
    def set_sync_config(self, cob_id, period, enabled):
        raise NotImplementedError()

    @abstractmethod
    def sdo_read(self, value, value_type, node_id, index):
        raise NotImplementedError()

    @abstractmethod
    def sdo_write(self, node_id, index, value):
        raise NotImplementedError()

    @abstractmethod
    def subscribe(self, cob_id, callback):
        raise NotImplementedError()

    @abstractmethod
    def unsubscribe(self, cob_id, callback):
        raise NotImplementedError()
