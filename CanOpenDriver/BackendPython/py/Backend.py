
import argparse
from concurrent import futures
import logging

import grpc
import Backend_pb2_grpc

from RouteGuideServicer import RouteGuideServicer


def serve():
    parser = argparse.ArgumentParser(description='pathon-canopen to gRPC server')

    parser.add_argument('--grpc-port', dest='port', type=int, required=False, default=1889, help="gRPC port to listen")

    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=2))
    Backend_pb2_grpc.add_ConnectionServicer_to_server(
        RouteGuideServicer(), server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    log = logging.getLogger("serve")
    log.setLevel(logging.INFO)
    log.info("Python CanOpen server listening on port {}".format(args.port))
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    logging.basicConfig()
    serve()
