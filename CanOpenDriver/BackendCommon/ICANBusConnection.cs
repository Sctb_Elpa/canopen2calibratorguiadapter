﻿using System;

namespace CanopenBackend.Interaces
{
	public interface ICANBusConnection : IDisposable
	{
		#region Properties

		string Bus { get; }
		int BusSpeed { get; }

		#endregion Properties

		#region Methods

		ILSSMaster getLssMaster();

		IFastScaner getFastScaner();

        /*
		IDeviceWatcher GetDeviceWatcher();

		ISDOClient GetSDOClient();

		INMTmaster getNMTService();

		NodeChecker getNodeCHecker();
		*/

        #endregion Methods
    }
}