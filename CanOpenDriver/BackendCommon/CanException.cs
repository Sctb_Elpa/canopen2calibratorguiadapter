﻿using System;

namespace CanopenBackend.Interaces
{
	public class CanException : Exception
	{
		public CanException(string text) : base(text)
		{
		}
		public CanException(string text, Exception ex) : base(text, ex)
		{
		}
	}
}