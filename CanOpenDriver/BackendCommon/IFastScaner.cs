﻿using System;
using Utils.LSS;

namespace CanopenBackend.Interaces
{
	public interface IFastScaner : IDisposable
	{
		#region Properties

		ProgressState ProgressState { get; }

		#endregion Properties

		#region Methods

		UniqueLSSId GetResult();

		bool IsBusy();

		void Start(UniqueLSSId uniqueLSSId);

		void Join();

		#endregion Methods
	}

	public class ProgressState
	{
		#region Properties

		public int Bit { get; set; }
		public UniqueLSSId.KnownBits Field { get; set; }

		#endregion Properties
	}
}