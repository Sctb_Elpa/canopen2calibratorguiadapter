﻿using System;
using Utils.LSS;

namespace CanopenBackend.Interaces
{
	public abstract class AbstrcatLSSFastScanScaner : IDisposable
	{
		#region Fields

		public static readonly byte CS_FAST_SCAN = 0x51;

		public static readonly byte CS_IDENTIFY_SLAVE = 0x4F;

		public static readonly uint LSS_RX_COBID = 0x7E4;

		public static readonly uint LSS_TX_COBID = 0x7E5;

		#endregion Fields

		#region Delegates

		public delegate void ReportProgress(byte lss_bit_check, byte lss_sub);

		#endregion Delegates

		#region Methods

		public abstract void Dispose();

		public abstract bool FindDevice(out UniqueLSSId lss_id, ReportProgress reporter = null);
		public abstract bool FindDevicePartialyKnown(ref UniqueLSSId lss_id, ReportProgress reporter = null);

		#endregion Methods
	}
}