﻿using Utils.LSS;

namespace CanopenBackend.Interaces
{
	public interface ILSSMaster
	{
		#region Methods

		void BroadcastWaiting(); /* throws CanException */
		void BroadcastConfiguration(); /* throws CanException */
		void SetSpeed(int busSpeed); /* throws CanException */
		void StoreConfiguration(); /* throws CanException */
		void ApplyBitrate(); /* throws CanException */
		void Configuration(UniqueLSSId lssID); /* throws CanException */
		void SetNodeID(byte newNodeID); /* throws CanException */

		#endregion Methods
	}
}