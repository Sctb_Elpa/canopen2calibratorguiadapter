﻿namespace Common
{
	public static class Defaults
	{
		public const int DefaultCanBitrate = 50000;
		public const int DefaultWebPort = 1771;
		public const int DefaultgRPCPort = 1779;
        public const int DefaultDriverPort = 8923;
    }
}
