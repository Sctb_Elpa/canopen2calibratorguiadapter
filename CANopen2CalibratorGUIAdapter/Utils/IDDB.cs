﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Utils;
using Utils.LSS;

namespace CANopen2CalibratorGUIAdapter.Utils
{
	public static class IDDB
	{
		public static CanOpenRegistredVendor GetVendorByID(uint vendorID)
		{
			var res = StaticData.Vandors.TryGetValue(vendorID, out CanOpenRegistredVendor vid);
			if (!res)
			{
				vid = new CanOpenRegistredVendor();
			}
			return vid;
		}

		public static string GetDeviceNameByID(uint vendorID, uint productID)
		{
			if (!StaticData.Products.TryGetValue(vendorID, out Dictionary<uint, string> product_dict))
			{
				return null;
			}
			if (!product_dict.TryGetValue(productID, out string product))
			{
				return null;
			}
			return product;
		}

		public static Result<IEdsDetector> TryGetEdsDetectorByDeviceID(UniqueLSSId id)
		{
			var featureID = id.ToFeatureID();

			var detected = (from knowndev in StaticData.DevicesWithEDSDetectors
							let similarityIndex = knowndev.Key.CompareTo(featureID)
							orderby similarityIndex
							select (knowndev.Value, similarityIndex))
					  .LastOrDefault();

			if (detected.similarityIndex == 0)
			{
				return Result.Fail<IEdsDetector>("Unknown device LSS id");
			}
			else
			{
				return Result.Ok(detected.Value);
			}
		}

		public static string GetEdsFile(string eds_name)
		{
			var path = Path.Combine(StaticData.EdsPath, eds_name).Replace(":/", ":///");
			return new Uri(path).LocalPath;
		}
	}
}