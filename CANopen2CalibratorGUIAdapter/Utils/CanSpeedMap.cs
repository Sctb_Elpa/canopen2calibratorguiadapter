﻿using System.Collections.Generic;
using Utils;

namespace CANopen2CalibratorGUIAdapter.Utils
{
    public static class CanSpeedMap
    {
        public static readonly Dictionary<int, int> SpeedMap = new Dictionary<int, int>();

        static CanSpeedMap()
        {
            foreach (var key in StaticData.speedMap.Keys)
            {
                if (key != 0)
                {
                    SpeedMap.Add(StaticData.speedMap.Forward[key], key / 1000);
                }
            }
        }
    }
}