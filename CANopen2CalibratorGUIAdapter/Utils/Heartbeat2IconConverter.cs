﻿using System;

namespace CANopen2CalibratorGUIAdapter.Utils
{
    public static class Heartbeat2IconConverter
    {
        public static string ToHeartbeatIconColor(this TimeSpan timeSpan)
        {
            if (timeSpan < TimeSpan.FromSeconds(1))
            {
                return "success";
            }
            else if (timeSpan < TimeSpan.FromSeconds(5))
            {
                return "warning";
            }
            else
            {
                return "danger";
            }
        }

        public static string ToHeartbeatTooltipText(this TimeSpan timeSpan)
        {
            if (timeSpan < TimeSpan.FromSeconds(1))
            {
                return "Менее секунды назад";
            }
            else if (timeSpan < TimeSpan.FromSeconds(5))
            {
                return $"{timeSpan:s\\.f} секунд назад";
            }
            else
            {
                return $"{timeSpan:mm\\:ss} секунд назад";
            }
        }
    }
}
