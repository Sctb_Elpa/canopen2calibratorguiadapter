﻿using System;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;

namespace CANopen2CalibratorGUIAdapter.Models
{
    public class EmergencyLogItem
    {
        public int Id { get; set; }
        public byte NodeId { get; set; }
        public DateTime Timestamp { get; set; }
        public uint ErrorRegister { get; set; }
        public ushort EmcyErrorCode { get; set; }
        public string ManufacturerSpec { get; set; }
        public string EmcyErrorDescr => EmergencyDecoder.GetErrorDescription(EmcyErrorCode);
    }
}
