﻿using Utils.OD;

namespace CANopen2CalibratorGUIAdapter.Models
{
    public class SDOIndex
    {
        public SDOIndex(ODIndex index)
        {
            ParentId = index.Subindex == 0 ? null : new ODIndex { Index = index.Index }.ToString();
            Index = index.ToString();
        }

        public string Index { get; set; }

        // imageHtml - htmlкоторый будет использован как иконка '<i class="material-icons">folder</i>'
        public string ImageHtml { get; set; }

        // parentId - идентификатор перента (нужен для ленивой загрузки)
        public string ParentId { get; set; }

        // text - текстовой содержимое элемента
        public string Text { get; set; }

        // hasChildren - есть субиндексы?
        public bool HasChildren { get; set; } = false;
    }
}
