﻿using Utils.OD;

namespace CANopen2CalibratorGUIAdapter.Models.SDO
{
    public class SDOIndexModel
    {
        public bool IsSubindex { get; set; }
        public ODIndex Index { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
    }
}