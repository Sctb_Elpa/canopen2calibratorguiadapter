﻿namespace CANopen2CalibratorGUIAdapter.Models
{
    public class SYNCConfig
    {
        public bool Enabled { get; set; }
        public uint CobID { get; set; }
        public ushort SyncPeriod { get; set; }

        public override string ToString() => $"Enabled: {Enabled}, Period: {SyncPeriod}, COB-ID: {CobID}";
    }
}
