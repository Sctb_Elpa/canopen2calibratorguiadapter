﻿namespace CANopen2CalibratorGUIAdapter.Models
{
    public class AppImageUpdateData
    {
        public string data { get; set; }
        public bool encrypted { get; set; }
    }
}
