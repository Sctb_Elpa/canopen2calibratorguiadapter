﻿namespace CANopen2CalibratorGUIAdapter.Models.LssModels
{
    public class LssSetupNetworkSpeedModel
    {
        public bool SpeedControlSupported { get; set; }

        public int? SpeedID { get; set; }
    }
}
