﻿
namespace CANopen2CalibratorGUIAdapter.Models
{
    public class LssModel
    {
        public bool Connected { get; set; }

        public bool SpeedSetupSupported { get; set; }

        public int? CurrentSpeedGrade { get; set; }

        public string CurrentNetworkConfig { get; set; }
    }
}
