﻿using System;

namespace CANopen2CalibratorGUIAdapter.Models.DeviceCardModels
{
	public class DeviceCardBootloaderFooterModel
	{
		public byte NodeID { get; set; }
		public string AppID { get; set; }
        public Version Version { get; internal set; }
    }
}