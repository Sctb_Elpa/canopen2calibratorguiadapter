﻿namespace CANopen2CalibratorGUIAdapter.Models.DeviceCardModels
{
    public class DeviceCardSDOSummaryFooterModel
    {
        public byte NodeID { get; set; }

        public int MondatoryObjectsCount { get; set; }

        public int OptionalObjectsCount { get; set; }

        public int ManufacturerObjectsCount { get; set; }

        public int IndexCount => MondatoryObjectsCount + OptionalObjectsCount + ManufacturerObjectsCount;
    }
}
