﻿using System;

namespace CANopen2CalibratorGUIAdapter.Models.DeviceCardModels
{
    public class TPDOItem
    {
        public string Index { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public DateTime? Timestamp { get; set; }

        public override string ToString()
        {
            return $"[{Timestamp}]\"{Name}\" ({Index}): {Value}";
        }
    }
}
