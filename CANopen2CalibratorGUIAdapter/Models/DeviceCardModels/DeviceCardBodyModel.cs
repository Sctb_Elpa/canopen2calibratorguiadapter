﻿namespace CANopen2CalibratorGUIAdapter.Models.DeviceCardModels
{
	public class DeviceCardBodyModel
	{
		public byte NodeID { get; set; }
		public string MarkClass { get; internal set; }
		public string Vendor { get; internal set; }
		public string Product { get; internal set; }
		public uint Revision { get; internal set; }
		public uint Serial { get; internal set; }
		public string EDSButtonTooltip { get; internal set; }
	}
}
