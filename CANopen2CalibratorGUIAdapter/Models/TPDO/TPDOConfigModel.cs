﻿namespace CANopen2CalibratorGUIAdapter.Models.TPDO
{
    public class TPDOConfigModel
    {
        public int MappingID { get; set; }
        public bool Enabled { get; set; }
        public ushort Communication { get; set; }
        public ushort Mapping { get; set; }
        public uint CobId { get; set; }
        public byte TransferTypeID { get; set; }
        public string TransferType { get; set; }
        public ushort Inhibit { get; set; }
        public ushort IventTimer { get; set; }
        public byte SyncStart { get; set; }
        public int MaxMappingSize { get; set; }
        public bool ConfigEnabled { get; set; }
        public bool MappingEnabled { get; set; }
    }
}
