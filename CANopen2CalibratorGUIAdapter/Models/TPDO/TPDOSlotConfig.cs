﻿using CANopen2CalibratorGUIAdapter.CANOpenClient.PDO;

namespace CANopen2CalibratorGUIAdapter.Models.TPDO
{
    public class TPDOSlotConfig
    {
        public bool Enabled { get; set; }
        public uint CobId { get; set; }
        public byte TransferTypeID { get; set; }
        public ushort Inhibit { get; set; }
        public ushort IventTimer { get; set; }
        public byte SyncStart { get; set; }

        public override string ToString()
            => $"COB-ID: 0x{CobId:X}, Enabled: {Enabled} " +
            $"TransferType: {TPDOManager.TranferTypeToString(TransferTypeID)}, " +
            $"Inhibit: {Inhibit}, IventTimer: {IventTimer}, SyncStart: {SyncStart}";
    }
}
