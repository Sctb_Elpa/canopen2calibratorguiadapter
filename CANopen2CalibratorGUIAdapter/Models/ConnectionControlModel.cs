﻿namespace CANopen2CalibratorGUIAdapter.Models
{
    public class ConnectionControlModel
    {
        public bool NowConnected { get; set; }

        public int CanConnection { get; set; }

        public int Speed { get; set; }
    }
}
