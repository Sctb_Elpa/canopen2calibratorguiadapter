﻿using System;

namespace CANopen2CalibratorGUIAdapter.Models.Bootloader
{
    public class BootloaderSummary
    {
        public Version BootladerVersion { get; set; }

        public string AppCRC32 { get; set; }

        public bool AppValid { get; set; }
    }
}
