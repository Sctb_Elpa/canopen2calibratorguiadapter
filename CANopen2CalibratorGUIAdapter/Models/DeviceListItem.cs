﻿using System;
using System.Collections.Generic;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using Utils.LSS;
using Utils.NMT;

namespace CANopen2CalibratorGUIAdapter.Models
{
    public class DeviceListItem
    {
        private static readonly Dictionary<NMTState, string> state2borderClass = new Dictionary<NMTState, string> {
            { NMTState.Unknown, "secondary" },
            { NMTState.BootUp, "primary" },
            { NMTState.Operatuional, "success" },
            { NMTState.Stopped, "danger" },
            { NMTState.PreOperational, "warning " }
        };

        public byte NodeID { get; set; }

        public NMTState NMTState { get; set; }

        public UniqueLSSId LSSId { get; set; }

        public TimeSpan TimeFromLastHeartbeat { get; set; }

        public string MarkClass => state2borderClass[NMTState];

        public string VendorString { get; set; }

        public string ProductString { get; set; }

        public IBootloaderController BootloaderController { get; set; }

        public EdsInfo EdsInfo { get; set; }

        public bool DeviceKnown { get; set; }
    }
}