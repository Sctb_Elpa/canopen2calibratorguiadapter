﻿using System.Collections.Generic;

namespace CANopen2CalibratorGUIAdapter.Models
{
    public class ConnectionViewModel
    {
        public bool Connected { get; set; }

        public bool SpeedSetupSupported { get; set; }

        public IDictionary<int, string> CanBusses { get; set; }

        public int? ConnectionID { get; set; }

        public int? SpeedID { get; set; }
    }
}
