﻿namespace CANopen2CalibratorGUIAdapter.Models.WebRPC
{
    public class FastScanResult
    {
        #region Properties

        public uint BitN { get; set; }
        public string Error { get; set; }
        public string FieldName { get; set; }
        public uint ProductID { get; set; }
        public uint RevisionNumber { get; set; }
        public uint SerialNumber { get; set; }
        public string Status { get; set; }
        public uint VendorID { get; set; }

        #endregion Properties
    }
}