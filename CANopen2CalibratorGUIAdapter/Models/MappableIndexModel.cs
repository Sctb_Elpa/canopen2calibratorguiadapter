﻿namespace CANopen2CalibratorGUIAdapter.Models
{
    public class MappableIndexModel
    {
        public string Index { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int BitSize { get; set; }
        public bool Mapped { get; set; }
        public byte? BitOffset { get; set; }
    }
}
