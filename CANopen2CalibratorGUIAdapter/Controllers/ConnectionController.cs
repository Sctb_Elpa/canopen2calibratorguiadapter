﻿using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CANopen2CalibratorGUIAdapter.Controllers
{
    public class ConnectionController : Controller
    {
        #region Fields

        private readonly ILogger<ConnectionController> _logger;
        private readonly IBackendClient Backend;

        private int? lastUsedConnectionID = null;
        private int? lastUsedConnectionSpeedID = 6;

        #endregion Fields

        #region Constructors

        public ConnectionController(IBackendClient backend, ILogger<ConnectionController> logger)
        {
            Backend = backend;
            _logger = logger;
        }

        #endregion Constructors

        #region Methods

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var connected = Backend.Connected;

            return View(new ConnectionViewModel
            {
                Connected = connected,
                SpeedSetupSupported = Backend.SpeedSetupSupported,
                CanBusses = await Backend.GetCanBusListAsync(),
                ConnectionID = connected
                    ? Backend.ConnectionID
                    : lastUsedConnectionID,
                SpeedID = (Backend.SpeedSetupSupported && connected)
                    ? Backend.SpeedID
                    : lastUsedConnectionSpeedID,
            });
        }

        [HttpGet]
        public IActionResult ConnectionError(string error) => View((object)error);

        [HttpPost]
        public async Task<IActionResult> ControlAsync(ConnectionControlModel model)
        {
            var request = model.NowConnected ? "Disconnect" : $"Connect to {model.CanConnection}";
            _logger.LogInformation($"{request} requested");

            if (!model.NowConnected)
            {
                if (Backend.Connected)
                {
                    return Redirect("Overview");
                }

                var result = await Backend.ConnectAsync(model.CanConnection, model.Speed);
                if (result.Failure)
                {
                    return RedirectToAction("ConnectionError", new { @error = result.Error });
                }

                return RedirectToAction("Index", "Overview");
            }
            else
            {
                var result = await Backend.DisconnectAsync();
                if (result.Failure)
                {
                    return RedirectToAction("ConnectionError", new { @error = result.Error });
                }
            }
            return RedirectToAction("Index");
        }

        #endregion Methods
    }
}