﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CANopen2CalibratorGUIAdapter.Controllers
{
    public class DebugController : Controller
    {
        private readonly ILogger<DebugController> _logger;
        private readonly Channel Channel;

        private readonly Google.Protobuf.WellKnownTypes.Empty Empty = new Google.Protobuf.WellKnownTypes.Empty();

        public DebugController(ILogger<DebugController> logger, Options options)
        {
            _logger = logger;
            Channel = new Channel(options.CanDriver, ChannelCredentials.Insecure);
        }

        private Ru.Sctbelpa.CanopenDriver.Connection.ConnectionClient connect() {
            return new Ru.Sctbelpa.CanopenDriver.Connection.ConnectionClient(Channel);
        }
        
        [HttpGet]
        public async Task<IActionResult> ConnectionsListAsync()
        {
            using (var responseStream = connect().GetConnectionList(Empty))
            {
                List<Ru.Sctbelpa.CanopenDriver.CANConnectionID> items = 
                    new List<Ru.Sctbelpa.CanopenDriver.CANConnectionID>();

                var sb = new StringBuilder();

                await foreach (var c in responseStream.ResponseStream.ReadAllAsync())
                {
                    items.Add(c);
                }

                return Json(items);
            }
        }

        [HttpGet]
        public async Task<IActionResult> IsConnectedAsync() {
            var resp = await connect().IsConnectedAsync(Empty);
            return Json(resp);
        }
    }
}