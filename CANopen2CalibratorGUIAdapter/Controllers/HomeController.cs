﻿using System.Diagnostics;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CANopen2CalibratorGUIAdapter.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBackendClient Backend;
        private readonly ILogger<HomeController> _logger;

        public HomeController(IBackendClient backend, ILogger<HomeController> logger)
        {
            Backend = backend;
            _logger = logger;
        }

        public IActionResult Index() => Backend.Connected
                ? RedirectToAction("Index", "Overview")
                : RedirectToAction("Index", "Connection");

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}