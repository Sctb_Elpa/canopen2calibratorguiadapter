﻿using System.Linq;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CANopen2CalibratorGUIAdapter.Controllers
{
    public class Emergency : Controller
    {
        private readonly ILogger<Emergency> _logger;
        private readonly IBackendClient Backend;

        public Emergency(IBackendClient backend, ILogger<Emergency> logger)
        {
            Backend = backend;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(new EmergencyModel
            {
                Connected = Backend.Connected
            });
        }

        [HttpGet]
        public IActionResult Log() => Json(Backend.GetEmergencyLog().Messages);

        [HttpGet]
        public IActionResult LogEntry(int id) => Json(Backend.GetEmergencyLog().GetMessage(id));
    }
}
