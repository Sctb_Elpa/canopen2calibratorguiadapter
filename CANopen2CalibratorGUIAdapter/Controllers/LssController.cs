﻿using System.Linq;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CANopen2CalibratorGUIAdapter.Controllers
{
    public class LssController : Controller
    {
        #region Fields

        private readonly ILogger<LssController> _logger;
        private readonly IBackendClient Backend;

        #endregion Fields

        #region Constructors

        public LssController(IBackendClient backend, ILogger<LssController> logger)
        {
            Backend = backend;
            _logger = logger;
        }

        #endregion Constructors

        #region Methods

        [HttpGet]
        public IActionResult Index()
        {
            return base.View(new LssModel
            {
                Connected = Backend.Connected,
                SpeedSetupSupported = Backend.SpeedSetupSupported,
                CurrentSpeedGrade = Backend.SpeedID,

                CurrentNetworkConfig = GetNetworkConfigString()
            });
        }

        [HttpGet]
        public IActionResult NetworkConfig() => Content(GetNetworkConfigString());

        private string GetNetworkConfigString() => string.Join("\n",
                    Backend.Devices.GetAliveDeviceList().Select(x => $"{x.LSSId.ToShortString()}={x.NodeID}"));

        #endregion Methods
    }
}