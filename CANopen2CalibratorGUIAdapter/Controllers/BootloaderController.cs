﻿using System;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using CANopen2CalibratorGUIAdapter.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CANopen2CalibratorGUIAdapter.Controllers
{
    public class BootloaderController : Controller
    {
		private readonly ILogger<ConnectionController> _logger;
		private readonly IBackendClient Backend;

		public BootloaderController(IBackendClient backend, ILogger<ConnectionController> logger)
		{
			Backend = backend;
			_logger = logger;
		}

		[HttpGet]
        public IActionResult Index(int id) => View((byte)id);

        [HttpGet]
        public async Task<IActionResult> GetSettingsBackup(int id)
        {
			IBootloaderController btctrl;
			try
			{
				btctrl = Backend.Devices[(byte)id].BootloaderController;
			} catch {
				_logger.LogError($"Node id {id} is unregistred or not in bootloader state");
				return NotFound();
			}

			var backup = await btctrl.BackupSettings();
			if (backup.Success)
			{
				_logger.LogInformation("Get backup of application settings");
				return File(backup.Value, "application/octet-stream");
			}
			else
			{
				var err = $"Failed to read application settings image {backup.Error}";
				_logger.LogError(err);
				return BadRequest(err);
			}
		}

		[HttpPut]
		public async Task<IActionResult> UploadFirmware(int id, [FromBody] AppImageUpdateData data)
        {
			if (string.IsNullOrEmpty(data.data))
			{
				var msg = "Empty firmware data";
				_logger.LogError(msg);
				return Forbid("Empty data");
			}

			try
			{
				await Backend.Devices[(byte)id].BootloaderController
					.UploadFirmwareImage(Convert.FromBase64String(data.data), data.encrypted);
			}
			catch (BackendCanError ex)
			{
				var msg = "Failed to upload firmware: " + ex.Message;
				_logger.LogError(msg);
				return BadRequest(msg);
			}

			_logger.LogInformation("Firmware udated");
			return new EmptyResult();
		}

        [HttpGet]
        public IActionResult Summary(int id) => ViewComponent("BootloaderSummary", (byte)id);

		[HttpGet]
		public async Task<IActionResult> StartReserveKit(int id) {
			try {
				await Backend.Devices[(byte)id].BootloaderController.StartReservKit();
				Backend.Devices.Remove((byte)id);
			} catch {
				_logger.LogError($"Node id {id} is unregistred or not in bootloader state");
				return NotFound();
			}

			return RedirectToAction("Index", "Overview");
		}
    }
}