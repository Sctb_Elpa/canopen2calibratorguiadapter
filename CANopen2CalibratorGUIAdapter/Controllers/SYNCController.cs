﻿using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CANopen2CalibratorGUIAdapter.Controllers
{
    public class SYNCController : Controller
    {
        private readonly IBackendClient Backend;
        public ILogger<SYNCController> _logger;

        public SYNCController(IBackendClient backend, ILogger<SYNCController> logger)
        {
            Backend = backend;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            if (!Backend.Connected)
            {
                return View(null);
            }

            var config = await Backend.GetSyncConfig();
            if (config.Failure)
            {
                _logger.LogError($"Failed to get SYNC config: {config.Error}");
                return View("Error", config.Error);
            }
            else
            {
                _logger.LogInformation($"Current SYNC config: {config.Value}");
                return View(config.Value);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Configure(SYNCConfig config)
        {
            _logger.LogWarning($"Updating SYNC config: {config}...");
            var res = await Backend.SetSyncConfig(config);
            if (res.Failure)
            {
                _logger.LogError($"Failed: {res.Error}");
            }
            return RedirectToAction("Index");
        }
    }
}
