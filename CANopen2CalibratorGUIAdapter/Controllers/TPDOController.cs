﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.CANOpenClient.PDO;
using CANopen2CalibratorGUIAdapter.Models;
using CANopen2CalibratorGUIAdapter.Models.TPDO;
using libEDSsharp;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Utils.OD;

namespace CANopen2CalibratorGUIAdapter.Controllers
{
    public class TPDOController : Controller
    {
        #region Fields

        private readonly ILogger<TPDOController> _logger;
        private readonly IBackendClient Backend;

        #endregion Fields

        #region Constructors

        public TPDOController(IBackendClient backend, ILogger<TPDOController> logger)
        {
            Backend = backend;
            _logger = logger;
        }

        #endregion Constructors

        #region Methods

        public async Task<IActionResult> Mapping(int id, int mappingId)
        {
            try
            {
                var tpdo_mgr = Backend.Devices[(byte)id].TPDO;
                var pdo_mappable = tpdo_mgr.TPDOMapable();
                var map_info = await tpdo_mgr.GetMapInfo(mappingId - 1);

                var indexes = (from elemnt in pdo_mappable
                               let index = new ODIndex { Index = elemnt.Index, Subindex = (byte)elemnt.Subindex }
                               let offset = GetBitOffset(map_info, elemnt)
                               orderby index
                               select new MappableIndexModel
                               {
                                   Index = index.ToString(),
                                   Mapped = offset != null,
                                   BitOffset = offset,
                                   Name = elemnt.parameter_name,
                                   Type = elemnt.datatype.ToString(),
                                   BitSize = elemnt.Sizeofdatatype()
                               }).ToList();
                return Json(new { records = indexes, total = indexes.Count() });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return NotFound(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult Index(int id) => View((byte)id);

        [HttpGet]
        public async Task<IActionResult> MappingConfig(int id)
        {
            try
            {
                var tpdo_mgr = Backend.Devices[(byte)id].TPDO;
                var cfg = (await tpdo_mgr.TPDOConfig())
                    .Select((slot, index) => new TPDOConfigModel
                {
                    MappingID = index + 1,
                    Enabled = !slot.invalid,
                    Communication = slot.ConfigurationIndex,
                    Mapping = slot.MappingIndex,
                    CobId = slot.COB & 0x0FFFFFFF,
                    TransferTypeID = slot.transmissiontype,
                    TransferType = TPDOManager.TranferTypeToString(slot.transmissiontype),
                    Inhibit = slot.inhibit,
                    IventTimer = slot.eventtimer,
                    SyncStart = slot.syncstart,
                    MaxMappingSize = tpdo_mgr.GetMappingSize(slot.MappingIndex),
                    ConfigEnabled = slot.configAccessType != EDSsharp.AccessType.ro,
                    MappingEnabled = slot.mappingAccessType != EDSsharp.AccessType.ro
                }).ToArray();

                return Json(new { records = cfg, total = cfg.Count() });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return NotFound(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult TPDOModes()
            => Json(TPDOManager.TransferTypeNames.Select(item => new { transferTypeID = item.Key, Text = item.Value }));

        private static byte? GetBitOffset(List<ODentry> mapinfo, ODentry entry)
        {
            byte offset = 0;
            foreach (var index in mapinfo)
            {
                if (index == entry)
                {
                    return offset;
                }
                offset += (byte)index.Sizeofdatatype();
            }
            return null;
        }

        #endregion Methods
    }
}