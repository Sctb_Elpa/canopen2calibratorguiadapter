﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using CANopen2CalibratorGUIAdapter.Models;
using CANopen2CalibratorGUIAdapter.Models.SDO;
using CANopen2CalibratorGUIAdapter.ViewComponents;
using libEDSsharp;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Utils.OD;

namespace CANopen2CalibratorGUIAdapter.Controllers
{
    public class SDOController : Controller
    {
        private static readonly DataType[] not_visible_types =
        {
            DataType.UNKNOWN, DataType.PDO_COMMUNICATION_PARAMETER, DataType.PDO_MAPPING
        };

        private readonly ILogger<SDOController> _logger;
        private readonly IBackendClient Backend;
        private readonly IViewRenderService ViewRenderService;

        private static readonly string listItem_viewComponent = "SDO/Components/SDOIndex/Default";

        public static readonly string ValueIcon = "fa-file";
        public static readonly string FolderIcon = "fa-folder";

        public SDOController(IBackendClient backend, IViewRenderService viewRenderService,
            ILogger<SDOController> logger)
        {
            Backend = backend;
            ViewRenderService = viewRenderService;
            _logger = logger;
        }

        private EdsInfo GetDeviceRegistred(byte id)
        {
            try
            {
                return Backend.Devices[id].Eds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        [HttpGet]
        public IActionResult All(int id)
        {
            if (GetDeviceRegistred((byte)id) == null)
            {
                return NotFound($"Device with NodeID = {id} not registred or unknown");
            }

            return View((byte)id);
        }

        [HttpGet]
        public async Task<IActionResult> Dictionary(int id)
        {
            var eds = GetDeviceRegistred((byte)id);

            var indexes = new List<SDOIndex>();
            foreach (var index_data in eds.Indexes())
            {
                indexes.Add(await BuildIndex(new ODIndex { Index = index_data.Index }, null, index_data, (byte)id));
            }

            return Json(indexes);
        }

        public async Task<IActionResult> LazyDictionary(int id, string parentid)
        {
            if (string.IsNullOrEmpty(parentid))
            {
                return await Dictionary(id);
            }

            ODIndex parentIndex;
            try
            {
                parentIndex = new ODIndex(parentid);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }

            var eds = GetDeviceRegistred((byte)id);

            var res = eds.Indexes().Where(i => i.Index == parentIndex.Index).FirstOrDefault();
            if (res == null || res.subobjects.Count == 0)
            {
                return Json(new object[0]);
            }

            var subindexes = new List<SDOIndex>();
            foreach (var subindex_data in res.subobjects.Values)
            {
                subindexes.Add(await BuildIndex(new ODIndex
                {
                    Index = res.Index,
                    Subindex = (byte)subindex_data.Subindex
                }, parentIndex, subindex_data, (byte)id));
            }

            return Json(subindexes);
        }

        private bool OdentryHasSubindexes(ODentry entry) => entry.subobjects.Count > 0;

        private async Task<string> TryReadIndex(ODIndex index, ODentry entry, byte nodeID)
        {
            string quote(string v) => $"\"{v}\"";

            try
            {
                var sdoClient = Backend.Devices[nodeID].SDOClient;

                return entry.datatype switch
                {
                    DataType.INTEGER8 => (await sdoClient.ReadAsync<sbyte>(index)).ToString(),
                    DataType.INTEGER16 => (await sdoClient.ReadAsync<short>(index)).ToString(),
                    DataType.INTEGER32 => (await sdoClient.ReadAsync<int>(index)).ToString(),
                    DataType.UNSIGNED8 => (await sdoClient.ReadAsync<byte>(index)).ToString(),
                    DataType.UNSIGNED16 => (await sdoClient.ReadAsync<ushort>(index)).ToString(),
                    DataType.UNSIGNED32 => (await sdoClient.ReadAsync<uint>(index)).ToString(),
                    DataType.REAL32 => (await sdoClient.ReadAsync<float>(index)).ToString(),
                    DataType.REAL64 => (await sdoClient.ReadAsync<double>(index)).ToString(),
                    DataType.VISIBLE_STRING => quote(Encoding.ASCII.GetString(await sdoClient.ReadBytesAsync(index))),
                    DataType.OCTET_STRING => (await sdoClient.ReadBytesAsync(index)).ToDumpString(),
                    DataType.UNICODE_STRING => quote(Encoding.UTF8.GetString(await sdoClient.ReadBytesAsync(index))),
                    DataType.INTEGER64 => (await sdoClient.ReadAsync<Int64>(index)).ToString(),
                    DataType.UNSIGNED64 => (await sdoClient.ReadAsync<UInt64>(index)).ToString(),
                    DataType.BOOLEAN => "<input type=\"checkbox\" disabled " + 
                        $"{(await sdoClient.ReadAsync<bool>(index) ? "checked" : "")} />",
                    DataType.DOMAIN => $"<a href=\"javascript:getDomainData({nodeID}, '{index}');\">Скачать</a>",
                    _ => entry.datatype.ToString() + " is not visible",
                };
            }
            catch (Exception ex)
            {
                return $"<span class=\"text-danger\">{ex.Message}</span>";
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetDomain(int id, string index)
        {
            try
            {
                var idx = new ODIndex(index);
                var sdoClient = Backend.Devices[(byte)id].SDOClient;
                var eds = GetDeviceRegistred((byte)id);

                var entry = eds.Indexes().FirstOrDefault(el => el.Index == idx.Index);
                if (idx.Subindex > 0)
                {
                    entry = entry.subobjects[idx.Subindex];
                }

                if (entry.datatype != DataType.DOMAIN)
                {
                    throw new Exception($"Index {index} is not DOMAIN type");
                }

                return File(await sdoClient.ReadBytesAsync(idx), "application/octet-stream", $"NodeID{id}-{index}.dat");
            }
            catch (Exception ex)
            {
                return NotFound($"Не удалось получить содержимое индекса: {ex.Message}");
            }
        }

        private async Task<SDOIndexModel> Odentry2Model(ODIndex index, ODentry entry, byte nodeID, bool IsSubindex) 
            => new SDOIndexModel
        {
            IsSubindex = IsSubindex,
            Index = index,
            Name = entry.parameter_name,
            Type = not_visible_types.Contains(entry.datatype) ? string.Empty : entry.datatype.ToString(),
            Value = await TryReadIndex(index, entry, nodeID)
        };

        private string Odentry2Icon(ODentry entry)
            => $"<i class=\"far {(OdentryHasSubindexes(entry) ? FolderIcon : ValueIcon)}\"></i>";

        private async Task<SDOIndex> BuildIndex(ODIndex index, ODIndex parent, ODentry entry, byte nodeID)
            => new SDOIndex(index)
            {
                Text = await ViewRenderService.RenderToStringAsync(listItem_viewComponent,
                        await Odentry2Model(index, entry, nodeID, parent != null)),
                ParentId = parent?.ToString(),
                ImageHtml = Odentry2Icon(entry),
                HasChildren = OdentryHasSubindexes(entry)
            };
    }
}