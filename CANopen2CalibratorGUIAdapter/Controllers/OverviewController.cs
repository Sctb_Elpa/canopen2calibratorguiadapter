﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.Models;
using CANopen2CalibratorGUIAdapter.Models.DeviceCardModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CANopen2CalibratorGUIAdapter.Controllers
{
    public class OverviewController : Controller
    {
        private readonly ILogger<OverviewController> _logger;
        private readonly IBackendClient Backend;

        public OverviewController(IBackendClient backend, ILogger<OverviewController> logger)
        {
            Backend = backend;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(new OverwiewModel
            {
                Connected = Backend.Connected,
            });
        }

        public IActionResult NodeCard(int id)
        {
            var Node = Backend.Devices[(byte)id];

            return Node != null ? ViewComponent("DeviceListItem", new DeviceListItem
            {
                NodeID = Node.NodeID,
                LSSId = Node.LSSId,
                NMTState = Node.State,

                BootloaderController = Node.BootloaderController,

                DeviceKnown = Node.IsKnownDevice,
                VendorString = Node.GetVendorString(),
                ProductString = Node.GetProductString(),
                TimeFromLastHeartbeat = DateTime.Now - Node.LastHBTimestamp,
                EdsInfo = Node.Eds
            }) : null;
        }

        [HttpGet]
        public IActionResult Cards() => ViewComponent("DeviceList");

        [HttpGet]
        public async Task<IActionResult> GetTPDOMapping(int id)
        {
            var TPDO = Backend.Devices[(byte)id]?.TPDO;
            if (TPDO == null)
            {
                return Json(new { records = new object[0], total = 0 });
            }
            var config = await TPDO.TPDOConfig();
            var table = (from slot in config
                         where !slot.invalid
                         from index in slot.Mapping
                         select new TPDOItem
                         {
                             Index = $"{index.Index:X}sub{index.Subindex}",
                             Name = index.parameter_name,
                             Value = "N/A",
                             Timestamp = null
                         }).ToArray();

            return Json(new { records = table, total = table.Count() });
        }
    }
}