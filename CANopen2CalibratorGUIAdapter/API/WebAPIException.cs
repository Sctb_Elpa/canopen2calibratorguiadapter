﻿using System;

namespace CANopen2CalibratorGUIAdapter.API
{
    public class WebAPIException : Exception
    {
        public WebAPIException(string msg) : base(msg) { }
    }
}
