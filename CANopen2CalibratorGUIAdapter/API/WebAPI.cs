﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using CANopen2CalibratorGUIAdapter.CANOpenClient.PDO;
using CANopen2CalibratorGUIAdapter.Models;
using CANopen2CalibratorGUIAdapter.Models.TPDO;
using CANopen2CalibratorGUIAdapter.Utils;
using Makaretu.Dns.Resolving;
using Microsoft.Extensions.Logging;
using Utils;
using Utils.LSS;
using Utils.NMT;
using Utils.OD;
using WebSocketRPC;

namespace CANopen2CalibratorGUIAdapter.API
{
    internal class WebAPI : IWebAPI
    {
        #region Fields

        private static readonly Dictionary<string, NMTCommandCode> command_codes =
            new Dictionary<string, NMTCommandCode>
        {
            { "start", NMTCommandCode.GOTO_OPERTIONAL },
            { "stop", NMTCommandCode.GOTO_STOPPED },
            { "pre-op", NMTCommandCode.GOTO_PRE_OPERATIONAL },
            { "reset", NMTCommandCode.RESET_NODE },
            { "reset-comm", NMTCommandCode.RESET_COMMUNICATION },
        };

        private readonly ILogger _logger = Log.CreateLogger<WebAPI>();

        private readonly IBackendClient backend;

        private IEnumerable<IRemoteBinder<IClientEventAPI>> ReverseCaller;

        private bool BlockNetworkEvent = false;

        private readonly SemaphoreSlim mySemaphoreSlim = new SemaphoreSlim(1);

        #endregion Fields

        #region Constructors

        public WebAPI(IBackendClient backend)
        {
            this.backend = backend;
        }

        #endregion Constructors

        #region Enums

        private enum CanNodeEventType
        {
            HEARTBEAT,
            STATE_CHANGED,
            REMOVE,
            REMOVE_ALL,
        }

        #endregion Enums

        #region Methods

        public async Task<byte> AssignNodeID(string lssID, byte? nodeID)
        {
            var lss_id = UniqueLSSId.Parse(lssID);

            if (!lss_id.Valid())
            {
                var msg = $"{lssID} Not a fully determined LSS ID";
                _logger.LogError(msg);
                throw new WebAPIException(msg);
            }

            var result = await backend.AssignNodeID(lss_id, nodeID);
            if (result.Failure)
            {
                _logger.LogError($"Failed to assign node ID for device {lss_id} ({result.Error})");
                throw new WebAPIException(result.Error);
            }

            _logger.LogInformation($"NodeID {result.Value} has assigned to device {lss_id}");

            return result.Value;
        }

        public async Task ChangeBusSpeed(byte speedGrade)
        {
            void throwIfError(Result res)
            {
                if (res.Failure)
                {
                    _logger.LogError(res.Error);
                    throw new WebAPIException(res.Error);
                }
            }

            // broadcast nmt pre-operational
            throwIfError(await backend.NMTCommand(Constants.NodeIDBroadcast, NMTCommandCode.GOTO_PRE_OPERATIONAL));

            // broadcast lss configuration mode
            throwIfError(await backend.LSSSwitchStateGlobal(true));

            // broadcast set speed
            throwIfError(await backend.LSSConfigureNodeBitrate(speedGrade));

            // broadcast store settings
            throwIfError(await backend.LSSStoreConfiguration());

            // broadcast apply bitrate
            throwIfError(await backend.LSSSApplyBitrate());

            // open network at new speed
            throwIfError(await backend.ChangeNetworkSpeed(speedGrade));

            // broadcast lss waiting mode
            throwIfError(await backend.LSSSwitchStateGlobal(false));
        }

        public async Task<bool> CheckNodeIDAvalable(byte nodeID) => await backend.IsNodeIDAvalaible(nodeID);

        public Task ClearEmergencyLog()
        {
            backend.GetEmergencyLog().Clear();
            return Task.CompletedTask;
        }

        public DateTime GetLastHeartbeatTimestamp(byte nodeID)
        {
            var node = backend.Devices[nodeID];
            if (node == null)
            {
                var msg = $"Node {nodeID} not found in active list";
                _logger.LogError(msg);
                throw new WebAPIException(msg);
            }

            return node.LastHBTimestamp;
        }

        public async Task LssResetWaitingGlobal()
        {
            _logger.LogWarning("Resetting network LSS state...");

            var res = await backend.LSSSwitchStateGlobal(false);
            if (res.Failure)
            {
                throw new WebAPIException($"Не удалось Сбросить сеть ({res.Error})");
            }
        }

        public async Task NMTCommand(byte nodeID, string command)
        {
            if (!command_codes.ContainsKey(command))
            {
                var msg = $"Unknown command \"{command}\"";
                _logger.LogError(msg);
                throw new WebAPIException(msg);
            }

            if (nodeID > Constants.CANNodeIDMax)
            {
                var msg = $"Invalid node id {nodeID}";
                _logger.LogError(msg);
                throw new WebAPIException(msg);
            }

            var cmd = command_codes[command];

            Result result;

            // prevent reset comm on running devices - gui problem
            if (cmd == NMTCommandCode.RESET_COMMUNICATION)
            {
                if ((nodeID == 0 && backend.Devices.GetAliveDeviceList().Any(dev => dev.State == NMTState.Operatuional))
                    || (nodeID != 0 && backend.Devices[nodeID].State == NMTState.Operatuional))
                {

                    result = await backend.NMTCommand(nodeID, NMTCommandCode.GOTO_PRE_OPERATIONAL);
                    if (result.Failure)
                    {
                        _logger.LogError(result.Error);
                        throw new WebAPIException(result.Error);
                    }
                }
            }

            result = await backend.NMTCommand(nodeID, cmd);

            if (result.Failure)
            {
                _logger.LogError(result.Error);
                throw new WebAPIException(result.Error);
            }

            _logger.LogInformation($"NMT command {command} to node {nodeID} successfuly sent.");

            switch (cmd)
            {
                case NMTCommandCode.RESET_COMMUNICATION:
                case NMTCommandCode.RESET_NODE:
                    if (nodeID == Constants.NodeIDBroadcast)
                    {
                        backend.Devices.Reset();
                        SendNetworkEvent(0, CanNodeEventType.REMOVE_ALL);
                    }
                    else
                    {
                        backend.Devices.Remove(nodeID);
                        SendNetworkEvent(nodeID, CanNodeEventType.REMOVE);
                    }

                    break;
                case NMTCommandCode.GOTO_PRE_OPERATIONAL:
                    if (nodeID == Constants.NodeIDBroadcast)
                    {
                        backend.Devices.GetAliveDeviceList().ForEach(device => device.TPDO?.CancelAllPDOListeners());
                    }
                    else
                    {
                        backend.Devices[nodeID].TPDO?.CancelAllPDOListeners();
                    }

                    break;
                case NMTCommandCode.GOTO_OPERTIONAL:
                    if (nodeID == Constants.NodeIDBroadcast)
                    {
                        backend.Devices.GetAliveDeviceList().ForEach(async device => await device.TPDO?.UpdateSubscription());
                    }
                    else
                    {
                        await backend.Devices[nodeID].TPDO?.UpdateSubscription();
                    }

                    break;
            }
        }

        public void RegisterBackCalls()
        {
            ReverseCaller = RPC.For<IClientEventAPI>(this);

            backend.Devices.OnNodeHeartbeat += (_, node, state) => SendNetworkEvent(node, CanNodeEventType.HEARTBEAT);
            backend.Devices.OnNodeStateChanged += (_, node, state) => SendNetworkEvent(node, CanNodeEventType.STATE_CHANGED);

            backend.Devices.OnPDORessived += (_sender, timestamp, nodeId, _cobid, resault_pair)
                => SendPDOEvent(nodeId, timestamp, resault_pair);

            backend.GetEmergencyLog().OnEMCY += (s, ev) => SendEMCYEvent(ev.Id);
        }

        IBootloaderController GetBootloader(byte nodeID)
        {
            IBootloaderController btctrl;
            try
            {
                btctrl = backend.Devices[nodeID].BootloaderController;
            }
            catch
            {
                var err = $"Node id {nodeID} is unregistered or not in bootloader state";
                _logger.LogError(err);
                throw new WebAPIException(err);
            }

            return btctrl;
        }

        public async Task RestoreSettingsbackup(byte nodeID, string base64dataString)
        {
            if (string.IsNullOrEmpty(base64dataString))
            {
                var msg = "Empty backup data";
                _logger.LogError(msg);
                throw new WebAPIException(msg);
            }

            IBootloaderController btctrl = GetBootloader(nodeID);
            await btctrl.RestoreSettings(Convert.FromBase64String(base64dataString));

            _logger.LogInformation("Application settings restored");
        }

        public async Task ClearSettings(byte nodeID)
        {
            IBootloaderController btctrl = GetBootloader(nodeID);

            var backup = await btctrl.BackupSettings();
            if (backup.Success)
            {
                var empty_data = new byte[backup.Value.Length];
                Array.Fill<byte>(empty_data, 0xff);
                await btctrl.RestoreSettings(empty_data);
                _logger.LogInformation("Settings cleared");
            }
            else
            {
                var err = $"Failed to read application settings image {backup.Error}";
                _logger.LogError(err);
                throw new WebAPIException(err);
            }
        }

        public Task SetEds(byte nodeID, string EdsData)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(EdsData);
            writer.Flush();
            stream.Position = 0;

            var res = EdsInfo.TryLoad(stream);
            if (res.Failure)
            {
                throw new WebAPIException(res.Error);
            }

            backend.Devices[nodeID].Eds = res.Value;

            backend.Devices[nodeID].ConfigureTPDOManager(backend);

            return Task.CompletedTask;
        }

        public async Task SetTPDOConfig(byte nodeID, ushort configIndex, TPDOSlotConfig config)
        {
            _logger.LogInformation($"Mapping Config to node {nodeID}/[{configIndex:X}]: {config}");

            var tpdo = backend.Devices[nodeID].TPDO;
            await tpdo.WriteConfig(configIndex, config);

            //tpdo.UpdateSubscription();
        }

        public async Task SetTPDOMapping(byte nodeID, ushort mappingIndex, IEnumerable<string> mapping)
        {
            async Task TryWrite(TPDOManager tpdo, ushort mappingIndex)
            {
                try
                {
                    await tpdo.WriteMapping(mappingIndex);
                }
                catch (BackendCanError ex)
                {
                    // skip this kind of errors, due sealed slots
                    if (ex.Message.Contains("CANOPEN_REMOTE_NODE_ABORT"))
                    {
                        _logger.LogWarning($"Can't write mapping: {ex}");
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }

            _logger.LogInformation($"Mapping TPDO to node {nodeID}: {string.Join(", ", mapping)} -> [{mappingIndex:X4}]");

            var tpdo = backend.Devices[nodeID].TPDO;
            tpdo.GenerateMapping(mappingIndex, mapping.Select(s => new ODIndex(s)));
            var slot_cfg = (await tpdo.TPDOConfig()).Where(cfg => cfg.MappingIndex == mappingIndex).FirstOrDefault();
            if (slot_cfg != null && !slot_cfg.invalid)
            {
                // если слот был активен, то нельзя просто так взять и заменить конфиг, сперва надо его выключить, потом назад включить
                await tpdo.Control(slot_cfg.ConfigurationIndex, false);
                await TryWrite(tpdo, mappingIndex);
                await tpdo.Control(slot_cfg.ConfigurationIndex, true);
            }
            else
            {
                await TryWrite(tpdo, mappingIndex);
            }
        }

        public async Task StartApp(byte? NodeID)
        {
            if (!(NodeID is null))
            {
                var nodeID = (byte)NodeID;
                var node = backend.Devices[nodeID];
                if (node == null)
                {
                    var msg = $"Node {nodeID} not found in actibe list";
                    _logger.LogError(msg);
                    throw new WebAPIException(msg);
                }

                if (node.BootloaderController == null)
                {
                    var msg = $"Node {nodeID} has no bootloader controller";
                    _logger.LogError(msg);
                    throw new WebAPIException(msg);
                }

                try
                {
                    await node.BootloaderController.StartApplication();
                    _logger.LogInformation($"Node {nodeID}: Application started");
                }
                catch (BackendCanError ex)
                {
                    _logger.LogInformation($"Can't start application in {nodeID}: {ex.Message}");
                    throw new WebAPIException(ex.Message);
                }
            }
            else
            {
                _logger.LogWarning("Starting all applications");

                await mySemaphoreSlim.WaitAsync();
                BlockNetworkEvent = true;
                try
                {
                    foreach (var d in backend.Devices.GetAliveDeviceList())
                    {
                        await StartApp(d.NodeID);
                    }
                }
                finally
                {
                    BlockNetworkEvent = false;
                    mySemaphoreSlim.Release();
                }
            }
        }

        public async Task StartFastScan(string partial_known_lssID)
            => await backend.StartFastScan(UniqueLSSId.Parse(partial_known_lssID),
                new FastScanReportListener(ReverseCaller));

        private void SendEMCYEvent(int id) => Task.Run(() => ReverseCaller.CallAsync(x => x.EMCY(id)));

        private void SendNetworkEvent(byte nodeID, CanNodeEventType eventType)
        {
            if (!BlockNetworkEvent)
            {
                Task.Run(() => ReverseCaller.CallAsync(x => x.NetworkEvent(nodeID, eventType.ToString())));
            }
        }

        private void SendPDOEvent(byte nodeID, DateTime timestamp, KeyValuePair<ODIndex, object> resault_pair)
        {
            var index = resault_pair.Key;
            var name = backend.Devices[nodeID]?.Eds?.GetIndex(index.Index, index.Subindex)?.parameter_name ?? "Unknown";
            Task.Run(() => ReverseCaller.CallAsync(x => x.PDOEvent(
                        nodeID, timestamp,
                        resault_pair.Key.ToString(),
                        name,
                        resault_pair.Value.ToString())));
        }

        #endregion Methods
    }
}