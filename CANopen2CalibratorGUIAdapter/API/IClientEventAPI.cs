﻿using System;
using CANopen2CalibratorGUIAdapter.Models.WebRPC;

namespace CANopen2CalibratorGUIAdapter.API
{
    public interface IClientEventAPI
    {
        void NetworkEvent(byte NodeID, string eventName);

        void PDOEvent(byte nodeId, DateTime timestamp, string resultIndex, string name, string value);

        void FastScanProgress(FastScanResult msg);

        void EMCY(int ev);
    }
}
