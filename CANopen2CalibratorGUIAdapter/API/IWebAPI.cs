﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.Models.TPDO;

namespace CANopen2CalibratorGUIAdapter.API
{
    public interface IWebAPI
    {
        Task NMTCommand(byte nodeID, string command);

        Task StartApp(byte? nodeID);

        DateTime GetLastHeartbeatTimestamp(byte nodeID);

        Task LssResetWaitingGlobal();

        Task<bool> CheckNodeIDAvalable(byte nodeID);

        Task StartFastScan(string partial_known_lssID);

        Task<byte> AssignNodeID(string lssID, byte? nodeID);

        Task ChangeBusSpeed(byte speedGrade);

        Task RestoreSettingsbackup(byte nodeID, string base64dataString);

        Task ClearSettings(byte nodeID);

        Task SetEds(byte nodeID, string EdsData);

        Task SetTPDOMapping(byte nodeID, ushort mappingIndex, IEnumerable<string> mapping);

        Task SetTPDOConfig(byte nodeID, ushort configIndex, TPDOSlotConfig config);

        Task ClearEmergencyLog();
    }
}
