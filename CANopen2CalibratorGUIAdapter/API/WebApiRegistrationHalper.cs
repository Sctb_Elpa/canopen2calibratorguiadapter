﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using WebSocketRPC;

namespace CANopen2CalibratorGUIAdapter.API
{
    public static class WebApiRegistrationHalper
    {
        public static IEndpointConventionBuilder MapGRPCDefinition<T>(this IEndpointRouteBuilder builder, string route)
        {
            var jscontent = RPCJs.GenerateCallerWithDoc<T>();
            return builder.MapGet(route, async (HttpContext context)
                    => await context.Response.WriteAsync(jscontent));
        }
    }
}
