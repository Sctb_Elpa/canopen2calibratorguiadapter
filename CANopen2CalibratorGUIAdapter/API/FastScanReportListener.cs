﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.Models.WebRPC;
using Utils.LSS;
using WebSocketRPC;

namespace CANopen2CalibratorGUIAdapter.API
{
    internal class FastScanReportListener : IFastScanReportListener
    {
        #region Fields

        private readonly IEnumerable<IRemoteBinder<IClientEventAPI>> caller;

        #endregion Fields

        #region Constructors

        public FastScanReportListener(IEnumerable<IRemoteBinder<IClientEventAPI>> caller)
        {
            this.caller = caller;
        }

        #endregion Constructors

        #region Methods

        public async Task OnErrorMessage(string message)
            => await SendResult(new FastScanResult
            {
                Status = "error",
                Error = message
            });

        public async Task OnLssFinished(UniqueLSSId foundLssId)
             => await SendResult(new FastScanResult
             {
                 Status = "finished",
                 VendorID = foundLssId.VendorID,
                 ProductID = foundLssId.ProductID,
                 RevisionNumber = foundLssId.RevisionNumber,
                 SerialNumber = foundLssId.SerialNumber,
             });

        public async Task OnScanProgress(string fieldName, uint bitN)
            => await SendResult(new FastScanResult
            {
                Status = "progress",
                FieldName = fieldName,
                BitN = bitN
            });

        private async Task SendResult(FastScanResult res)
            => await caller.CallAsync(x => x.FastScanProgress(res));

        #endregion Methods
    }
}