using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CommandLine;
using Makaretu.Dns;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CANopen2CalibratorGUIAdapter
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var sd = new ServiceDiscovery();

            var builder = CreateHostBuilder(args, sd);
            if (builder == null)
            {
                return;
            }

            builder.Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args, ServiceDiscovery sd)
        {
            return Parser.Default.ParseArguments<Options>(args).MapResult(o =>
            {
                {
                    var ddnsDervice = new ServiceProfile($"{o.GRpcPort}", "_canopenadapter._tcp", (ushort)o.GRpcPort);
                    var txtrec = ddnsDervice.Resources.Find(r => r.Type == DnsType.TXT);
                    if (txtrec is TXTRecord r)
                    {
                        r.Strings.Add($"confWebPort={o.WebPort}");
                    }
                    sd.Advertise(ddnsDervice);
                }
                
                return Host.CreateDefaultBuilder(args)
                    .ConfigureWebHostDefaults(webBuilder =>
                    {
                        webBuilder.UseKestrel(options =>
                        {
                            // api insecure http2
                            options.ListenAnyIP(o.GRpcPort, listenOptions =>
                            {
                                listenOptions.Protocols = HttpProtocols.Http2;
                            });
                            // web interface
                            options.ListenAnyIP(o.WebPort, listenOptions =>
                            {
                                listenOptions.Protocols = HttpProtocols.Http1;
                            });
                        });
                        webBuilder.ConfigureServices(s => {
                            s.AddSingleton(o); 
                            s.AddSingleton<IBackendClient>(new BackendClient(o));
                        });
                        webBuilder.UseStartup<Startup>();
                    });
            }, _ => null);
        }
    }
}