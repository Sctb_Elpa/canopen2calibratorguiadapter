using CANopen2CalibratorGUIAdapter.API;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.Services;
using CANopen2CalibratorGUIAdapter.ViewComponents;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WebSocketRPC;

namespace CANopen2CalibratorGUIAdapter
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllersWithViews();
			services.AddGrpc();
			services.AddScoped<IViewRenderService, ViewRenderService>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
		{
			Log.LoggerFactory = loggerFactory;

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}
			app.UseStaticFiles(new StaticFileOptions
			{
				FileProvider = new PhysicalFileProvider(System.IO.Path.Combine(
					System.IO.Path.GetDirectoryName(
						System.Reflection.Assembly.GetEntryAssembly().Location),
						"wwwroot")),
				RequestPath = new PathString("")
			});

			app.UseRouting();

			//app.UseAuthorization();

			//initialize web-sockets
			app.UseWebSockets();

			//define route for a new connection and bind the API
			var webApi = new WebAPI(app.ApplicationServices.GetService<IBackendClient>());
			app.MapWebSocketRPC("/webAPI", (httpCtx, c) => {
				// ���� � ���, ��� ��� ������ ���������� ��������� ��������� � ����� url, �
				// RegisterBackCalls() ���� �������� ������ ����� Bind()
				c.Bind<WebAPI, IClientEventAPI>(webApi);
				webApi.RegisterBackCalls();
			});

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapGrpcService<BridgeService>();
				endpoints.MapGRPCDefinition<IWebAPI>("/webAPI.js");
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
