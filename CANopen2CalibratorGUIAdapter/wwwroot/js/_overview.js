﻿const fadein_speed = 'slow';
const in_progress = 'InProgress';

var api = null;

var cards = {};
var table_map = {};
var heartbeats = {};

//------------------------------------------------------

$(function () {
    $(function () {
        $('#nav-bar-overview').addClass('active');
    });

    api = new API();

    api.registerReversCall('eMCY', () => { });

    api.registerReversCall('networkEvent', function (nodeid, event) {
        switch (event) {
            case 'HEARTBEAT':
                update_heartbeat(nodeid);
                break;
            case 'STATE_CHANGED':
                reload_card(nodeid);
                break;
            case 'REMOVE':
                remove_card(nodeid);
            case 'REMOVE_ALL':
                removeAllCards();
                break;
            default:
                console.log('Unknown event: ' + event);
                break;
        }
    });

    api.registerReversCall('pDOEvent', function (nodeId, timestamp, resultIndex, value) {
        var table = table_map[nodeId];
        if (table) {
            var data = table.getById(resultIndex);
            if (data && (data.value != value)) {
                data.timestamp = timestamp;
                data.value = value;
                table.updateRow(resultIndex, data);
            }
        }
    });

    setTimeout(function updateHeartbeat() {
        const now = new Date();
        for (var $c of Object.values(cards)) {
            if ($c && $c != in_progress) {
                setHeartbeatStatus($c, now - heartbeats[$c.attr('node-id')]);
            }
        }
        setTimeout(updateHeartbeat, 1000);
    }, 1000);

    // if any device registred
    $.get('/LSS/NetworkConfig', (data) => { if (data) loadCardsSet(); });
});

//------------------------------------------------------

function find_card(card_id) { return cards[card_id]; }

function setHeartbeatStatus($card, heartbeat_td) {
    function generateInfo(td) {
        if (!td) {
            return { heartbeatToolTip: "N/A", heartBitIconColor: 'secondary' }
        } else if (td <= 1000) {
            return { heartbeatToolTip: "Менее секунды", heartBitIconColor: 'success' }
        } else if (td <= 5000) {
            return { heartbeatToolTip: td / 1000 + " секунд", heartBitIconColor: 'warning' }
        } else {
            return { heartbeatToolTip: Math.round(td / 1000) + " секунд", heartBitIconColor: 'danger' }
        }
    }

    const heartbeat_info = generateInfo(heartbeat_td);

    const $heartbit_icon = $card.find("span[data-title]");

    if ($heartbit_icon.attr('data-title') != heartbeat_info.heartbeatToolTip) {
        $heartbit_icon.attr('data-title', heartbeat_info.heartbeatToolTip);
        $heartbit_icon.attr('data-original-title', heartbeat_info.heartbeatToolTip);
    }

    const $svg = $heartbit_icon.find('svg');

    const new_color_class = 'text-' + heartbeat_info.heartBitIconColor;
    if (!$svg.hasClass(new_color_class)) {
        $svg.removeClass((_, className) => (className.match(/(^|\s)text-\S+/g) || []).join(' '))
        $svg.addClass(new_color_class);
    }
}

function loadCardsSet() {
    $.get('/Overview/Cards', (data) => {
        var d = $(data)
        enable_tooltips(d.find(data_toggle_selector));
        d.hide().fadeIn(fadein_speed);

        d.find('div.card').each((_, c) => {
            var $c = $(c);
            var _lCard_id = $c.attr('node-id');
            table_map[_lCard_id] = loadTPDOTable($c.find('table'), _lCard_id);
            cards[_lCard_id] = $c;
        });
        $('#device-list-cotainer').html(d);
    });
}

function create_card(card_id) {
    const prev_card = find_card(card_id - 1);

    cards[card_id] = in_progress;
    if (!prev_card) {
        loadCardsSet();
    } else {
        $.get('/Overview/NodeCard/' + card_id,
            (data) => {
                var $card = $(data);
                enable_tooltips($card.find(data_toggle_selector));
                $card.hide().fadeIn(fadein_speed);
                var pdoTable = $card.find('table');
                if (pdoTable.length > 0) {
                    table_map[card_id] = loadTPDOTable(pdoTable, card_id);
                }
                prev_card.after($card);
                cards[card_id] = $card;
            }
        );
    }
}

async function update_heartbeat(card_id) {
    const card = find_card(card_id);

    if (!card) {
        create_card(card_id);
        return;
    }
    if (card == in_progress) {
        return;
    }

    try {
        heartbeats[card_id] = new Date(await api.getLastHeartbeatTimestamp(card_id));
    } catch (err) {
        noty_error(err);
        return;
    }
}

function loadTPDOTable($table, NodeID) {
    if ($table.length == 0) {
        return null;
    }
    var table = $table.grid({
        primaryKey: 'mappingID',
        uiLibrary: 'bootstrap4',
        primaryKey: 'index',
        dataSource: '/Overview/GetTPDOMapping/' + NodeID,
        notFoundText: 'Маппинг пуст',
        columns: [
            { field: 'name', title: 'Переменная', align: 'left' },
            { field: 'value', title: 'Значение', align: 'right' }
        ]
    });
    table.on('rowDataBound', function (e, $row, id, record) {
        var cols = $row.find('div[title]');
        $(cols[0]).attr('title', '[' + id + ']');

        var timestamp = record.timestamp
            ? 'Получено в: ' + (new Date(record.timestamp)).toLocaleString('ru-RU', {
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit'
            })
            : "Данные не получены";
        $(cols[1]).attr('title', timestamp);
    });

    return table;
}

function reload_card(card_id) {
    const replace_table = function ($c) {
        var _lCard_id = $c.attr('node-id');
        table_map[card_id] = loadTPDOTable($c.find('table'), _lCard_id);
    }

    const card = find_card(card_id);
    if (!card) {
        create_card(card_id);
    } else {
        if (card == in_progress) {
            return;
        }
        cards[card_id] = in_progress;
        $.get(document.location.pathname + '/NodeCard/' + card_id,
            (data) => {
                var $new_card = $(data);
                enable_tooltips($new_card.find(data_toggle_selector));
                $new_card.hide().fadeIn(fadein_speed);
                if (card == in_progress) {
                    startAsync(async () => {
                        while (true) {
                            $oldcard = $('div.card[node-id]');
                            if ($oldcard.length > 0) {
                                $oldcard.replaceWith($new_card);
                                replace_table($new_card);
                                return;
                            }
                            await sleep(1000);
                        }
                    });
                } else {
                    card.replaceWith($new_card);
                    replace_table($new_card);
                }
                cards[card_id] = $new_card;
            });
    }
}

function remove_card(card_id) {
    find_card(card_id).remove();
    delete table_map[card_id];
}

function removeAllCards() {
    cards = {};
    table_map = {};

    $('#device-list-cotainer').load(document.location.pathname + '/Cards');
}

//------------------------------------------------------

async function NMTGlobal(command) {
    await voidApiCall(async () => await api.globalNMTCommand(command))
}

async function NMTTarget(nodeID, command) {
    await voidApiCall(async () => await api.NMTCommand(nodeID, command))
}

async function startApp(nodeID) {
    await voidApiCall(async () => await api.startApp(nodeID))
}

async function uploadNodeEds(nodeID) {
    const input = $('#eds-file');

    input.off();
    input.val('');

    input.change(function (e) {
        const files = e.target.files;
        if (files.length < 1) {
            return;
        }

        const reader = new FileReader();
        reader.onload = function (data) {
            startAsync(async () => {
                try {
                    await api.setEds(nodeID, data.target.result);
                    reload_card(nodeID);
                } catch (err) {
                    noty_error(err);
                }
            });
        }
        reader.readAsText(files[0]);
    });

    input.click();
}