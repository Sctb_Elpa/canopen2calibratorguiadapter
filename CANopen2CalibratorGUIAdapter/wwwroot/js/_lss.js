﻿var api = null;
var lss_inputs = null;

//------------------------------------------------------

$(function () {
    $(function () {
        $('#nav-bar-lss').addClass('active');
    });

    api = new API();

    lss_inputs = {
        VID: $("input[aria-describedby='VID-addon']"),
        PID: $("input[aria-describedby='PID-addon']"),
        Rev: $("input[aria-describedby='Revision-addon']"),
        Serial: $("input[aria-describedby='Serial-addon']"),
    };

    api.registerReversCall('networkEvent', () => { });
    api.registerReversCall('pDOEvent', () => { });
    api.registerReversCall('eMCY', () => { });

    $("input.lss-id-part")
        .each((_, e) => validateLssElement($(e)))
        .on("keyup paste", function () {
            validateLssElement($(this));
            updateLssFastscanCommandsAvalability();
        });

    $("#node-id-input").change(function () {
        $("#assign-node-id").prop("disabled", "disabled");
        checkNodeIDAvalable($(this), updateLssFastscanCommandsAvalability);
    });

    updateLssFastscanCommandsAvalability();

    $('#network-config-file').change(function (e) {
        var files = e.target.files;

        if (files.length < 1) {
            return;
        }

        var reader = new FileReader();
        reader.onload = function (data) {
            $('#network-config').val(data.target.result);
        }

        reader.readAsText(files[0]);
    });
});

//------------------------------------------------------------

function fillTemplate(string) {
    return '<span class="input-group-text"><i class="fa ' + string + '"></i></span>';
}

//------------------------------------------------------------

function checkNodeIDAvalable(node_id_input, cb) {
    var icon_holder = node_id_input.parent().find("div.form-control-feedback");
    var value = node_id_input.val();

    icon_holder.empty();

    if (value == "") {
        icon_holder.append(fillTemplate('fa-question'));
        return;
    }

    function showErr(msg) {
        icon_holder.append(fillTemplate('fa-times-circle text-danger'));
        noty({
            type: "error",
            text: "<i class='fa fa-times-circle'></i> " + msg,
            timeout: 5000
        });
    }

    v = parseInt(node_id_input.val());
    if (v > 0) {
        icon_holder.append(fillTemplate('fa-search'));

        startAsync(async () => {
            try {
                var result = await api.checkNodeIDAvalable(v);
                icon_holder.empty();
                if (result) {
                    icon_holder.append(fillTemplate('fa-check text-success'));
                } else {
                    showErr('Адресс ' + v + ' занят');
                }
            } catch (err) {
                showErr(err);
            }
            cb();
        });
    } else {
        showErr("\"" + value + "\" не является числом");
        cb();
    }
}

function updateLssFastscanCommandsAvalability() {
    var searchButton = $("#start-fast-scan");
    var NodeIDAssign = $("#assign-node-id");

    searchButton.attr("title", "Найти");

    var disable = false;
    var fullyAssigned = true;
    $("input.lss-id-part").each(function (_, element) {
        element = $(element);
        disable |= element.parent().find("svg").hasClass("text-danger");
        fullyAssigned &= !isNaN(parseInt(element.val()));
    });

    $("#node-id-input").each(function (_, element) {
        element = $(element);
        disable |= element.parent().find("svg").hasClass("text-danger");
    });

    if (!disable) {
        searchButton.removeAttr("disabled");
        if (fullyAssigned) {
            NodeIDAssign.removeAttr("disabled");
        } else {
            NodeIDAssign.attr("disabled", "disabled");
        }
    } else {
        searchButton.attr("disabled", "disabled");
        NodeIDAssign.attr("disabled", "disabled");
    }
}

function validateLssElement(element) {
    var icon_holder = element.parent().find("div.form-control-feedback");

    icon_holder.empty();

    if (element.val() == "") {
        // empty
        icon_holder.append(fillTemplate('fa-question'));
    } else {
        var value = parseInt(element.val());

        if (isNaN(value) || value > 0xffffffff) {
            // not valid value
            icon_holder.append(fillTemplate('fa-times-circle text-danger'));
        } else {
            // valid value
            icon_holder.append(fillTemplate('fa-check text-success'));
        }
    }
}

function buildLssRequestData() {
    const VID_v = lss_inputs.VID.val();
    const PID_v = lss_inputs.PID.val();
    const Rev_v = lss_inputs.Rev.val();
    const Serial_v = lss_inputs.Serial.val();

    return VID_v + ':' + PID_v + ':' + Rev_v + ':' + Serial_v;
}

function extractLssId(obj) {
    return obj.vendorID + ':' + obj.productID + ':' + obj.revisionNumber + ':' + obj.serialNumber;
}

function tryParceConfigString(line) {
    const re = new RegExp("([\\dxXA-Fa-f]+:[\\dxXA-Fa-f]+:[\\dxXA-Fa-f]+:[\\dxXA-Fa-f]+)=(\\d+)");
    m = line.match(re);
    if (m == null) {
        throw 'Строка конфигурации "' + line + '" имеет неверный формат!';
    } else {
        return { lssID: m[1], nodeID: m[2] };
    }
}

//------------------------------------------------------------

async function resetLssWaiting() {
    await voidApiCall(async () => await api.LssResetWaitingGlobal());
}

async function changeBusSpeed() {
    const selected = $('select[name="BusSpeed"]').children("option:selected");
    const speed_value = selected.text();
    const speed_grade = selected.val();

    if (confirm('Изменить скорость работы сети CANOpen на ' + speed_value + '?')) {
        await voidApiCall(async () => await api.changeBusSpeed(speed_grade));
    }
}

async function startFastScan() {
    try {
        var lss_id = await perform_fast_scan(buildLssRequestData());

        // update values
        lss_inputs.VID.val("0x" + lss_id.vendorID.toString(16));
        lss_inputs.PID.val("0x" + lss_id.productID.toString(16));
        lss_inputs.Rev.val(lss_id.revisionNumber);
        lss_inputs.Serial.val(lss_id.serialNumber);

        for (key in lss_inputs) {
            validateLssElement(lss_inputs[key]);
        }

        updateLssFastscanCommandsAvalability();
    } catch (err) {
        if (err == 'no-device') {
            noty_error('Ненастроенных устройств в сети не обнаружено');
        } else {
            noty_error(err);
        }
    }
}

async function assignNodeID() {
    const node_to_assign = parseInt($("#node-id-input").val())
    try {
        await perform_node_assign(buildLssRequestData(), node_to_assign)

        // clear form
        $('#lss-config-form')[0].reset();

        for (var e in lss_inputs) {
            validateLssElement(lss_inputs[e]);
        }
        checkNodeIDAvalable($("#node-id-input"), () => { });
        updateLssFastscanCommandsAvalability();

        reload_network_config();
    } catch (err) {
        noty_error(err);
    }
}

async function perform_fast_scan(partial_known_lssID) {
    var result = null;
    var status = null;

    function createReportListener(noty_inst) {
        return function (msg) {
            switch (msg.status) {
                case "progress":
                    const pos = 31 - msg.bitN;
                    noty_inst.setText(
                        "<div>" +
                        "<p><i class='fa fa-search'></i> Просмотр битов " + msg.fieldName + "</p>" +
                        "<div class='progress'>" +
                        "<div class='progress-bar' role='progressbar' " +
                        "aria-valuenow='" + pos + "' aria-valuemin='0' " +
                        "aria-valuemax='31' style='width:" + ((pos / 31) * 100) + "%;'></div>" +
                        "</div >");
                    return;

                case "finished":
                    status = 'finished';
                    result = msg;
                    break;

                case "error":
                    status = 'error'; 
                    result = (msg.error == 'No unconfigured devices found')
                        ? 'no-device' : msg.error;
                    break;

                default:
                    status = 'error';
                    result = null;
                    break;
            }

            api.registerReversCall('fastScanProgress', null);
        }
    }

    try {
        var noty_obj = noty({
            type: "information",
            text: "<i class='fas fa-spinner fa-pulse'></i> Поиск устройств начат...",
        });

        api.registerReversCall('fastScanProgress', createReportListener(noty_obj));

        await api.startFastScan(partial_known_lssID);
        noty_obj.close();
    } catch (err) {
        noty_obj.close();

        throw err;
    }
    if (status == 'finished') {
        return result;
    }
    throw result;
}

async function perform_node_assign(lss_id, node_id) {
    const assigned_node_id = await api.assignNodeID(lss_id, node_id);
    noty({
        type: "success",
        text: "<i class='fa fa-check'></i> Назначен NodeID " + assigned_node_id,
        timeout: 5000
    });
    return assigned_node_id;
}

async function autoConfigureNetwork() {
    const button = $('#net-auto-config');

    if (confirm('Настроить сеть автоматически? Будет циклически запущена операция ' +
        'FastScan пока в сети остаются ненастроенные устройства.')) {

        button.prop('disabled', true);

        var num_of_devocese_found = 0;
        try {
            await api.globalNMTCommand('reset'); // reset all
            await sleep(100);

            while (true) {
                var lss_id = extractLssId(await perform_fast_scan(':::'));
                await perform_node_assign(lss_id, null);
                ++num_of_devocese_found;
            }
        } catch (err) {
            if (err == 'no-device') {
                if (num_of_devocese_found > 0) {
                    noty_success('Настроена есть из ' + num_of_devocese_found + ' устройств(а)');
                } else {
                    noty_error('Не удалось обнаружить ни одного устройства!');
                }
            } else {
                noty_error(err);
            }
            await sleep(100);
            reload_network_config();
        }

        button.prop('disabled', false);
    }
}

function reload_network_config() {
    setTimeout(function () {
        $.get(document.location.pathname + '/NetworkConfig', (data) => $("#network-config").val(data));
    }, 500);
}

async function applyNetworkConfig() {
    const text = $("#network-config").val();

    var cfg;
    try {
        const lines = text.split('\n');
        for (var i in lines) {
            cfg = tryParceConfigString(lines[i].trim());
            await perform_node_assign(cfg.lssID, cfg.nodeID);
        }
        noty_success('Конфигурация сети применена');
    } catch (err) {
        noty_error('Ошибка назначения адреса ' + cfg.nodeID + ' устройству ' + cfg.lssID + ' (' +  err + ')');
    }
}

function downloadNetworkConfig() {
    const blob = new Blob([ $("#network-config").val() ], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "Network.lsscfg");
}

function uploadNetworkConfig() {
    $('#network-config-file').click();
}