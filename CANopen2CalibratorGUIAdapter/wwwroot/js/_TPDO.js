﻿const palete = ['#00BBC9', '#EC63AB', '#AA8AE4', '#83CE44', '#ff8f25', '#009EAA', '#CA4F7F', '#9C70C0', '#6BAF3B'];

var api = null;

var NodeID = 0;
var mappingTable = null;
var mappingConfig = null;
var mapping_control_buttons = null;

var model = {
    mapping: {},
    maxMappingSize: 0
};

//------------------------------------------------------

function getSlotConfig(row) {
    return {
        enabled: row.enabled,
        cobId: row.cobId,
        transferTypeID: row.transferTypeID,
        inhibit: row.inhibit,
        iventTimer: row.iventTimer,
        syncStart: row.syncStart,
    };
}

function hex_render(value) { return '0x' + value.toString(16); }

function mapping_config_renderer(value, record, $cell, $displayEl, id) {
    if (record.configEnabled) {
        const template = '<button class="btn btn-light btn-sm" title="TITLE"><i class="ICON"></i></button>';
        const btnGroupTemplate = '<div class="btn-group" role="group"></div>';

        var $btnGroups = [$(btnGroupTemplate), $(btnGroupTemplate).addClass('ml-2')];

        var $editButton = $(template.replace('ICON', 'far fa-edit').replace('TITLE', 'Изменить')),
            $applyButton = $(template.replace('ICON', 'fa fa-check text-success').replace('TITLE', 'Применить')).hide(),
            $cancelButton = $(template.replace('ICON', 'fa fa-undo text-danger').replace('TITLE', 'Отмена')).hide(),

            $saveasButton = $(template.replace('ICON', 'far fa-save').replace('TITLE', 'Сохранить в файл...')),
            $loadButton = $(template.replace('ICON', 'fa fa-file-upload').replace('TITLE', 'Загрузить из файла...'));

        function restore_default_button_set() {
            $editButton.show();
            $applyButton.hide();
            $cancelButton.hide();

            $saveasButton.show();
            $loadButton.show();
        }

        $editButton.click(() => {
            mappingConfig.edit(id);

            $editButton.hide();
            $applyButton.show();
            $cancelButton.show();

            $saveasButton.hide();
            $loadButton.hide();
        });
        $applyButton.click(() => {
            mappingConfig.update(id);
            restore_default_button_set();

            startAsync(async () => {
                var row = mappingConfig.get(id);
                try {
                    await api.setTPDOConfig(NodeID, row.communication, getSlotConfig(row));
                } catch (err) {
                    noty_error('Ошибка применения конфигурации: ' + err);
                    mappingConfig.reload();
                    return;
                }
                //mappingConfig.updateRow(row.mappingID, row);
                noty_success('Конфигурация применена');
            });
        });
        $cancelButton.click(() => {
            mappingConfig.cancel(id)
            restore_default_button_set();
        });

        $saveasButton.click(() => {
            $.get('../Mapping/' + NodeID, { mappingId: id }, function (data) {
                const current_config = mappingConfig.get(id);
                const mapped = data.records
                    .filter(item => item.mapped)
                    .map(item => ({
                        index: item.index,
                        bitOffset: item.bitOffset
                    }));

                const save_data = {
                    transferTypeID: parseInt(current_config.transferTypeID),
                    inhibit: parseInt(current_config.inhibit),
                    iventTimer: parseInt(current_config.iventTimer),
                    syncStart: parseInt(current_config.syncStart),
                    mapping: mapped
                };

                const blob = new Blob([JSON.stringify(save_data, null, ' ')], { type: "application/json;charset=utf-8" });
                saveAs(blob, "config.TPDOmap");
            });
        });

        $loadButton.click(() => $('#tpdo-config-input')
            .off()
            .val('')
            .change((e) => {
                const files = e.target.files;
                if (files.length < 1) {
                    return;
                }
                var reader = new FileReader();
                reader.onload = (d) => LoadMapConfig(id, d.target.result);
                reader.readAsText(files[0]);
            })
            .click());

        $btnGroups[0]
            .append($editButton)
            .append($applyButton)
            .append($cancelButton);

        $btnGroups[1]
            .append($saveasButton)
            .append($loadButton)

        $displayEl.empty().append($btnGroups);
    }
}

//------------------------------------------------------

function getRowById(id) {
    // from gijgo.js
    var c, d, e = mappingTable.getAll(!1),
        f = mappingTable.data("primaryKey"),
        g = void 0;
    if (f) {
        for (d = 0; d < e.length; d++)
            if (e[d][f] == id) {
                c = d + 1;
                break
            }
    } else {
        c = id;
    }
    return c && (g = mappingTable.children("tbody").children('tr[data-position="' + c + '"]')), g
}

function calc_unused_bits() {
    var unused_bits = 8 * 8;
    for (key in model.mapping) {
        unused_bits -= model.mapping[key].record.bitSize;
    }
    return unused_bits;
}

function getOrderedMappingScematic() {
    var res = Object.values(model.mapping);
    return res.sort((a, b) => {
        var _a = parseInt(a.record.bitOffset);
        var _b = parseInt(b.record.bitOffset);
        return _a - _b;
    });
}

function draw_usage_scematic() {
    $('#uv-div').empty();

    const unused_bits = calc_unused_bits();

    var graphdef = {
        categories: [],
        dataset: {}
    };

    const orderedMappingScematic = getOrderedMappingScematic();
    graphdef.categories = orderedMappingScematic.map(item => item.record.index);
    for (var value of Object.values(orderedMappingScematic)) {
        graphdef.dataset[value.record.index] = [
            { name: 'value', value: parseInt(value.record.bitSize) }
        ];
    }
    if (unused_bits > 0) {
        graphdef.categories.push('Свободно');
        graphdef.dataset['Свободно'] = [
            { name: 'value', value: unused_bits }
        ];
    };
    const testconfig = {
        graph: {
            custompalette: palete,
            bgcolor: 'none',
            max: 64, // не работает
            min: 0
        },
        dimension: {
            height: 20
        },
        margin: {
            top: 1,
            bottom: 1,
            left: 1,
            right: 1
        },
        axis: {
            showticks: false,
            showsubticks: false,
            showtext: false,
            showhortext: false,
            showvertext: false
        },
        frame: {
            bgcolor: 'none'
        },
        legend: {
            showlegends: false
        },
        label: {
            postfix: ' Бит',
            fontfamily: 'PT Sans'
        },
        effects: {
            duration: 100,
        },
        tooltip: {
            format: '%c: %v бит'
        }
    };

    uv.chart('StackedBar', graphdef, testconfig);
}

function LoadMapConfig(slotId, data) {
    try {
        data = JSON.parse(data);
    } catch (err) {
        noty_error('Некорректный файл конфигурации!');
        return;
    }

    var configData = mappingConfig.get(slotId);
    configData.transferTypeID = data.transferTypeID;
    configData.inhibit = data.inhibit;
    configData.iventTimer = data.iventTimer;
    configData.syncStart = data.syncStart;
    mappingConfig.updateRow(slotId, configData);

    // clear mapping
    for (var row = 1; row <= mappingTable.count(); ++row) {
        var rowData = mappingTable.get(row);
        rowData.mapped = false;
        rowData.bitOffset = null;
        mappingTable.updateRow(rowData.index, rowData);
    }

    // load mapping
    model.mapping = {};
    for (var m of Object.values(data.mapping)) {
        var record = mappingTable.getById(m.index);
        record.mapped = true;
        record.bitOffset = m.bitOffset;

        mappingTable.updateRow(m.index, record);
        model.mapping[m.index] = {
            record: record,
            row: getRowById(m.index)
        };
    }

    paint_rows();
    draw_usage_scematic();

    startAsync(async () => {
        // save
        try {
            var selected_mapping = mappingConfig.get(slotId);
            await api.setTPDOMapping(NodeID, selected_mapping.mapping, Object.keys(model.mapping));
            await api.setTPDOConfig(NodeID, selected_mapping.communication, getSlotConfig(selected_mapping));
        } catch (err) {
            noty_error('Ошибка загрузки конфигурации: ' + err);
            mappingConfig.reload();
            return;
        }
        noty_success('Конфигурация загружена в слот' + slotId);
    });
}

function model_can_append(record) {
    if (calc_unused_bits() < record.bitSize) {
        noty_error('Недостаточно места в сообщении для маппинга индекса ' + record.index);
        return false;
    }
    if (Object.keys(model.mapping).length == model.maxMappingSize) {
        noty_error('Невозможно добавить маппинг, исчерпаны конфигурационные поля');
        return false;
    }
    return true;
}

function try_scematic_add(index, bitsize) {
    if (calc_unused_bits(model.mapping) < bitsize) {
        noty_error('Недостаточно места в сообщении для маппинга индекса ' + index);
        return { res: false };
    }
    if (Object.keys(model.mapping).length == model.maxMappingSize) {
        noty_error('Невозможно добавить маппинг, исчерпаны конфигурационные поля');
        return { res: false };
    }

    model.mapping[index] = parseInt(bitsize);
    draw_usage_scematic(model.mapping);
    return { res: true, mapped: Object.keys(model.mapping).length };
}

function getColor(index) {
    return palete[index];
}

function paint_rows() {
    mappingTable.find('tr[data-position]').css('background-color', '');
    Object.values(getOrderedMappingScematic()).map((v, index) => v.row.css('background-color', getColor(index)));
}

function update_offsets() {
    var offset = 0;
    const sorted_indexies = getOrderedMappingScematic();

    for (var mapped_object of Object.values(sorted_indexies)) {
        mapped_object.record.bitOffset = offset;
        offset += parseInt(mapped_object.record.bitSize);
    }
}

function map_controller_renderer(value, record, $cell, $displayEl, id) {
    const template = '<button class="btn btn-light btn-sm"><i class="far ICON"></i></button>';

    var $addbutton = $(template.replace('ICON', 'fa-square')),
        $removebutton = $(template.replace('ICON', 'fa-check-square'));

    if (record.mapped) {
        $addbutton.hide();
    } else {
        $removebutton.hide();
    }

    $displayEl.empty().append($addbutton).append($removebutton);

    $addbutton.click(function (e) {
        if (model_can_append(record)) {
            $addbutton.hide();
            $removebutton.show();

            var mod_record = mappingTable.getById(id);
            mod_record.mapped = true;
            mod_record.bitOffset = Object.values(model.mapping)
                .reduce((a, v) => a + parseInt(v.record.bitSize), 0);
            mappingTable.updateRow(id, mod_record);

            model.mapping[record.index] = {
                record: mod_record,
                row: getRowById(id)
            };

            paint_rows();
            draw_usage_scematic();

            mapping_control_buttons.fadeIn(100);
        }
    });
    $removebutton.click(function (e) {
        $addbutton.show();
        $removebutton.hide();

        delete model.mapping[record.index];

        var mod_record = mappingTable.getById(id);
        mod_record.mapped = false;
        mod_record.bitOffset = null;
        mappingTable.updateRow(id, mod_record);

        // recalc offsets
        update_offsets();

        // update rows
        for (const [ri, rec] of Object.entries(model.mapping)) {
            mappingTable.updateRow(ri, rec.record);
        }

        paint_rows();
        draw_usage_scematic();

        mapping_control_buttons.fadeIn(100);
    });
}

//------------------------------------------------------

$(function () {
    api = new API();

    api.registerReversCall('networkEvent', () => { });
    api.registerReversCall('pDOEvent', () => { });
    api.registerReversCall('eMCY', () => { });

    NodeID = $('#node-id-ancor').attr('node-id');

    mapping_control_buttons = $('#mapping-control-buttons');

    mapping_control_buttons.hide();

    mappingConfig = $('#pdo-mappings').grid({
        primaryKey: 'mappingID',
        dataSource: '../MappingConfig/' + NodeID,
        uiLibrary: 'bootstrap4',
        inlineEditing: { mode: 'command', managementColumn: false },
        columns: [
            { field: 'mappingID', title: '№', align: 'center', width: 40 },
            { field: 'enabled', title: 'Вкл?', align: 'center', width: 70, type: 'checkbox', editor: true },
            { field: 'communication', title: 'Индекс', align: 'center', width: 100, renderer: hex_render },
            { field: 'mapping', title: 'Маппинг', align: 'center', width: 100, renderer: hex_render },
            { field: 'cobId', title: 'COB-ID', align: 'center', editor: true, width: 100 },
            {
                field: 'transferType',
                title: 'Режим',
                type: 'dropdown',
                editor: { dataSource: '../TPDOModes', valueField: 'transferTypeID' },
                editField: 'transferTypeID'
            },
            { field: 'inhibit', title: 'Подавление, *0,1 мс', editor: true, align: 'center', width: 150 },
            { field: 'iventTimer', title: 'Таймер, мс', editor: true, align: 'center', width: 100 },
            { field: 'syncStart', title: '#SYNC', editor: true, align: 'center', width: 100 },
            {
                title: '',
                align: 'center',
                width: 130,
                renderer: mapping_config_renderer
            }
        ]
    });

    mappingTable = $('#pdo-mappable-indexes').grid({
        title: 'Индексы, доступные для маппинга',
        primaryKey: 'index',
        dataSource: '../Mapping/' + NodeID,
        uiLibrary: 'bootstrap4',
        fixedHeader: true,
        autoLoad: false,
        columns: [
            { align: 'center', width: 80, renderer: map_controller_renderer },
            { field: 'bitOffset', title: 'Смещение', align: 'center', width: 100 },
            { field: 'index', title: 'Индекс', align: 'right', width: 100 },
            { field: 'name', title: 'Название' },
            { field: 'type', title: 'Тип данных', align: 'right', width: 150 },
            { field: 'bitSize', title: 'Бит', align: 'center', width: 80 },
        ],
    });

    mappingTable.on('dataBound', function (e, records, totalRecords) {
        model.mapping = {};
        for (var ri in records) {
            var record = records[ri];
            if (record.mapped) {
                model.mapping[record.index] = {
                    record: record,
                    row: mappingTable.find('tr[data-position="' + (parseInt(ri) + 1) + '"]'),
                };
            }
        }

        paint_rows();
        draw_usage_scematic();
    });

    mappingConfig.on('rowSelect', function (e, $row, id, record) {
        mappingTable.reload({ mappingId: id });
        mappingTable.attr('disabled', !record.mappingEnabled);
        mappingTable.data('mapping-id', id);
        model.maxMappingSize = record.maxMappingSize;

        mapping_control_buttons.hide();
    });
});

async function applyMapping() {
    try {
        var selected_mapping = mappingConfig.get(mappingTable.data('mapping-id'));
        await api.setTPDOMapping(NodeID, selected_mapping.mapping, Object.keys(model.mapping));
    } catch (err) {
        noty_error('Не удалось применить маппинг: ' + err);
        return;
    }
    noty_success('Маппинг был применен успешно');

    mapping_control_buttons.hide();
}

function resetMapping() {
    mappingTable.reload({ mapping: mappingConfig.get(mappingConfig.getSelected()).index });
    mapping_control_buttons.hide();
}

function LoadTpdoFromFile() {
    $('#tpdo-global-config-input')
        .off()
        .val('')
        .change((e) => {
            const files = e.target.files;
            if (files.length < 1) {
                return;
            }
            var reader = new FileReader();
            reader.onload = (d) => startAsync(async () => {
                try {
                    data = JSON.parse(d.target.result);
                } catch (err) {
                    noty_error('Некорректный файл конфигурации TPDO!');
                    return;
                }

                mappingConfig.unSelectAll();
                mappingTable.clear();
                mapping_control_buttons.hide();

                for (id in data) {
                    const record = data[id];
                    var mapped = record.mapped;
                    mapped.sort((a, b) => (a.bitOffset > b.bitOffset) ? 1 : -1);
                    try {
                        await api.setTPDOMapping(NodeID, record.mapping, mapped.map((v) => v.index));
                        await api.setTPDOConfig(NodeID, record.communication, getSlotConfig(record));
                        noty_success('Конфигурация загружена в слот' + id);
                    } catch (err) {
                        noty_error('Ошибка загрузки конфигурации в слот ' + id + ' : ' + err);
                    }
                }

                mappingConfig.reload();
            });
            reader.readAsText(files[0]);
        })
        .click();
}

async function SaveTpdaoToFile() {
    var cfg = mappingConfig.getAll(!1);
    var records = {};
    for (i in cfg) {
        const id = cfg[i].mappingID;
        await $.get('../Mapping/' + NodeID, { mappingId: id }, function (data) {
            const current_config = mappingConfig.get(id);
            const mapped = data.records
                .filter(item => item.mapped)
                .map(item => ({
                    index: item.index,
                    bitOffset: item.bitOffset
                }));

            records[id] = {
                cobId: parseInt(current_config.cobId),
                enabled: current_config.enabled,
                transferTypeID: parseInt(current_config.transferTypeID),
                inhibit: parseInt(current_config.inhibit),
                iventTimer: parseInt(current_config.iventTimer),
                syncStart: parseInt(current_config.syncStart),
                mapping: parseInt(current_config.mapping),
                communication: parseInt(current_config.communication),
                mapped: mapped,
            }
        }).promise()
    }

    const blob = new Blob([JSON.stringify(records, null, ' ')], { type: "application/json;charset=utf-8" });
    saveAs(blob, "TPDO.TPDOcfg");
}