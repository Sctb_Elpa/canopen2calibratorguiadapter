﻿Date.prototype["toMMSS"] = function () {
    let minutes = this.getMinutes()
    let seconds = this.getSeconds()

    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return minutes + ':' + seconds
}

$.noty.defaults.theme = "bootstrapTheme"

function noty_error(text: string, timeot: number = 5000) {
    noty({
        type: "error",
        text: "<i class='fa fa-times'></i> Ошибка: " + text,
        timeout: timeot
    });
}

function noty_success(text: string = "Операция выполнена успешно.", timeout: number = 3000) {
    noty({
        type: "success",
        text: "<i class='fa fa-check'></i> " + text,
        timeout: timeout
    });
}

const data_toggle_selector: JQuery.Selector = '[data-toggle="tooltip"]'

function enable_tooltips(selector: any) {
    selector.tooltip();
}

// https://getbootstrap.com/docs/4.0/components/tooltips/
$(function () {
    enable_tooltips($(data_toggle_selector));
})

type TAsincVoidCall = () => Promise<void>;

async function voidApiCall(func: TAsincVoidCall) {
    try {
        await func();
        noty_success();
    } catch (err) {
        noty_error(err);
    }
}

type TBackgroundFuction = (...any) => void;

function startAsync(fun: TBackgroundFuction) {
    setTimeout(fun, 0);
}

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function filedialogdata2string(data: string | ArrayBuffer): string {
    if (typeof data === 'string') {
        return data;
    } else {
        let binary: string = '';
        const bytes = new Uint8Array(data);
        const len = bytes.byteLength;
        for (let i = 0; i < len; i++) {
            binary += String.fromCharCode(bytes[i]);
        }
        return binary;
    }
}

function filedialogdata2base64(data: string | ArrayBuffer): string {
    return btoa(filedialogdata2string(data));
}