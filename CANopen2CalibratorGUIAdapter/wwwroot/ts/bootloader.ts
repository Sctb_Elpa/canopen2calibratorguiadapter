﻿$(function () {
    function updateSummary(node_id: number) {
        $.get('../Summary/' + node_id, (content) => $('#summary').replaceWith(content));
    }

    let api = new API();

    api.registerReversCall('networkEvent', () => { });
    api.registerReversCall('pDOEvent', () => { });
    api.registerReversCall('eMCY', () => { });

    const settings_file: JQuery<HTMLInputElement> = $("#settings-file");

    settings_file.on('change', (e) => {
        const files: FileList = e.target.files;

        if (files.length < 1) { return; }

        const file = files[0];
        const reader = new FileReader();
        reader.onload = (data: ProgressEvent<FileReader>) => {
            startAsync(async () => await voidApiCall(
                async () => {
                    const base64data = filedialogdata2base64(data.target.result)
                    await api.restoreSettingsbackup(parseInt(settings_file.attr('node-id')), base64data);
                }
            ));
        }
        reader.readAsArrayBuffer(file);
    });

    ($('#app-image-file') as JQuery<HTMLInputElement>).on('change', (e) => {
        const files = e.target.files;

        if (files.length < 1) { return; }
        const file = files[0];

        if (file.name.endsWith('.deploy')) {
            $('#app-image-encrypted').prop('checked', true);
        }
    });

    const update_firmware_form: JQuery<HTMLFormElement> = $('#update-firmware');

    update_firmware_form.on('submit', (event) => {
        const formdata = new FormData(update_firmware_form[0]);

        let msg = noty({
            type: "warning",
            text: "<i class='fas fa-spinner fa-pulse'></i> Обновление приложения, пожалуйста подождите...",
        });

        let reader = new FileReader();
        reader.onload = (data) => startAsync(async () => {
            const node_id = parseInt(settings_file.attr('node-id'));

            $.ajax({
                url: "../UploadFirmware/" + node_id,
                cache: false,
                data: JSON.stringify({
                    data: filedialogdata2base64(data.target.result),
                    encrypted: formdata.get('app-image-encrypted') === 'on'
                }),
                success: function () {
                    msg.close();
                    noty_success('Приложение успешно обновлено!');
                    update_firmware_form.trigger('reset');
                    updateSummary(node_id);
                },
                error: function (err) {
                    msg.close();
                    noty_error(err.statusText);
                },
                contentType: 'application/json',
                method: "PUT",
            });

        });

        const b = new Blob([formdata.get('app-image-file')], { type: "application/octet-stream"} );
        reader.readAsArrayBuffer(b);

        event.preventDefault();
    });

    $('#backup-app-settings').on('click', () => {
        const node_id: string = $('#settings-file').attr('node-id');
        saveAs("../GetSettingsBackup/" + node_id, 'node-id-' + node_id + '-backup.settings');
    });

    $('#restore-app-settings').on('click', () => $('#settings-file').trigger('click'));

    $('#clear-app-settings').on('click', () => {
        const node_id: string = $('#settings-file').attr('node-id');
        startAsync(async () => await voidApiCall(
            async () => {
                await api.clearSettings(parseInt(node_id));
            }
        ));
    });
});
