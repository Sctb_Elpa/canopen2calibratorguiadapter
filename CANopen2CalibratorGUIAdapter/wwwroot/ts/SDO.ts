﻿$(function () {
    let api = new API();

    api.registerReversCall('networkEvent', () => { });
    api.registerReversCall('pDOEvent', () => { });
    api.registerReversCall('eMCY', () => { });

    const _tree = $('#tree');
    const node_id = _tree.attr('node-id');

    _tree.tree({
        uiLibrary: 'bootstrap4',
        dataSource: '../LazyDictionary/' + node_id,
        primaryKey: 'index',
        lazyLoading: true,
        border: true,
        iconsLibrary: 'fontawesome',
        select: (_e, _node, _id) => false // запрет выбирать элементы
    });
});

async function getDomainData(nodeID: number, index: string) {
    const filenameRegex: RegExp = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;

    const noty_obj = noty({
        type: "information",
        text: "<i class='fas fa-spinner fa-pulse'></i> Скачивание, пожалуйста подождите...",
    });

    const fr = await fetch("../GetDomain/" + nodeID + "?" + $.param({ "index": index }));

    noty_obj.close();
    if (fr.ok) {
        let filename: string;
        const matches = filenameRegex.exec(fr.headers.get('Content-Disposition'));
        if (matches != null && matches[1]) {
            filename = matches[1].replace(/['"]/g, '');
        } else {
            filename = "NodeID" + nodeID + "-" + index + ".dat";
        }
        saveAs(new Blob([await fr.blob()], { type: fr.headers.get('Content-Type') }), filename);
    } else {
        noty_error(await fr.text());
    }
}