﻿class EmergencyLogItem {
    public id: number;
    public nodeId: number;
    public timestamp: string;
    public errorRegister: number;
    public emcyErrorCode: number;
    public manufacturerSpec: string;
    public emcyErrorDescr: string;
}

//-----------------------------------------------------------

$(function () {
    function date_renderer(value: string, _record: EmergencyLogItem, _$cell: JQuery<HTMLTableCellElement>, _$displayEl: JQuery<HTMLDivElement>): string {
        return (new Date(value)).toLocaleString('ru-RU', {
            month: 'short',
            day: '2-digit',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit'
        });
    }

    function flag_render(value: number, _record: EmergencyLogItem, _$cell: JQuery<HTMLTableCellElement>, $displayEl: JQuery<HTMLDivElement>) {
        class Flag {
            code: string;
            tooltip: string;
        }

        const flags: Array<Flag> = [
            { code: 'M', tooltip: 'Специфично для производителя' },
            { code: 'R', tooltip: 'Зарезервировано' },
            { code: 'P', tooltip: 'Специфично для профиля' },
            { code: 'C', tooltip: 'Сommunication' },
            { code: 'T', tooltip: 'Температура' },
            { code: 'V', tooltip: 'Напряжение' },
            { code: 'I', tooltip: 'Ток' },
            { code: 'G', tooltip: 'Общая ошибка' },
        ];

        $displayEl.empty();

        flags.forEach((flag: Flag, i) => {
            const bit = 7 - i;
            $displayEl.append($('<span class="text-monospace badge badge-' +
                ((value & (1 << bit)) ? 'danger' : 'secondary') + '"'
                + 'title="' + flag.tooltip + '">' + flag.code + '</span>'));
        })
    }

    function code_render(value: number, record: EmergencyLogItem, _$cell: JQuery<HTMLTableCellElement>, $displayEl: JQuery<HTMLDivElement>) {
        $displayEl.empty();
        $displayEl.append($('<span title="' + '0x' +
            ('0000' + value.toString(16)).substring(-4) + '">' + record.emcyErrorDescr + '</span>'));
    }

    let existing_ids = new Set<number>();

    $(function () {
        $('#nav-bar-emergency').addClass('active');
    });

    let log_table: Types.Grid<EmergencyLogItem, null> = $('#emcy-log').grid({
        primaryKey: 'id',
        dataSource: '/Emergency/Log',
        uiLibrary: 'bootstrap4',
        notFoundText: 'Журнал аварийных сообщений пуст',
        columns: [
            { field: 'id', title: '№', align: 'center', width: 40 },
            { field: 'nodeId', title: 'NodeID', align: 'center', width: 40 },
            { field: 'timestamp', title: 'Время', align: 'left', width: 150, renderer: date_renderer },
            { field: 'errorRegister', title: 'Флаги', align: 'center', width: 170, renderer: flag_render },
            { field: 'emcyErrorCode', title: 'Тип ошибки', align: 'center', renderer: code_render },
            { field: 'manufacturerSpec', title: 'Доп.инфо.', align: 'center' }
        ]
    });

    let api = new API();

    api.registerReversCall('networkEvent', () => { });
    api.registerReversCall('pDOEvent', () => { });
    api.registerReversCall('eMCY', (emergencyid: number) => {
        if (!(existing_ids.has(emergencyid))) {
            existing_ids.add(emergencyid);
            $.get('/Emergency/LogEntry/' + emergencyid, (emergencyData: EmergencyLogItem) => {
                log_table.addRow(emergencyData);
            });
        }
    });

    $('#clear_log_btn').on('click', async (_e) => {
        await api.clearEmergencyLog();
        log_table.reload();
    });  
});
