﻿type TonOpen = () => void;
type TonError = (err) => void;
type TonClose = (arg) => void;

interface TPDOSlotConfig {
    enabled: boolean;
    cobId: number;
    transferTypeID: number;
    inhibit: number;
    iventTimer: number;
    syncStart: number;
}

interface _IWebAPI {
    connect(onOpen: TonOpen, onError: TonError, onClose: TonClose);
    nMTCommand(node_id: number, command: string);
    startApp(node_id?: number);
    getLastHeartbeatTimestamp(node_id: number);
    lssResetWaitingGlobal();
    checkNodeIDAvalable(node_id: number): boolean;
    startFastScan(lss_partial_known_string: string);
    assignNodeID(lss: string, nodeID?: number): number;
    changeBusSpeed(grade: number);
    restoreSettingsbackup(nodeID: number, base64dataString: string);
    clearSettings(nodeID: number);
    setEds(nodeID: number, edsData: string);
    setTPDOMapping(nodeID: number, mapping_index: number, mapped_indexies: Array<string>);
    setTPDOConfig(nodeID: number, configIndex: number, config: TPDOSlotConfig);
    clearEmergencyLog();
}

interface IReversCallback {
    (...arg: any): any
}

declare function IWebAPI(url: string): void;

class API {
    private connected: boolean = false;
    private client: _IWebAPI;

    constructor() {
        const api_url: string = "ws://" + document.location.host + "/webAPI";
        this.client = new IWebAPI(api_url);

        this.client.connect(
            () => {
                console.log("websosket connecton opened!")
                this.connected = true;
            },
            (err) => console.log("jsonrpc api error: " + err.summary),
            (arg) => {
                console.log("websosket connecton closed " + arg.reason)
                this.connected = false;
            })
    }

    private check_connection() {
        if (!this.connected) {
            throw new Error('Websocket not ready yet');
        }
    }

    public registerReversCall(name: string, func: IReversCallback) {
        this.client[name] = func;
    }

    //-------------------------------------------

    public globalNMTCommand(command: string) {
        this.check_connection();
        return this.client.nMTCommand(0, command);
    }

    public NMTCommand(nodeID: number, command: string) {
        this.check_connection();
        return this.client.nMTCommand(nodeID, command);
    }

    public startApp(nodeID?: number) {
        this.check_connection();
        return this.client.startApp(nodeID);
    }

    public getLastHeartbeatTimestamp(nodeID: number) {
        this.check_connection();
        return this.client.getLastHeartbeatTimestamp(nodeID);
    }

    public LssResetWaitingGlobal() {
        this.check_connection();
        return this.client.lssResetWaitingGlobal();
    }

    public checkNodeIDAvalable(node_id: number): boolean {
        this.check_connection();
        return this.client.checkNodeIDAvalable(node_id);
    }

    public startFastScan(lss_partial_known_string: string) {
        this.check_connection();
        return this.client.startFastScan(lss_partial_known_string);
    }

    public assignNodeID(lss: string, nodeID: number): number {
        this.check_connection();
        return this.client.assignNodeID(lss, nodeID);
    }

    public changeBusSpeed(grade: number) {
        this.check_connection();
        return this.client.changeBusSpeed(grade);
    }

    public restoreSettingsbackup(nodeID: number, data: string) {
        this.check_connection();
        return this.client.restoreSettingsbackup(nodeID, data);
    }

    public clearSettings(nodeID: number) {
        this.check_connection();
        return this.client.clearSettings(nodeID);
    }

    public setEds(nodeID: number, edsData: string) {
        this.check_connection();
        return this.client.setEds(nodeID, edsData);
    }

    public setTPDOMapping(nodeID: number, mapping_index: number, mapped_indexies: Array<string>) {
        this.check_connection();
        return this.client.setTPDOMapping(nodeID, mapping_index, mapped_indexies);
    }

    public setTPDOConfig(nodeID: number, configIndex: number, config: TPDOSlotConfig) {
        this.check_connection();
        return this.client.setTPDOConfig(nodeID, configIndex, config);
    }

    public clearEmergencyLog() {
        this.check_connection();
        return this.client.clearEmergencyLog();
    }
}