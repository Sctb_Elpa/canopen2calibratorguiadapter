﻿interface LssInputs {
    VID: JQuery<HTMLInputElement>;
    PID: JQuery<HTMLInputElement>;
    Rev: JQuery<HTMLInputElement>;
    Serial: JQuery<HTMLInputElement>;
}

interface IFastScanResult {
    bitN: number;
    error: string;
    fieldName: string;
    productID: number;
    revisionNumber: number;
    serialNumber: number;
    status: string;
    vendorID: number;
}

var api: API = null;
var lss_inputs: LssInputs = null;

//------------------------------------------------------

$(function () {
    $(function () {
        $('#nav-bar-lss').addClass('active');
    });

    api = new API();

    lss_inputs = {
        VID: $("input[aria-describedby='VID-addon']"),
        PID: $("input[aria-describedby='PID-addon']"),
        Rev: $("input[aria-describedby='Revision-addon']"),
        Serial: $("input[aria-describedby='Serial-addon']"),
    };

    api.registerReversCall('networkEvent', () => { });
    api.registerReversCall('pDOEvent', () => { });
    api.registerReversCall('eMCY', () => { });

    ($("input.lss-id-part") as JQuery<HTMLInputElement>)
        .each((_, e) => validateLssElement($(e)))
        .on("keyup paste", (ev: JQuery.TriggeredEvent) => {
            validateLssElement($(ev.target));
            updateLssFastscanCommandsAvalability();
        });

    $("#node-id-input").on('change', (ev: JQuery.ChangeEvent) => {
        $("#assign-node-id").prop("disabled", "disabled");
        checkNodeIDAvalable($(ev.target), updateLssFastscanCommandsAvalability);
    });

    updateLssFastscanCommandsAvalability();

    $('#network-config-file').on('change', (e: JQuery.ChangeEvent) => {
        const files = e.target.files;

        if (files.length < 1) {
            return;
        }
        const file = files[0];

        var reader = new FileReader();
        reader.onload = function (data) {
            $('#network-config').val(filedialogdata2string(data.target.result));
        }
        reader.readAsText(file);
    });
});

//------------------------------------------------------------

function fillTemplate(str: string) {
    return '<span class="input-group-text"><i class="fa ' + str + '"></i></span>';
}

//------------------------------------------------------------

function checkNodeIDAvalable(node_id_input: JQuery<HTMLInputElement>, cb?: () => void) {
    function showErr(msg) {
        icon_holder.append(fillTemplate('fa-times-circle text-danger'));
        noty({
            type: "error",
            text: "<i class='fa fa-times-circle'></i> " + msg,
            timeout: 5000
        });
    }

    const icon_holder = node_id_input.parent().find("div.form-control-feedback") as JQuery<HTMLDivElement>;
    const value = node_id_input.val().toString();

    icon_holder.empty();

    if (value == "") {
        icon_holder.append(fillTemplate('fa-question'));
        return;
    }

    const v = parseInt(value);
    if (v > 0) {
        icon_holder.append(fillTemplate('fa-search'));

        startAsync(async () => {
            try {
                var result = await api.checkNodeIDAvalable(v);
                icon_holder.empty();
                if (result) {
                    icon_holder.append(fillTemplate('fa-check text-success'));
                } else {
                    showErr('Адресс ' + v + ' занят');
                }
            } catch (err) {
                showErr(err);
            }
            if (cb != null) {
                cb();
            }
        });
    } else {
        showErr("\"" + value + "\" не может быть идентификатором узла");
        if (cb != null) {
            cb();
        }
    }
}

function updateLssFastscanCommandsAvalability() {
    const searchButton: JQuery<HTMLButtonElement> = $("#start-fast-scan");
    const NodeIDAssign: JQuery<HTMLButtonElement> = $("#assign-node-id");

    searchButton.attr("title", "Найти");

    let disable = false;
    let fullyAssigned = true;

    ($("input.lss-id-part") as JQuery<HTMLInputElement>).each(function (_i: number, el: HTMLInputElement) {
        const element = $(el);
        disable ||= element.parent().find("svg").hasClass("text-danger");
        fullyAssigned &&= !isNaN(parseInt(element.val().toString()));
    });

    ($("#node-id-input") as JQuery<HTMLInputElement>).each(function (_i: number, element: HTMLInputElement) {
        disable ||= $(element).parent().find("svg").hasClass("text-danger");
    });

    if (disable) {
        searchButton.attr("disabled", "disabled");
        NodeIDAssign.attr("disabled", "disabled");
    } else {
        searchButton.removeAttr("disabled");
        if (fullyAssigned) {
            NodeIDAssign.removeAttr("disabled");
        } else {
            NodeIDAssign.attr("disabled", "disabled");
        }
    }
}

function validateLssElement(element: JQuery<HTMLInputElement>) {
    const icon_holder = element.parent().find("div.form-control-feedback");

    icon_holder.empty();

    const value = element.val().toString();
    if (value == "") {
        // empty
        icon_holder.append(fillTemplate('fa-question'));
    } else {
        const intvalue = parseInt(value);

        if (isNaN(intvalue) || intvalue > 0xffffffff || intvalue < 0) {
            // not valid value
            icon_holder.append(fillTemplate('fa-times-circle text-danger'));
        } else {
            // valid value
            icon_holder.append(fillTemplate('fa-check text-success'));
        }
    }
}

function buildLssRequestData(): string {
    const VID_v = lss_inputs.VID.val();
    const PID_v = lss_inputs.PID.val();
    const Rev_v = lss_inputs.Rev.val();
    const Serial_v = lss_inputs.Serial.val();

    return VID_v + ':' + PID_v + ':' + Rev_v + ':' + Serial_v;
}

function tryParceConfigString(line): [string, number] {
    const re = new RegExp("([\\dxXA-Fa-f]+:[\\dxXA-Fa-f]+:[\\dxXA-Fa-f]+:[\\dxXA-Fa-f]+)=(\\d+)");
    const m = line.match(re);
    if (m == null) {
        throw 'Строка конфигурации "' + line + '" имеет неверный формат!';
    } else {
        return [m[1], parseInt(m[2])];
    }
}

//------------------------------------------------------------

async function resetLssWaiting() {
    await voidApiCall(async () => await api.LssResetWaitingGlobal());
}

async function changeBusSpeed() {
    const selected = $('select[name="BusSpeed"]').children("option:selected");
    const speed_value = selected.text();
    const speed_grade = parseInt(selected.val().toString());

    if (confirm('Изменить скорость работы сети CANOpen на ' + speed_value + '?')) {
        await voidApiCall(async () => await api.changeBusSpeed(speed_grade));
    }
}

async function startFastScan() {
    try {
        const lss_id: IFastScanResult = await perform_fast_scan(buildLssRequestData());

        // update values
        lss_inputs.VID.val("0x" + lss_id.vendorID.toString(16));
        lss_inputs.PID.val("0x" + lss_id.productID.toString(16));
        lss_inputs.Rev.val(lss_id.revisionNumber);
        lss_inputs.Serial.val(lss_id.serialNumber);

        for (const key in lss_inputs) {
            validateLssElement(lss_inputs[key]);
        }

        updateLssFastscanCommandsAvalability();
    } catch (err) {
        if (err === 'no-device') {
            noty_error('Ненастроенных устройств в сети не обнаружено');
        } else {
            noty_error(err);
        }
    }
}

async function assignNodeID() {
    const node_to_assign = parseInt($("#node-id-input").val().toString())
    try {
        await perform_node_assign(buildLssRequestData(), node_to_assign);

        // clear form
        ($('#lss-config-form') as JQuery<HTMLFormElement>)[0].reset();

        for (const e in lss_inputs) {
            validateLssElement(lss_inputs[e]);
        }
        checkNodeIDAvalable($("#node-id-input"), null);
        updateLssFastscanCommandsAvalability();

        reload_network_config();
    } catch (err) {
        noty_error(err);
    }
}

async function perform_fast_scan(partial_known_lssID: string): Promise<IFastScanResult> {
    const enum enStatus {
        FINISHED,
        ERROR,
    };

    let result: string | IFastScanResult | null = null;
    let status: enStatus;

    function createReportListener(noty_inst: Noty) {
        return (msg: IFastScanResult) => {
            switch (msg.status) {
                case "progress":
                    const pos = 31 - msg.bitN;
                    noty_inst.setText(
                        "<div>" +
                        "<p><i class='fa fa-search'></i> Просмотр битов " + msg.fieldName + "</p>" +
                        "<div class='progress'>" +
                        "<div class='progress-bar' role='progressbar' " +
                        "aria-valuenow='" + pos + "' aria-valuemin='0' " +
                        "aria-valuemax='31' style='width:" + ((pos / 31) * 100) + "%;'></div>" +
                        "</div >");
                    return;

                case "finished":
                    status = enStatus.FINISHED;
                    result = msg;
                    break;

                case "error":
                    status = enStatus.ERROR;
                    result = (msg.error == 'No unconfigured devices found')
                        ? 'no-device' : msg.error;
                    break;

                default:
                    status = enStatus.ERROR;
                    result = null;
                    break;
            }

            api.registerReversCall('fastScanProgress', null);
        }
    }

    let noty_obj: Noty = noty({
        type: "information",
        text: "<i class='fas fa-spinner fa-pulse'></i> Поиск устройств начат...",
    });
    api.registerReversCall('fastScanProgress', createReportListener(noty_obj));

    try {
        await api.startFastScan(partial_known_lssID);
        noty_obj.close();
    } catch (err) {
        noty_obj.close();

        throw err;
    } finally {
        // unregister
        api.registerReversCall('fastScanProgress', () => { });
    }

    if (status == enStatus.FINISHED && typeof result != 'string') {
        return result;
    }
    throw result;
}

async function perform_node_assign(lss_id: string, node_id: number): Promise<number> {
    const assigned_node_id = await api.assignNodeID(lss_id, node_id);
    noty({
        type: "success",
        text: "<i class='fa fa-check'></i> Назначен NodeID " + assigned_node_id,
        timeout: 5000
    });
    return assigned_node_id;
}

async function autoConfigureNetwork() {
    function extractLssId(obj) {
        return obj.vendorID + ':' + obj.productID + ':' + obj.revisionNumber + ':' + obj.serialNumber;
    }

    const button = $('#net-auto-config') as JQuery<HTMLButtonElement>;

    if (confirm('Настроить сеть автоматически? Будет циклически запущена операция ' +
        'FastScan пока в сети остаются ненастроенные устройства.')) {

        button.prop('disabled', 'disabled');

        let num_of_devs_found = 0;
        try {
            await api.globalNMTCommand('reset'); // reset all
            await sleep(100);

            while (true) {
                var lss_id = extractLssId(await perform_fast_scan(':::'));
                await perform_node_assign(lss_id, null);
                ++num_of_devs_found;
            }
        } catch (err) {
            if (err == 'no-device') {
                if (num_of_devs_found > 0) {
                    noty_success('Настроена есть из ' + num_of_devs_found + ' устройств(а)');
                } else {
                    noty_error('Не удалось обнаружить ни одного устройства!');
                }
            } else {
                noty_error(err);
            }
            await sleep(100);
            reload_network_config();
        }

        button.removeProp('disabled');
    }
}

function reload_network_config() {
    setTimeout(() => {
        $.get(document.location.pathname + '/NetworkConfig', (data: string) => $("#network-config").val(data));
    }, 500);
}

async function applyNetworkConfig() {
    const text = ($("#network-config") as JQuery<HTMLTextAreaElement>).val().toString();
    const lines = text.split('\n');

    let success = true;
    for (var i in lines) {
        const cfg = tryParceConfigString(lines[i].trim());
        try {
            await perform_node_assign(cfg[0], cfg[1]);
        } catch (err) {
            success &&= false;
            noty_error('Ошибка назначения адреса ' + cfg[1] + ' устройству ' + cfg[0] + ' (' + err + ')');
        }
    }
    if (success) {
        noty_success('Конфигурация сети применена');
    } else {
        noty({
            type: "warning",
            text: "<i class='fa fa-check'></i> Конфигурация сети применена не полностью!",
            timeout: 500
        })
    }
}

function downloadNetworkConfig() {
    const blob = new Blob([$("#network-config").val().toString()], { type: "text/plain;charset=utf-8" });
    saveAs(blob, "Network.lsscfg");
}

function uploadNetworkConfig() {
    $('#network-config-file').trigger('click');
}