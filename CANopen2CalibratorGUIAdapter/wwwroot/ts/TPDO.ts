﻿
interface SlotConfig {
    enabled: boolean,
    cobId: number,
    transferTypeID: number,
    inhibit: number,
    iventTimer: number,
    syncStart: number,
}

interface IMapping {
    record: IMappableIndexModel,
    row: JQuery<HTMLTableRowElement>
}

interface IModel {
    mapping: Map<string, IMapping>,
    maxMappingSize: number,
}

interface IGraphSerie {
    name: string,
    value: number
}

interface IGraphDef {
    categories: Array<string>,
    dataset: Array<Map<string, IGraphSerie>>
}

interface IUv {
    chart: (chart_type: string, data: IGraphDef, config) => void;
}

declare var uv: IUv;

//------------------------------------------------------

interface ISlotSave {
    transferTypeID: number;
    inhibit: number;
    iventTimer: number;
    syncStart: number;
    mapped: Array<IMappableIndexModelBase>;
}

interface ITPDOSlotConfig {
    enabled: boolean;
    cobId: number;
    transferTypeID: number;
    inhibit: number;
    iventTimer: number;
    syncStart: number;
}

interface IMappableIndexModelBase {
    index: string;
    bitOffset?: string; //number;
}

interface IMappableIndexModel extends IMappableIndexModelBase {
    name: string;
    type: string;
    bitSize: string; //number;
    mapped: boolean;
}

interface ITPDOConfigModel {
    mappingID: string; // number
    enabled: boolean;
    communication: number;
    mapping: number;
    cobId: string; // number
    transferTypeID: number;
    transferType: string;
    inhibit: string; // number
    iventTimer: string; // number
    syncStart: string; // number
    maxMappingSize: number;
    configEnabled: boolean;
    mappingEnabled: boolean;
}

interface ICountedTPDOConfigModel {
    records: Array<ITPDOConfigModel>,
    total: number
}

interface ICountedMappableIndexModel {
    records: Array<IMappableIndexModel>,
    total: number
}

interface ISaveTPDOSlotConfig {
    cobId: string;
    enabled: boolean;
    transferTypeID: number;
    inhibit: number;
    iventTimer: number;
    syncStart: number;
    mapping: number;
    communication: number;
    mapped: Array<IMappableIndexModel>;
}

//------------------------------------------------------

const palete: Array<string> = ['#00BBC9', '#EC63AB', '#AA8AE4', '#83CE44', '#ff8f25', '#009EAA', '#CA4F7F', '#9C70C0', '#6BAF3B'];

let mappingTable: Types.Grid<IMappableIndexModel, any>;
let mappingConfig: Types.Grid<ITPDOConfigModel, any>;
let mapping_control_buttons: JQuery<HTMLDivElement>;

let model: IModel = {
    mapping: null,
    maxMappingSize: 0
};

//------------------------------------------------------

function getMappingConfigById(id: number): ITPDOConfigModel {
    return mappingConfig.get(id) as unknown as ITPDOConfigModel;
}

function Model2TPDOSlotConfig(v: ITPDOConfigModel): TPDOSlotConfig {
    return {
        enabled: v.enabled,
        cobId: parseInt(v.cobId),
        transferTypeID: v.transferTypeID,
        inhibit: parseInt(v.inhibit),
        iventTimer: parseInt(v.iventTimer),
        syncStart: parseInt(v.syncStart),
    };
}

function SaveTPDOSlotConfig2TPDOSlotConfig(v: ISaveTPDOSlotConfig, node_id: number): TPDOSlotConfig {
    let cobid = v.cobId.toUpperCase();

    let correct_cob_id = cobid.startsWith("$NODEID+")
        ? parseInt(cobid.replace("$NODEID+", "")) + node_id
        : parseInt(cobid);

    return {
        enabled: v.enabled,
        cobId: correct_cob_id,
        transferTypeID: v.transferTypeID,
        inhibit: v.inhibit,
        iventTimer: v.iventTimer,
        syncStart: v.syncStart,
    };
}

//------------------------------------------------------

function getMppingRowById(id: string): IMappableIndexModel {
    // from gijgo.js
    var c, d;
    const e = mappingTable.getAll(!1);
    const f = mappingTable.data("primaryKey");
    let g = void 0;
    if (f) {
        for (d = 0; d < e.length; d++)
            if (e[d][f] == id) {
                c = d + 1;
                break
            }
    } else {
        c = id;
    }
    return c && (g = mappingTable.children("tbody").children('tr[data-position="' + c + '"]')), g
}

function calc_unused_bits(): number {
    let unused_bits: number = 8 * 8;
    for (const key in model.mapping) {
        unused_bits -= model.mapping[key].record.bitSize;
    }
    return unused_bits;
}

// TODO: model typing
function getOrderedMappingScematic(): Array<IMapping> {
    let res: Array<IMapping> = Object.values(model.mapping);
    return res.sort((a, b) => {
        let _a = parseInt(a.record.bitOffset);
        let _b = parseInt(b.record.bitOffset);
        return _a - _b;
    });
}

function draw_usage_scematic() {
    $('#uv-div').empty();

    const unused_bits = calc_unused_bits();

    var graphdef: IGraphDef = {
        categories: [],
        dataset: new Array<Map<string, IGraphSerie>>()
    };

    const orderedMappingScematic = getOrderedMappingScematic();
    graphdef.categories = orderedMappingScematic.map(item => item.record.index);
    for (const value of Object.values(orderedMappingScematic)) {
        graphdef.dataset[value.record.index] = [
            { name: 'value', value: parseInt(value.record.bitSize) } as IGraphSerie
        ];
    }

    if (unused_bits > 0) {
        graphdef.categories.push('Свободно');
        graphdef.dataset['Свободно'] = [
            { name: 'value', value: unused_bits } as IGraphSerie
        ];
    };

    const chartconfig = {
        graph: {
            custompalette: palete,
            bgcolor: 'none',
            max: 64, // не работает
            min: 0
        },
        dimension: {
            height: 20
        },
        margin: {
            top: 1,
            bottom: 1,
            left: 1,
            right: 1
        },
        axis: {
            showticks: false,
            showsubticks: false,
            showtext: false,
            showhortext: false,
            showvertext: false
        },
        frame: {
            bgcolor: 'none'
        },
        legend: {
            showlegends: false
        },
        label: {
            postfix: ' Бит',
            fontfamily: 'PT Sans'
        },
        effects: {
            duration: 100,
        },
        tooltip: {
            format: '%c: %v бит'
        }
    };

    uv.chart('StackedBar', graphdef, chartconfig);
}

function paint_rows() {
    mappingTable.find('tr[data-position]').css('background-color', '');
    Object.values(getOrderedMappingScematic()).map((v, index) => v.row.css('background-color', palete[index]));
}

function update_offsets() {
    let offset: number = 0;
    const sorted_indexies = getOrderedMappingScematic();

    for (const mapped_object of Object.values(sorted_indexies)) {
        mapped_object.record.bitOffset = offset.toString();
        offset += parseInt(mapped_object.record.bitSize);
    }
}

//------------------------------------------------------

$(function () {
    let api = new API();
    const NodeID = parseInt($('#node-id-ancor').attr('node-id'));

    function hex_render(value: number): string {
        return '0x' + value.toString(16);
    }

    function mapping_config_renderer(_value, record: ITPDOConfigModel, _$cell: JQuery<HTMLTableCellElement>, $displayEl: JQuery<HTMLDivElement>, id: number) {
        if (record.configEnabled) {
            const template = '<button class="btn btn-light btn-sm" title="TITLE"><i class="ICON"></i></button>';
            const btnGroupTemplate = '<div class="btn-group" role="group"></div>';

            const $btnGroups = [$(btnGroupTemplate), $(btnGroupTemplate).addClass('ml-2')];

            const $editButton = $(template.replace('ICON', 'far fa-edit').replace('TITLE', 'Изменить'));
            const $applyButton = $(template.replace('ICON', 'fa fa-check text-success').replace('TITLE', 'Применить')).hide()
            const $cancelButton = $(template.replace('ICON', 'fa fa-undo text-danger').replace('TITLE', 'Отмена')).hide()

            const $saveasButton = $(template.replace('ICON', 'far fa-save').replace('TITLE', 'Сохранить в файл...'));
            const $loadButton = $(template.replace('ICON', 'fa fa-file-upload').replace('TITLE', 'Загрузить из файла...'));

            function restore_default_button_set() {
                $editButton.show();
                $applyButton.hide();
                $cancelButton.hide();

                $saveasButton.show();
                $loadButton.show();
            }

            $editButton.on('click', (_ev) => {
                mappingConfig.edit(id.toString());

                $editButton.hide();
                $applyButton.show();
                $cancelButton.show();

                $saveasButton.hide();
                $loadButton.hide();
            });

            $applyButton.on('click', (_ev) => {
                mappingConfig.update(id.toString());
                restore_default_button_set();

                startAsync(async () => {
                    const row = getMappingConfigById(id);
                    // Баг: может случиться так, что transferTypeId == null, хотя все отобразжается
                    // вместе с тем, если трогать его в эдиторе, то значение корректно
                    // костыль: перечитаем значение с сервера
                    if (row.transferTypeID == null) {
                        await $.getJSON('../MappingConfig/' + NodeID, (data: ICountedTPDOConfigModel) => {
                            const row_data = data.records.find((d) => d.mappingID.toString() == id.toString());
                            row.transferType = row_data.transferType;
                            row.transferTypeID = row_data.transferTypeID;
                        }).promise();
                    }
                    try {
                        await api.setTPDOConfig(NodeID, row.communication, Model2TPDOSlotConfig(row));
                    } catch (err) {
                        noty_error('Ошибка применения конфигурации: ' + err);
                        mappingConfig.reload();
                        return;
                    }
                    mappingConfig.updateRow(row.mappingID, row);
                    noty_success('Конфигурация применена');
                });
            });

            $cancelButton.on('click', (_ev) => {
                mappingConfig.cancel(id.toString())
                restore_default_button_set();
            });

            $saveasButton.on('click', () => {
                $.get('../Mapping/' + NodeID, { mappingId: id }, function (data: ICountedMappableIndexModel) {
                    const current_config = getMappingConfigById(id);
                    const mapped: Array<IMappableIndexModelBase> = data.records
                        .filter(item => item.mapped)
                        .map(item => ({
                            index: item.index,
                            bitOffset: item.bitOffset
                        }));

                    const save_data: ISlotSave = {
                        transferTypeID: current_config.transferTypeID,
                        inhibit: parseInt(current_config.inhibit),
                        iventTimer: parseInt(current_config.iventTimer),
                        syncStart: parseInt(current_config.syncStart),
                        mapped: mapped
                    };

                    const blob = new Blob([JSON.stringify(save_data, null, ' ')], { type: "application/json;charset=utf-8" });
                    saveAs(blob, "config.TPDOmap");
                });
            });

            $loadButton.on('click', () => ($('#tpdo-config-input') as JQuery<HTMLInputElement>)
                .off()
                .val('')
                .on('change', (e) => {
                    const files = e.target.files;
                    if (files.length < 1) {
                        return;
                    }
                    var reader = new FileReader();
                    reader.onload = (d) => LoadMapConfig(id, filedialogdata2string(d.target.result));
                    reader.readAsText(files[0]);
                })
                .trigger('click')
            );

            $btnGroups[0]
                .append($editButton)
                .append($applyButton)
                .append($cancelButton);

            $btnGroups[1]
                .append($saveasButton)
                .append($loadButton)

            $displayEl.empty().append($btnGroups);
        }
    }


    function map_controller_renderer(_value, record: IMappableIndexModel, _$cell: JQuery<HTMLTableCellElement>, $displayEl: JQuery<HTMLDivElement>, id: string) {
        function model_can_append(record) {
            if (calc_unused_bits() < record.bitSize) {
                noty_error('Недостаточно места в сообщении для маппинга индекса ' + record.index);
                return false;
            }
            if (Object.keys(model.mapping).length == model.maxMappingSize) {
                noty_error('Невозможно добавить маппинг, исчерпаны конфигурационные поля');
                return false;
            }
            return true;
        }

        const template = '<button class="btn btn-light btn-sm"><i class="far ICON"></i></button>';

        const $addbutton = $(template.replace('ICON', 'fa-square'));
        const $removebutton = $(template.replace('ICON', 'fa-check-square'));

        if (record.mapped) {
            $addbutton.hide();
        } else {
            $removebutton.hide();
        }

        $displayEl.empty().append($addbutton).append($removebutton);

        $addbutton.on('click', (_ev) => {
            if (model_can_append(record)) {
                $addbutton.hide();
                $removebutton.show();

                const mod_record = mappingTable.getById(id);
                mod_record.mapped = true;
                mod_record.bitOffset =
                    Object.values(model.mapping)
                        .reduce((a: number, v: IMapping) => a + parseInt(v.record.bitSize), 0)
                        .toString();
                mappingTable.updateRow(id, mod_record);

                model.mapping[record.index] = {
                    record: mod_record,
                    row: getMppingRowById(id)
                };

                paint_rows();
                draw_usage_scematic();

                mapping_control_buttons.fadeIn(100);
            }
        });
        $removebutton.on('click', (_ev) => {
            $addbutton.show();
            $removebutton.hide();

            delete model.mapping[record.index];

            const mod_record = mappingTable.getById(id);
            mod_record.mapped = false;
            mod_record.bitOffset = null;
            mappingTable.updateRow(id, mod_record);

            update_offsets();

            // update rows
            for (const [ri, rec] of Object.entries(model.mapping)) {
                mappingTable.updateRow(ri, rec.record);
            }

            paint_rows();
            draw_usage_scematic();

            mapping_control_buttons.fadeIn(100);
        });
    }

    const LoadMapConfig = (slotId: number, json_data: string) => {
        let data: ISlotSave;
        try {
            data = JSON.parse(json_data);
        } catch (err) {
            noty_error('Некорректный файл конфигурации!');
            return;
        }
    
        var configData = getMappingConfigById(slotId);
        configData.transferTypeID = data.transferTypeID;
        configData.inhibit = data.inhibit.toString();
        configData.iventTimer = data.iventTimer.toString();
        configData.syncStart = data.syncStart.toString();
        mappingConfig.updateRow(slotId.toString(), configData);

        // clear mapping
        for (let row = 1; row <= mappingTable.count(); ++row) {
            const rowData = mappingTable.get(row) as unknown as IMappableIndexModel;
            rowData.mapped = false;
            rowData.bitOffset = null;
            mappingTable.updateRow(rowData.index, rowData);
        }

        // load mapping
        model.mapping = new Map<string, IMapping>();
        for (const m of Object.values(data.mapped)) {
            var record = mappingTable.getById(m.index);
            record.mapped = true;
            record.bitOffset = m.bitOffset;
    
            mappingTable.updateRow(m.index, record);
            model.mapping[m.index] = {
                record: record,
                row: getMppingRowById(m.index)
            };
        }
    
        paint_rows();
        draw_usage_scematic();

        startAsync(async () => {
            // save
            try {
                await api.setTPDOMapping(NodeID, configData.mapping, Object.keys(model.mapping));
                await api.setTPDOConfig(NodeID, configData.communication, Model2TPDOSlotConfig(configData));
            } catch (err) {
                noty_error('Ошибка загрузки конфигурации: ' + err);
                mappingConfig.reload();
                return;
            }
            noty_success('Конфигурация загружена в слот' + slotId);
        });
    }

    api.registerReversCall('networkEvent', () => { });
    api.registerReversCall('pDOEvent', () => { });
    api.registerReversCall('eMCY', () => { });

    mapping_control_buttons = $('#mapping-control-buttons') as JQuery<HTMLDivElement>;

    mapping_control_buttons.hide();

    mappingConfig = $('#pdo-mappings').grid({
        primaryKey: 'mappingID',
        dataSource: '../MappingConfig/' + NodeID,
        uiLibrary: 'bootstrap4',
        inlineEditing: { mode: 'command', managementColumn: false },
        columns: [
            { field: 'mappingID', title: '№', align: 'center', width: 40 },
            { field: 'enabled', title: 'Вкл?', align: 'center', width: 70, type: 'checkbox', editor: true },
            { field: 'communication', title: 'Индекс', align: 'center', width: 100, renderer: hex_render },
            { field: 'mapping', title: 'Маппинг', align: 'center', width: 100, renderer: hex_render },
            { field: 'cobId', title: 'COB-ID', align: 'center', editor: true, width: 100 },
            {
                field: 'transferType',
                title: 'Режим',
                type: 'dropdown',
                editor: { dataSource: '../TPDOModes', valueField: 'transferTypeID' },
                editField: 'transferTypeID'
            },
            { field: 'inhibit', title: 'Подавление, *0,1 мс', editor: true, align: 'center', width: 150 },
            { field: 'iventTimer', title: 'Таймер, мс', editor: true, align: 'center', width: 100 },
            { field: 'syncStart', title: '#SYNC', editor: true, align: 'center', width: 100 },
            {
                title: '',
                align: 'center',
                width: 130,
                renderer: mapping_config_renderer
            }
        ]
    });

    mappingTable = $('#pdo-mappable-indexes').grid({
        title: 'Индексы, доступные для маппинга',
        primaryKey: 'index',
        dataSource: '../Mapping/' + NodeID,
        uiLibrary: 'bootstrap4',
        fixedHeader: true,
        autoLoad: false,
        columns: [
            { align: 'center', width: 80, renderer: map_controller_renderer },
            { field: 'bitOffset', title: 'Смещение', align: 'center', width: 100 },
            { field: 'index', title: 'Индекс', align: 'right', width: 100 },
            { field: 'name', title: 'Название' },
            { field: 'type', title: 'Тип данных', align: 'right', width: 150 },
            { field: 'bitSize', title: 'Бит', align: 'center', width: 80 },
        ],
    });

    mappingTable.on('dataBound', (_e, records: Array<IMappableIndexModel>, _totalRecords: number) => {
        model.mapping = new Map<string, IMapping>;
        for (const ri in records) {
            const record = records[ri];
            if (record.mapped) {
                model.mapping[record.index] = {
                    record: record,
                    row: mappingTable.find('tr[data-position="' + (parseInt(ri) + 1) + '"]'),
                };
            }
        }

        paint_rows();
        draw_usage_scematic();
    });

    mappingConfig.on('rowSelect', (e, $row: JQuery<HTMLTableRowElement>, id: number, record: ITPDOConfigModel) => {
        mappingTable.reload({ mappingId: id });
        mappingTable.attr('disabled', (!record.mappingEnabled).toString());
        mappingTable.data('mapping-id', id);
        model.maxMappingSize = record.maxMappingSize;

        mapping_control_buttons.hide();
    });

    $('#save_tpdo_btn').on('click', async (_e) => {
        const cfg = mappingConfig.getAll(!1);
        var records = {};
        for (const i in cfg) {
            const id = parseInt(cfg[i].mappingID);
            await $.get('../Mapping/' + NodeID, { mappingId: id }, (data: ICountedMappableIndexModel) => {
                const current_config = cfg[i];

                const mapped = data.records
                    .filter(item => item.mapped)
                    .map(item => ({
                        index: item.index,
                        bitOffset: item.bitOffset
                    }));

                records[id] = {
                    cobId: "$NODEID+" + (parseInt(current_config.cobId) - NodeID),
                    enabled: current_config.enabled,
                    transferTypeID: current_config.transferTypeID,
                    inhibit: parseInt(current_config.inhibit),
                    iventTimer: parseInt(current_config.iventTimer),
                    syncStart: parseInt(current_config.syncStart),
                    mapping: current_config.mapping,
                    communication: current_config.communication,
                    mapped: mapped,
                }
            }).promise()
        }

        const blob = new Blob([JSON.stringify(records, null, ' ')],
            { type: "application/json;charset=utf-8" }
        );
        saveAs(blob, "TPDO.TPDOcfg");
    });

    $('#load_tpdo_btn').on('click', (_e) => {
        ($('#tpdo-global-config-input') as JQuery<HTMLInputElement>)
            .off()
            .val('')
            .on('change', (e) => {
                const files = e.target.files;
                if (files.length < 1) {
                    return;
                }
                var reader = new FileReader();
                reader.onload = (d) => startAsync(async () => {
                    let data: Array<ISaveTPDOSlotConfig>;
                    try {
                        data = JSON.parse(filedialogdata2string(d.target.result));
                    } catch (err) {
                        noty_error('Некорректный файл конфигурации TPDO!');
                        return;
                    }

                    mappingConfig.unSelectAll();
                    mappingTable.clear();
                    mapping_control_buttons.hide();

                    for (const id in data) {
                        const record = data[id];
                        var mapped = record.mapped;
                        mapped.sort((a, b) => (a.bitOffset > b.bitOffset) ? 1 : -1);
                        try {
                            await api.setTPDOMapping(NodeID, record.mapping, mapped.map((v) => v.index));
                            await api.setTPDOConfig(NodeID, record.communication, SaveTPDOSlotConfig2TPDOSlotConfig(record, NodeID));
                            noty_success('Конфигурация загружена в слот' + id);
                        } catch (err) {
                            noty_error('Ошибка загрузки конфигурации в слот ' + id + ' : ' + err);
                        }
                    }

                    mappingConfig.reload();
                });
                reader.readAsText(files[0]);
            })
            .trigger('click');
    });

    $('#apply_mapping_btn').on('click', async (_e) => {
        try {
            const selected_mapping = getMappingConfigById(mappingTable.data('mapping-id'));
            await api.setTPDOMapping(NodeID, selected_mapping.mapping, Object.keys(model.mapping));
        } catch (err) {
            noty_error('Не удалось применить маппинг: ' + err);
            return;
        }
        noty_success('Маппинг был применен успешно');

        mapping_control_buttons.hide();
    });

    $('#reset_mapping_btn').on('click', async (_e) => {
        mappingTable.reload({ mapping: mappingTable.data('mapping-id') });
        mapping_control_buttons.hide();
    });
});
