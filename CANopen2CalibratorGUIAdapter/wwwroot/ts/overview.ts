﻿interface ITPDOItem {
    index: string;
    name: string;
    value: string;
    timestamp: string;
}

const fadein_speed = 'slow';

type TInProgress = 'InProgress';
type Card = JQuery<HTMLDivElement> | TInProgress;

var api: API = null;

const cards = new Map<number, Card>();
const table_map = new Map<number, Types.Grid<ITPDOItem, null>>();
const heartbeats = new Map<number, Date>();

//------------------------------------------------------

$(function () {
    $(function () {
        $('#nav-bar-overview').addClass('active');
    });

    api = new API();

    api.registerReversCall('eMCY', () => { });

    api.registerReversCall('networkEvent', function (nodeid: number, event: string) {
        switch (event) {
            case 'HEARTBEAT':
                update_heartbeat(nodeid);
                break;
            case 'STATE_CHANGED':
                startAsync(async () => await reload_card(nodeid));
                break;
            case 'REMOVE':
                remove_card(nodeid);
                updateStartAppButtonState();
                break;
            case 'REMOVE_ALL':
                removeAllCards();
                $('start_app_btn').hide();
                break;
            default:
                console.log('Unknown event: ' + event);
                break;
        }
    });

    api.registerReversCall('pDOEvent', function (nodeId: number, timestamp: string, resultIndex: string, name: string, value: string) {
        const table: Types.Grid<ITPDOItem, null> = table_map.get(nodeId);
        if (table != null) {
            const row = table.getById(resultIndex);
            if (row && (row.value != value)) {
                // обновить
                row.timestamp = timestamp;
                row.value = value;
                table.updateRow(resultIndex, row);
            } else if (row === null) {
                // добавить ряд
                table.addRow({
                    index: resultIndex,
                    name: name,
                    timestamp: timestamp,
                    value: value
                });
            }
        }
    });

    setTimeout(function updateHeartbeat() {
        const now = new Date();
        cards.forEach(($c, _i) => {
            if ($c && $c !== 'InProgress') {
                const last_hearbeat = heartbeats.get(parseInt($c.attr('node-id')));
                let diff_ms = 0;
                if (last_hearbeat) {
                    diff_ms = now.valueOf() - last_hearbeat.valueOf();
                }
                setHeartbeatStatus($c, diff_ms / 1000.0);
            }
        });
        setTimeout(updateHeartbeat, 1000);
    }, 1000);

    // if any device registred
    $.get('/LSS/NetworkConfig', (data: string) => { if (data) loadCardsSet(); });
});

//------------------------------------------------------

function find_card(card_id: number): Card { return cards.get(card_id); }

function setHeartbeatStatus($card: JQuery<HTMLDivElement>, heartbeat_td: number) {
    function generateInfo(td) {
        if (!td) {
            return { heartbeatToolTip: "N/A", heartBitIconColor: 'secondary' }
        } else if (td <= 1000) {
            return { heartbeatToolTip: "Менее секунды", heartBitIconColor: 'success' }
        } else if (td <= 5000) {
            return { heartbeatToolTip: td / 1000 + " секунд", heartBitIconColor: 'warning' }
        } else {
            return { heartbeatToolTip: Math.round(td / 1000) + " секунд", heartBitIconColor: 'danger' }
        }
    }

    const heartbeat_info = generateInfo(heartbeat_td);

    const $heartbit_icon = $card.find("span[data-title]") as JQuery<HTMLSpanElement>;

    if ($heartbit_icon.attr('data-title') != heartbeat_info.heartbeatToolTip) {
        $heartbit_icon.attr('data-title', heartbeat_info.heartbeatToolTip);
        $heartbit_icon.attr('data-original-title', heartbeat_info.heartbeatToolTip);
    }

    const $svg = $heartbit_icon.find('svg');

    const new_color_class = 'text-' + heartbeat_info.heartBitIconColor;
    if (!$svg.hasClass(new_color_class)) {
        $svg.removeClass((_, className) => (className.match(/(^|\s)text-\S+/g) || []).join(' '))
        $svg.addClass(new_color_class);
    }
}

async function loadCardsSet() {
    await $.get('/Overview/Cards', (data: HTMLElement) => {
        const $d = $(data)
        enable_tooltips($d.find(data_toggle_selector));
        $d.hide().fadeIn(fadein_speed);

        const $container = $('#device-list-cotainer') as JQuery<HTMLDivElement>;
        $container.empty();
        ($d.find('div.card') as JQuery<HTMLDivElement>).each((_i, c: HTMLDivElement) => {
            const $c = $(c);
            const _lCard_id = parseInt($c.attr('node-id'));
            table_map.set(_lCard_id, loadTPDOTable($c.find('table'), _lCard_id));
            cards.set(_lCard_id, $c);
        });
        $container.append($d);

        updateStartAppButtonState();
    }).promise();
}

async function create_card(card_id: number) {
    const prev_card = find_card(card_id - 1);

    cards.set(card_id, 'InProgress');
    if (prev_card == null || prev_card === 'InProgress') {
        await loadCardsSet();
    } else {
        await $.get('/Overview/NodeCard/' + card_id,
            (data: HTMLDivElement) => {
                var $card = $(data);
                enable_tooltips($card.find(data_toggle_selector));
                $card.hide().fadeIn(fadein_speed);
                var pdoTable = $card.find('table');
                if (pdoTable.length > 0) {
                    table_map.set(card_id, loadTPDOTable(pdoTable, card_id));
                }
                prev_card.after($card);
                cards.set(card_id, $card);
            }
        ).promise();
    }
}

async function update_heartbeat(card_id: number) {
    const card = find_card(card_id);

    if (!card) {
        await create_card(card_id);
        return;
    }
    if (card === 'InProgress') {
        return;
    }

    try {
        heartbeats.set(card_id, new Date(await api.getLastHeartbeatTimestamp(card_id)));
    } catch (err) {
        noty_error(err);
    }
}

// todo: typing
function loadTPDOTable($table: JQuery<HTMLTableElement>, NodeID: number): Types.Grid<ITPDOItem, null> {
    if ($table.length == 0) {
        // Если карточка не в режиме TPDO - нечего загружать, скип.
        return null;
    }
    const table: Types.Grid<ITPDOItem, null> = $table.grid({
        uiLibrary: 'bootstrap4',
        primaryKey: 'index',
        // Вместо загрузки маппинга целиком сразу будем обновлять таблицу по мере прихода ивентов
        //dataSource: '/Overview/GetTPDOMapping/' + NodeID, 
        notFoundText: 'Данные отсутствуют',
        columns: [
            { field: 'name', title: 'Переменная', align: 'left' },
            { field: 'value', title: 'Значение', align: 'right' }
        ]
    });

    table.on('rowDataBound', function (_e, $row: JQuery<HTMLTableRowElement>, id: string, record: ITPDOItem) {
        const cols = $row.find('div[title]') as JQuery<HTMLDivElement>;
        $(cols[0]).attr('title', '[' + id + ']');

        const timestamp = record.timestamp
            ? 'Получено в: ' + (new Date(record.timestamp)).toLocaleString('ru-RU', {
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit'
            })
            : "Данные не получены";
        $(cols[1]).attr('title', timestamp);
    });

    return table;
}

async function reload_card(card_id: number) {
    // todo typing
    const replace_table = function ($c: JQuery<HTMLDivElement>) {
        var _lCard_id = parseInt($c.attr('node-id'));
        table_map.set(card_id, loadTPDOTable($c.find('table'), _lCard_id));
    }

    const card = find_card(card_id);
    const card2replace = card;
    if (!card) {
        await create_card(card_id);
    } else {
        if (card === 'InProgress') {
            return;
        }

        cards.set(card_id, 'InProgress');
        await $.get(document.location.pathname + '/NodeCard/' + card_id,
            (data: HTMLDivElement) => {
                const $new_card = $(data);
                enable_tooltips($new_card.find(data_toggle_selector));
                $new_card.hide().fadeIn(fadein_speed);

                if (card2replace === 'InProgress') {
                    startAsync(async () => {
                        while (true) {
                            const $oldcard = $('div.card[node-id]');
                            if ($oldcard.length > 0) {
                                $oldcard.replaceWith($new_card);
                                replace_table($new_card);
                                return;
                            }
                            await sleep(1000);
                        }
                    });
                } else {
                    $(card2replace).replaceWith($new_card);
                    replace_table($new_card);
                }
                cards.set(card_id, $new_card);

                updateStartAppButtonState();
            }).promise();
    }
}

function remove_card(card_id: number) {
    let card = find_card(card_id)
    if (typeof card !== 'string') {
        card.remove();
    }
    cards.delete(card_id);
    table_map.delete(card_id);
    heartbeats.delete(card_id)
}

function removeAllCards() {
    // clear card data
    cards.clear();
    table_map.clear();
    heartbeats.clear();

    $('#device-list-cotainer').load(document.location.pathname + '/Cards');
}

//------------------------------------------------------

async function NMTGlobal(command: string) {
    await voidApiCall(async () => await api.globalNMTCommand(command))
}

async function NMTTarget(nodeID: string, command: string) {
    await voidApiCall(async () => await api.NMTCommand(parseInt(nodeID), command))
}

async function startApp(nodeID: string) {
    await voidApiCall(async () => await api.startApp(parseInt(nodeID)))
}

function uploadNodeEds(nodeID) {
    ($('#eds-file') as JQuery<HTMLInputElement>)
        .off()
        .val('')
        .on('change', (ev) => {
            const files = ev.target.files;
            if (files.length < 1) {
                return;
            }

            const reader = new FileReader();
            reader.onload = function (data) {
                startAsync(async () => {
                    try {
                        await api.setEds(nodeID, filedialogdata2string(data.target.result));
                        await reload_card(nodeID);
                    } catch (err) {
                        noty_error(err);
                    }
                });
            }
            reader.readAsText(files[0]);
        })
        .trigger('click');
}

function updateStartAppButtonState(): number {
    let bootloaders_count = 0;
    cards.forEach((v, _i) => {
        if (v !== "InProgress" && v.hasClass('border-primary')) {
            bootloaders_count++;
        }
    });
    const btn = $('#start_app_btn') as JQuery<HTMLButtonElement>;

    if (bootloaders_count > 0) {
        btn.show();
    }  else {
        btn.hide();
    }
    return bootloaders_count;
}

function TryStartApps() {
    let count = updateStartAppButtonState();
    if (count > 0) {
        if (confirm('Будет произведена попытка запустить приложения для устройств: ' + count +
            " Продолжить?")) {
            api.startApp();
        }
    } else {
        console.log('TryStartApps() wrong call');
    }
}