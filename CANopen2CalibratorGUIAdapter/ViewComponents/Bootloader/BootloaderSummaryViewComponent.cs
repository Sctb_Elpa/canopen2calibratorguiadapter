﻿using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.Models.Bootloader;
using Microsoft.AspNetCore.Mvc;

namespace CANopen2CalibratorGUIAdapter.ViewComponents.Bootloader
{
    public class BootloaderSummaryViewComponent : ViewComponent
	{
		#region Fields

		private readonly IBackendClient Backend;

		#endregion Fields

		public BootloaderSummaryViewComponent(IBackendClient backend)
		{
			Backend = backend;
		}

		public async Task<IViewComponentResult> InvokeAsync(byte NodeId)
		{
			try
			{
				var ctrl = Backend.Devices[NodeId].BootloaderController;

				if (ctrl != null)
                {
					return View(new BootloaderSummary
					{
						AppCRC32 = await ctrl.GetApplicationIdentification(),
						AppValid = await ctrl.GetIsApplicationValid(),
						BootladerVersion = ctrl.Version
					});
				}
			} catch { }

			return Content(string.Empty);
		}
    }
}
