using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using Microsoft.AspNetCore.Mvc;

namespace CANopen2CalibratorGUIAdapter.ViewComponents.Bootloader
{
    public class DuplicationControlViewComponent : ViewComponent
	{
		#region Fields

		private readonly IBackendClient Backend;

		#endregion Fields

		public DuplicationControlViewComponent(IBackendClient backend)
		{
			Backend = backend;
		}

		public async Task<IViewComponentResult> InvokeAsync(byte NodeId)
		{
			try
			{
				var ctrl = Backend.Devices[NodeId].BootloaderController;

				if (ctrl != null && await ctrl.IsDuplicationSupported())
                {
					return View("Default", NodeId);
				}
			} catch { }

			return View("Empty");
		}
    }
}
