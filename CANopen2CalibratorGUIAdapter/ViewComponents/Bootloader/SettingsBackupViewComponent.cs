﻿using Microsoft.AspNetCore.Mvc;

namespace CANopen2CalibratorGUIAdapter.ViewComponents.Bootloader
{
    public class SettingsBackupViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke(byte NodeId)
		{
			return View(NodeId);
		}
    }
}
