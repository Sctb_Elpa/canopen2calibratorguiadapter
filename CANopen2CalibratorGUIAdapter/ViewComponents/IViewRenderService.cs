﻿using System.Threading.Tasks;

namespace CANopen2CalibratorGUIAdapter.ViewComponents
{
    // https://forums.asp.net/post/6178759.aspx
    public interface IViewRenderService
    {
        Task<string> RenderToStringAsync(string viewName, object model);
    }
}
