﻿using Microsoft.AspNetCore.Mvc;

namespace CANopen2CalibratorGUIAdapter.ViewComponents
{
	public class LssConfigureNetworkViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke(string CurrentConfig)
		{
			return View((object)CurrentConfig);
		}
	}
}