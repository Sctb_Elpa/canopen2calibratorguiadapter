﻿using CANopen2CalibratorGUIAdapter.Models.LssModels;
using Microsoft.AspNetCore.Mvc;

namespace CANopen2CalibratorGUIAdapter.ViewComponents
{
    public class LssSetupNetworkSpeedViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke(int? CurrentSpeedID, bool SpeedControlSupported = true)
		{
			return View(new LssSetupNetworkSpeedModel
			{
				SpeedControlSupported = SpeedControlSupported,
				SpeedID = CurrentSpeedID
			});
		}
	}
}
