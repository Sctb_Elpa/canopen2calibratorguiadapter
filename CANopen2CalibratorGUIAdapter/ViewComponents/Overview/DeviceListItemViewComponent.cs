﻿using CANopen2CalibratorGUIAdapter.Models;
using Microsoft.AspNetCore.Mvc;

namespace CANopen2CalibratorGUIAdapter.ViewComponents
{
	public class DeviceListItemViewComponent : ViewComponent
	{
        public IViewComponentResult Invoke(DeviceListItem item) => View(item);
    }
}
