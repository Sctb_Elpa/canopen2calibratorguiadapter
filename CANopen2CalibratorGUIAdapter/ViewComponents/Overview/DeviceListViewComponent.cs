﻿using System;
using System.Linq;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.Controllers;
using CANopen2CalibratorGUIAdapter.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CANopen2CalibratorGUIAdapter.ViewComponents
{
    public class DeviceListViewComponent : ViewComponent
    {
        #region Fields

        private readonly ILogger<OverviewController> _logger;
        private readonly IBackendClient Backend;

        #endregion Fields

        #region Constructors

        public DeviceListViewComponent(IBackendClient backend, ILogger<OverviewController> logger)
        {
            Backend = backend;
            _logger = logger;
        }

        #endregion Constructors

        #region Methods

        public IViewComponentResult Invoke()
            => View(Backend.Devices.GetAliveDeviceList().Select(Node =>
            new DeviceListItem
            {
                NodeID = Node.NodeID,
                LSSId = Node.LSSId,
                NMTState = Node.State,

                BootloaderController = Node.BootloaderController,

                DeviceKnown = Node.IsKnownDevice,
                VendorString = Node.GetVendorString(),
                ProductString = Node.GetProductString(),
                TimeFromLastHeartbeat = DateTime.Now - Node.LastHBTimestamp,
                EdsInfo = Node.Eds
            }));

        #endregion Methods
    }
}