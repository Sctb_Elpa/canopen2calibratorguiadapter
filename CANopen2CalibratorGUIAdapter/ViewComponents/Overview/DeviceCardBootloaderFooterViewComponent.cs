﻿using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using CANopen2CalibratorGUIAdapter.Models.DeviceCardModels;
using Microsoft.AspNetCore.Mvc;

namespace CANopen2CalibratorGUIAdapter.ViewComponents
{
	public class DeviceCardBootloaderFooterViewComponent : ViewComponent
	{
		public async Task<IViewComponentResult> InvokeAsync(byte NodeID, IBootloaderController BootloaderController)
		{
			return View(new DeviceCardBootloaderFooterModel
			{
				NodeID = NodeID,
				AppID = await BootloaderController.GetApplicationIdentification(),
				Version = BootloaderController.Version
			});
		}
	}
}