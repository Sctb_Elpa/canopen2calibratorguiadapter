﻿using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using CANopen2CalibratorGUIAdapter.Models.DeviceCardModels;
using Microsoft.AspNetCore.Mvc;

namespace CANopen2CalibratorGUIAdapter.ViewComponents
{
    public class DeviceCardSDOSummaryFooterViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(byte NodeId, EdsInfo EdsInfo) 
            => EdsInfo == null
                ? View("Unknown")
                : View(new DeviceCardSDOSummaryFooterModel
                {
                    NodeID = NodeId,
                    MondatoryObjectsCount = EdsInfo.MondatoryObjectsCount,
                    OptionalObjectsCount = EdsInfo.OptionalObjectsCount,
                    ManufacturerObjectsCount = EdsInfo.ManufacturerObjectsCount
                });
    }
}