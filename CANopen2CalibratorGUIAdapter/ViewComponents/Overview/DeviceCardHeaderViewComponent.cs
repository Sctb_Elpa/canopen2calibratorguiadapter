﻿using System;
using CANopen2CalibratorGUIAdapter.Models.DeviceCardModels;
using CANopen2CalibratorGUIAdapter.Utils;
using Microsoft.AspNetCore.Mvc;
using Utils.NMT;

namespace CANopen2CalibratorGUIAdapter.ViewComponents
{
    public class DeviceCardHeaderViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(string MarkClass, byte NodeID,
            NMTState NmtState, TimeSpan Heartbeat, bool AvatarReady) 
            => View(new DeviceCardHeaderModel
            {
                MarkClass = MarkClass,
                NodeID = NodeID,
                NmtStateString = NmtState.GetDescription(),
                AvatarIcon = AvatarReady ? "map-marker-alt" : "question",

                HeartBitIconColor = Heartbeat.ToHeartbeatIconColor(),
                HeartbeatToolTip = Heartbeat.ToHeartbeatTooltipText()
            });
    }
}