﻿using CANopen2CalibratorGUIAdapter.Models.DeviceCardModels;
using Microsoft.AspNetCore.Mvc;

namespace CANopen2CalibratorGUIAdapter.ViewComponents
{
	public class DeviceCardBodyViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke(byte NodeId, string MarkClass, string Vendor, string Product,
			uint Revision, uint Serial, bool EdsReload)
		{
			return View(new DeviceCardBodyModel
			{
				NodeID = NodeId,
				MarkClass = MarkClass,
				Vendor = Vendor,
				Product = Product,
				Revision = Revision,
				Serial = Serial,
				EDSButtonTooltip = (EdsReload ? "Изменить" : "Загрузить") + " EDS"
			});
		}
	}
}