﻿using Microsoft.AspNetCore.Mvc;

namespace CANopen2CalibratorGUIAdapter.ViewComponents
{
    public class DeviceListEmptyViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke() => View();
    }
}