﻿using CANopen2CalibratorGUIAdapter.Models.DeviceCardModels;
using Microsoft.AspNetCore.Mvc;

namespace CANopen2CalibratorGUIAdapter.ViewComponents
{
	public class DeviceCardTpdoSummaryFooterViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke()
		{
			return View(new DeviceCardTpdoSummaryFooterModel());
		}
	}
}