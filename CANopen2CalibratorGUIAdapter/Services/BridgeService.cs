﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using libEDSsharp;
using Microsoft.Extensions.Logging;
using Ru.Sctbelpa.CANopen2CalibratorGUIAdapter;

using Index = Utils.OD.ODIndex;
using Result = Ru.Sctbelpa.CANopen2CalibratorGUIAdapter.Result;

namespace CANopen2CalibratorGUIAdapter.Services
{
    public class BridgeService : CanAdapter.CanAdapterBase
    {
        private readonly ILogger<BridgeService> _logger;
        private readonly IBackendClient Backend;

        public BridgeService(IBackendClient backend, ILogger<BridgeService> logger)
        {
            _logger = logger;
            Backend = backend;
        }

        public override Task<Result> TestConnection(Empty request, ServerCallContext context)
            => Task.FromResult(new Result()
            {
                Error = (Backend != null)
                    ? null
                    : new AdapterError { Message = "Backend not connected" }
            });

        public override Task<ConnectedDeviceIDsList> GetConnectedNodes(Empty request, ServerCallContext context)
        {
            _logger.LogInformation($"Client {context.Peer} requested connected node list");

            var res = new ConnectedDeviceIDsList();
            res.NodeIDs.AddRange(Backend.Devices.GetAliveDeviceList().Select(d => (uint)d.NodeID));

            return Task.FromResult(res);
        }

        public override Task<CanDeviceIdentificationResult> GetNodeIdentifcation(NodeIDRequest request, ServerCallContext context)
        {
            var node = Backend.Devices[(byte)request.NodeID];
            var result = new CanDeviceIdentificationResult();

            if (node == null)
            {
                result.Error = new AdapterError
                {
                    Message = $"NodeID={request.NodeID} not connected to CANOpen network"
                };
                _logger.LogError(result.Error.Message);
            }
            else
            {
                result.Identification = new CanDeviceIdentificationResult.Types.CanDeviceIdentification
                {
                    VendorID = node.LSSId.VendorID,
                    ProductID = node.LSSId.ProductID,
                    Revision = node.LSSId.RevisionNumber,
                    Serial = node.LSSId.SerialNumber,
                };
            }

            return Task.FromResult(result);
        }

        public override Task<ObjectDictionaryResult> GetObjectDictionary(NodeIDRequest request, ServerCallContext context)
        {
            static ObjectDictionary.Types.ObjectDictionaryEntry buildEntry(ODentry entry, string IndexName)
                => new ObjectDictionary.Types.ObjectDictionaryEntry
                {
                    Index = entry.Index,
                    Subindex = entry.Subindex,

                    Name = entry.parameter_name,
                    IndexName = IndexName,

                    ValueType = entry.datatype.ToString(),
                    IsReadable = (entry.accesstype != EDSsharp.AccessType.wo) &&
                                (entry.accesstype != EDSsharp.AccessType.UNKNOWN),
                    IsWritable = (entry.accesstype != EDSsharp.AccessType.ro) &&
                                (entry.accesstype != EDSsharp.AccessType.@const) &&
                                (entry.accesstype != EDSsharp.AccessType.UNKNOWN),
                };

            var node = Backend.Devices[(byte)request.NodeID];
            var result = new ObjectDictionaryResult();

            if (!RejectIfWrongRequest(node, request.NodeID, (err) => result.Error = err))
            {
                result.Result = new ObjectDictionary();

                foreach (var index in node.Eds.Indexes())
                {
                    if (index.objecttype == ObjectType.VAR)
                    {
                        result.Result.Entries.Add(buildEntry(index, string.Empty));
                    }
                    else if (index.subobjects.Count > 0)
                    {
                        result.Result.Entries.Add(
                            index.subobjects.Values
                                .Where(subindex => subindex.Subindex > 0)
                                .Select(subindex =>
                                    buildEntry(subindex, subindex.parent.parameter_name)));
                    }
                }
            }

            return Task.FromResult(result);
        }

        public override Task<NodeInfoResult> GetDeviceInfo(NodeIDRequest request, ServerCallContext context)
        {
            var node = Backend.Devices[(byte)request.NodeID];
            var result = new NodeInfoResult();

            if (!RejectIfWrongRequest(node, request.NodeID, (err) => result.Error = err))
            {
                _logger.LogInformation($"Client {context.Peer} requested {request.NodeID} device info");
                result.Info = new NodeInfoResult.Types.NodeInfo
                {
                    Name = node.Eds.ProductName
                };
            }

            return Task.FromResult(result);
        }

        public override async Task<IndexListResult> GetTPDOMappedIndexies(NodeIDRequest request, ServerCallContext context)
        {
            var node = Backend.Devices[(byte)request.NodeID];
            var result = new IndexListResult();

            if (!RejectIfWrongRequest(node, request.NodeID, (err) => result.Error = err))
            {
                _logger.LogInformation($"Client {context.Peer} requested {request.NodeID} TPDO mapping");
                result.Result = new IndexList();

                var TPDO = node.TPDO;
                var config = await TPDO.TPDOConfig();
                result.Result.Indexies.Add(from slot in config
                                           where !slot.invalid
                                           from index in slot.Mapping
                                           select new ODIndex
                                           {
                                               Index = index.Index,
                                               Subindex = index.Subindex
                                           });
            }

            return result;
        }

        public override async Task<SDOReadResult> ReadIndexies(SDORequest request, ServerCallContext context)
        {
            var node = Backend.Devices[(byte)request.NodeID];
            var result = new SDOReadResult() { NodeID = request.NodeID };

            if (!RejectIfWrongRequest(node, request.NodeID, (err) => result.GlobalError = err))
            {
                var isRunning = node.State == global::Utils.NMT.NMTState.Operatuional;

                {
                    var indexList = string.Join(", ", request.Indexies.Select(idx => $"{ODindex2Str(idx)}"));
                    _logger.LogDebug($"Client {context.Peer} reads indexies: {indexList}");
                }

                result.Items = new SDOReadResult.Types.SDOReadItems();
                foreach (var r_index in request.Indexies)
                {
                    var odindex = node.Eds.GetIndex((ushort)r_index.Index, (byte)r_index.Subindex);
                    var v = new SDOReadResult.Types.SDOReadItem { Index = r_index };

                    if (isRunning && odindex.PDOMapping && node.TPDO.IsMapedAndReady(odindex))
                    {
                        v.Value = odindex.actualvalue;
                        _logger.LogDebug($"[{ODindex2Str(r_index)}] <= PDO: {v.Value}");
                    }
                    else
                    {
                        try
                        {
                            var read_result = await node.SDOClient.ReadAsStringAsync(odindex);

                            v.Value = read_result;
                        }
                        catch (Exception ex)
                        {
                            v.Error = new AdapterError { Message = ex.Message };
                            _logger.LogError($"Failed to read {ODindex2Str(r_index)} ({ex.Message})");
                        }
                    }

                    result.Items.Items.Add(v);
                }
            }

            return result;
        }

        public override async Task<SDOWriteResult> WriteIndexies(SDOWriteRequest request, ServerCallContext context)
        {
            var node = Backend.Devices[(byte)request.NodeID];
            var result = new SDOWriteResult() { NodeID = request.NodeID };

            if (!RejectIfWrongRequest(node, request.NodeID, (err) => result.GlobalError = err))
            {
                {
                    var req_list = string.Join(", ", request.Requests.Select(
                        r => $"{ODindex2Str(r.Index)}={r.Value}"));
                    _logger.LogInformation($"Client {context.Peer} write indexies: {req_list}");
                }

                result.Results = new SDOWriteResult.Types.SDOWriteResults();
                foreach (var r in request.Requests)
                {
                    var v = new SDOWriteResult.Types.SDOWriteResultItem { Index = r.Index };
                    try
                    {
                        await node.SDOClient.WriteStringAsync(
                            node.Eds.GetIndex((ushort)r.Index.Index, (byte)r.Index.Subindex), r.Value);
                    }
                    catch (Exception ex)
                    {
                        v.Error = new AdapterError { Message = ex.Message };
                        _logger.LogError($"Failed to write {ODindex2Str(r.Index)}={r.Value} ({ex.Message})");
                    }
                    result.Results.Items.Add(v);
                }
            }

            return result;
        }

        private bool RejectIfWrongRequest(DeviceState node, uint rq_NodeId, Action<AdapterError> writeError)
        {
            if (node == null)
            {
                var Error = new AdapterError
                {
                    Message = $"NodeID={rq_NodeId} not connected to CANOpen network"
                };
                _logger.LogError(Error.Message);
                writeError(Error);
                return true;
            }
            else if (!node.IsKnownDevice)
            {
                var Error = new AdapterError
                {
                    Message = $"EDS not loaded for NodeID={rq_NodeId}"
                };
                _logger.LogError(Error.Message);
                writeError(Error);
                return true;
            }
            return false;
        }

        private string ODindex2Str(ODIndex index) => $"{index.Index:X}sub{index.Subindex}";
    }
}