﻿using CommandLine;
using Common;

namespace CANopen2CalibratorGUIAdapter
{
    public class Options
    {
        [Option("can-driver", Required = true, HelpText = "CanOpen Driver to connect to")]
        public string CanDriver { get; set; }

        [Option("web-port", Required = false, Default = Defaults.DefaultWebPort, HelpText = "Web Server port")]
        public int WebPort { get; set; }

        [Option("grpc-port", Required = false, Default = Defaults.DefaultgRPCPort, HelpText = "gRPC port")]
        public int GRpcPort { get; set; }
    }
}