﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using CANopen2CalibratorGUIAdapter.CANOpenClient.SDO;
using CANopen2CalibratorGUIAdapter.Models;
using Grpc.Core;
using GrpcService;
using HalperExtantions;
using Microsoft.Extensions.Logging;
using Ru.Sctbelpa.CanopenDriver;
using Utils.LSS;
using Utils.NMT;

using Result = Utils.Result;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient
{
    internal class BackendClient : IBackendClient, IDisposable
    {
        #region Fields

        public const ushort NodePresentCheckIndex = 0x1000;

        private readonly string CanDriverString;
        private readonly Google.Protobuf.WellKnownTypes.Empty Empty = new Google.Protobuf.WellKnownTypes.Empty();
        private Dictionary<int, string> _CanBusList = null;
        private Channel Channel = null;

        private readonly SDOClient SDoClient = new SDOClient();
        private readonly EmergencyLog EmergencyLog = new EmergencyLog();

        #endregion Fields

        #region Constructors

        public BackendClient(Options options)
        {
            CanDriverString = options.CanDriver;
        }

        #endregion Constructors

        #region Properties

        public bool Connected => Connect().IsConnected(Empty).Connected;

        public int? ConnectionID
        {
            get
            {
                var res = Connect().IsConnected(Empty);
                if (res.Connected)
                {
                    var l = FindConnectedBus(res);
                    while (!l.Any())
                    {
                        var task = Task.Run(() => TryEnumerateBussesAsync());
                        task.Wait();

                        l = FindConnectedBus(res);
                    }
                    return l.First();
                }
                else
                {
                    return 0;
                }
            }
        }

        public double ConnectionTimeout_s { get; set; } = 5.0;

        public DeviceDB Devices { get; private set; } = new DeviceDB();

        public AbstractEmergencyLog GetEmergencyLog() => EmergencyLog;

        public int? SpeedID => (int)Connect().IsConnected(Empty).Speed;

        public bool SpeedSetupSupported =>
                            (Connect().IsConnected(Empty).Connection.Features & ConnectionFetures.HasBaudRateSelect)
                == ConnectionFetures.HasBaudRateSelect;

        #endregion Properties

        #region Methods

        private void LogTime(string method, long total, long self)
        {
            ILogger _logger = Log.CreateLogger<BackendClient>();
            var grpc_took = total - self;
            _logger.LogTrace($"{method}() took {self} (self) + {grpc_took} (grpc) ms");
        }

        public async Task<global::Utils.Result<byte>> AssignNodeID(UniqueLSSId lss_id, byte? nodeID)
        {
            var stopwatch = Stopwatch.StartNew();
            var connection = Connect();

            var r1 = await connection.LSSSwitchStateAsync(new LSSState
            {
                TargetNodeID = lss_id.ToPB_LSSID(),
                State = LSSState.Types.StateCode.Configuration
            });
            if (r1.Error != null)
            {
                return Result.Fail<byte>(r1.Error.Message);
            }

            var r2 = await connection.LSSConfigureNodeIDAsync(new NodeIDConfig
            {
                NodeID = nodeID.GetValueOrDefault()
            });
            if (r2.Error != null)
            {
                return Result.Fail<byte>(r2.Error.Message);
            }

            var r3 = await connection.LSSSaveSettingsAsync(Empty);
            if (r3.Error != null)
            {
                return Result.Fail<byte>(r3.Error.Message);
            }

            var r4 = await connection.LSSSwitchStateAsync(new LSSState
            {
                TargetNodeID = lss_id.ToPB_LSSID(),
                State = LSSState.Types.StateCode.Waiting
            });
            if (r4.Error != null)
            {
                return Result.Fail<byte>(r4.Error.Message);
            }

            {
                ILogger _logger = Log.CreateLogger<BackendClient>();
                _logger.LogDebug($"AssignNodeID() took: {r1.ExecutuinTimeMs} + {r2.ExecutuinTimeMs} + {r3.ExecutuinTimeMs} + {r4.ExecutuinTimeMs} ({stopwatch.ElapsedMilliseconds} with grpc)");
            }

            return Result.Ok((byte)r2.NodeID);
        }

        public async Task<Result> ChangeNetworkSpeed(byte speedGrade)
        {
            var connctionInfo = await Connect().IsConnectedAsync(Empty);
            if (connctionInfo.Error != null)
            {
                return Result.Fail(connctionInfo.Error.Message);
            }

            var result = await DisconnectAsync();
            if (result.Failure)
            {
                return result;
            }

            return await ConnectAsync(connctionInfo.Connection.ID, speedGrade);
        }

        private Connection.ConnectionClient Connect()
        {
            if (Channel == null || (Channel.State != ChannelState.Ready && Channel.State != ChannelState.Idle))
            {
                var channel = new Channel(CanDriverString, ChannelCredentials.Insecure);

                var connectAwaiter = Task.Run(
                    () => channel.TryWaitForStateChangedAsync(ChannelState.Connecting,
                        DateTime.UtcNow.AddSeconds(ConnectionTimeout_s))
                    );
                connectAwaiter.Wait();

                if (!connectAwaiter.Result)
                {
                    throw new TimeoutException($"Failed to connetct to driver backend: {CanDriverString}");
                }

                Channel = channel; // connected

                SDoClient.Chanel = channel;

                Devices.Sdoclient = SDoClient;
                Task.Run(() => RunHeartbeatMonitor(Devices));
                Task.Run(() => RunEmergencyMonitor(EmergencyLog));
            }
            return new Connection.ConnectionClient(Channel);
        }

        public async Task<Result> ConnectAsync(int canConnection, int speed)
        {
            var status = await Connect().ConnectBusAsync(new ConnectionRequest
            {
                Id = canConnection,
                Speed = (CanBusSpeed)speed
            });
            return status.Error != null
                ? Result.Fail(status.Error.Message)
                : Result.Ok();
        }

        public async Task<Result> DisconnectAsync()
        {
            var res = await Connect().DisconnectBusAsync(Empty);
            return res.Error != null
                ? Result.Fail(res.Error.Message)
                : Result.Ok();
        }

        public void Dispose()
        {
            // unsubscribe emergency
            EmergencyLog.Dispose();

            // destroy devices databas
            if (Devices != null)
            {
                Devices.Dispose();
                Devices = null;
            }
        }

        public IDictionary<int, string> GetCanBusList()
        {
            if (_CanBusList == null || !_CanBusList.Any())
            {
                var task = Task.Run(() => TryEnumerateBussesAsync());
                task.Wait();
            }
            return _CanBusList;
        }

        public async Task<IDictionary<int, string>> GetCanBusListAsync()
        {
            if (_CanBusList == null || !_CanBusList.Any())
            {
                await TryEnumerateBussesAsync();
            }
            return _CanBusList;
        }

        public async Task<global::Utils.Result<SYNCConfig>> GetSyncConfig()
        {
            var res = await Connect().GetSyncConfigAsync(Empty);
            return res.Error != null
                ? Result.Fail<SYNCConfig>(res.Error.Message)
                : Result.Ok(new SYNCConfig
                {
                    Enabled = res.Value.Enabled,
                    CobID = res.Value.CobID,
                    SyncPeriod = (ushort)res.Value.SyncPeriod,
                });
        }

        public async Task<bool> IsNodeIDAvalaible(byte nodeID)
        {
            try
            {
                var r = await SDoClient.ReadAsync<uint>(nodeID, NodePresentCheckIndex);
                return false;
            }
            catch (BackendCanError)
            {
                return true;
            }
        }

        public async Task<Result> LSSConfigureNodeBitrate(byte speedGrade)
        {
            var stopwatch = Stopwatch.StartNew();
            if (!Utils.CanSpeedMap.SpeedMap.ContainsKey(speedGrade))
            {
                return Result.Fail($"Speed grade {speedGrade} is unknown");
            }

            var res = await Connect().LSSConfigureBusSpeedAsync(new BusSpeedConfig
            {
                Speed = (CanBusSpeed)speedGrade
            });

            LogTime(System.Reflection.MethodBase.GetCurrentMethod().Name, stopwatch.ElapsedMilliseconds, res.ExecutuinTimeMs);

            return res.Error != null
                ? Result.Fail(res.Error.Message)
                : Result.Ok();
        }

        public async Task<Result> LSSSApplyBitrate()
        {
            var stopwatch = Stopwatch.StartNew();

            var res = await Connect().LSSApplyBusSpeedAsync(Empty);

            LogTime(System.Reflection.MethodBase.GetCurrentMethod().Name, stopwatch.ElapsedMilliseconds, res.ExecutuinTimeMs);

            return res.Error != null
                ? Result.Fail(res.Error.Message)
                : Result.Ok();
        }

        public async Task<Result> LSSStoreConfiguration()
        {
            var stopwatch = Stopwatch.StartNew();

            var res = await Connect().LSSSaveSettingsAsync(Empty);

            LogTime(System.Reflection.MethodBase.GetCurrentMethod().Name, stopwatch.ElapsedMilliseconds, res.ExecutuinTimeMs);

            return res.Error != null
                ? Result.Fail(res.Error.Message)
                : Result.Ok();
        }

        public async Task<Result> LSSSwitchStateGlobal(bool ConfigurationState = false)
        {
            var stopwatch = Stopwatch.StartNew();

            var res = await Connect().LSSSwitchStateAsync(new LSSState
            {
                State = ConfigurationState
                    ? LSSState.Types.StateCode.Configuration
                    : LSSState.Types.StateCode.Waiting
            });

            LogTime(System.Reflection.MethodBase.GetCurrentMethod().Name, stopwatch.ElapsedMilliseconds, res.ExecutuinTimeMs);

            return res.Error != null
                ? Result.Fail(res.Error.Message)
                : Result.Ok();
        }

        public async Task<Result> NMTCommand(byte nodeID, NMTCommandCode nMTCommandCode)
        {
            var stopwatch = Stopwatch.StartNew();

            var status = await Connect().NMTSendCommandAsync(new NMTCommand
            {
                TargetNodeID = nodeID,
                Command = nMTCommandCode.ToNMTCommandCode()
            });

            LogTime(System.Reflection.MethodBase.GetCurrentMethod().Name, stopwatch.ElapsedMilliseconds, status.ExecutuinTimeMs);

            return status.Error != null
                ? Result.Fail(status.Error.Message)
                : Result.Ok();
        }

        public async Task<Result> SetSyncConfig(SYNCConfig config)
        {
            var stopwatch = Stopwatch.StartNew();

            var res = await Connect().SetSyncConfigAsync(new SyncConfig
            {
                CobID = config.CobID,
                SyncPeriod = config.SyncPeriod,
                Enabled = config.Enabled
            });

            LogTime(System.Reflection.MethodBase.GetCurrentMethod().Name, stopwatch.ElapsedMilliseconds, res.ExecutuinTimeMs);

            return res.Error != null
                ? Result.Fail(res.Error.Message)
                : Result.Ok();
        }

        public async Task StartFastScan(UniqueLSSId initialLSSid, IFastScanReportListener listener)
        {
            var connection = Connect();
            {
                var syncStatus = await connection.GetSyncConfigAsync(Empty);
                if (syncStatus.Error != null)
                {
                    throw new Exception(syncStatus.Error.Message);
                }

                if (syncStatus.Value.Enabled)
                {
                    throw new Exception("Service conflict: Unable to fast scan while SYNC is active." +
                        "Please, disable <a href=\"/SYNC\">SYNC</a> first.");
                }
            }

            using (var stream = connection.LSSStartFastScan(initialLSSid.ToPB_LSSID()))
            {
                await foreach (var result in stream.ResponseStream.ReadAllAsync())
                {
                    try
                    {
                        switch (result.StateCase)
                        {
                            case ScanProgress.StateOneofCase.Progress:
                                await listener.OnScanProgress(UniqueLSSId.FieldName((byte)result.Progress.Field),
                                    result.Progress.BitN);
                                break;

                            case ScanProgress.StateOneofCase.LssResult:
                                await listener.OnLssFinished(result.LssResult.ToUniqLSSID());
                                break;

                            case ScanProgress.StateOneofCase.ErrorMessage:
                                await listener.OnErrorMessage(result.ErrorMessage.Message);
                                break;

                            case ScanProgress.StateOneofCase.BusyReject:
                                throw new BackendCanError("Драйвер занят другой операцией FastScan");
                            default:
                                await listener.OnErrorMessage("Драйвер отправил пустое сообщение");
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        var log = Log.CreateLogger<BackendClient>();
                        log.LogError("Frontend error: " + ex.Message);
                    }
                }
            }
        }

        public async Task SubscribeTPO(uint CobId, Func<ushort, byte[], Task> callback, CancellationToken CancellationToken)
        {
            using var stream = Connect().PDOSubscribe(new PDOSubscribeInfo { CobId = CobId });
            try
            {
                await foreach (var item in stream.ResponseStream.ReadAllAsync(CancellationToken))
                {
                    await callback.Invoke((ushort)item.CobId, item.Data.ToArray());
                }
            } 
            catch (RpcException) { /* pass */ }
        }

        private IEnumerable<int> FindConnectedBus(ConnectionStatus res) => from t in GetCanBusList()
                                                                           where t.Value == res.Connection.Uri
                                                                           select t.Key;

        private async Task RunHeartbeatMonitor(IHeartbeatMonitor monitor)
        {
            using var stream = Connect().SubscribeHeartbeat(Empty);
            await foreach (var item in stream.ResponseStream.ReadAllAsync(monitor.CancellationToken))
            {
                await monitor.HBRessived((byte)item.NodeID,
                    (NMTState)item.State, this, item.DateTimeStamp.ToDateTime().ToLocalTime());
            }
        }

        private async Task RunEmergencyMonitor(AbstractEmergencyLog monitor)
        {
            using var stream = Connect().SubscribeEmergency(Empty);
            await foreach (var item in stream.ResponseStream.ReadAllAsync(monitor.CancellationToken))
            {
                monitor.EMCYRessived((byte)item.NodeId, item.Timestamp.ToDateTime().ToLocalTime(),
                    item.ErrorRegister, item.EmcyErrorCode, item.ManufacturerSpec.ToArray());
            }
        }

        private async Task TryEnumerateBussesAsync()
        {
            using var stream = Connect().GetConnectionList(Empty);
            var res = stream.ResponseStream.ReadAllAsync();

            _CanBusList = new Dictionary<int, string>();
            await foreach (var c in res)
            {
                _CanBusList.Add(c.ID, c.Uri);
            }
        }
        #endregion Methods
    }
}