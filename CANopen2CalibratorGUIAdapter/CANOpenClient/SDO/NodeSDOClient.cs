﻿using System.Threading.Tasks;
using Utils;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.SDO
{
    public class NodeSDOClient : AbstractNodeSDOClient
    {
        private readonly AbstractSDOClient Client;
        private readonly byte NodeId;

        public NodeSDOClient(AbstractSDOClient client, byte nodeID)
        {
            Client = client;
            NodeId = nodeID;
        }

        public override Task<T> ReadAsync<T>(ushort index, byte subindex)
            => Client.ReadAsync<T>(NodeId, index, subindex);

        public override Task<byte[]> ReadBytesAsync(ushort index, byte subindex)
            => Client.ReadBytesAsync(NodeId, index, subindex);

        public override Task WriteAsync<T>(ushort index, byte subindex, T value)
            => Client.WriteAsync<T>(NodeId, index, subindex, value);

        public override Task WriteBytesAsync(ushort index, byte subindex, byte[] data)
            => Client.WriteBytesAsync(NodeId, index, subindex, data);
    }
}
