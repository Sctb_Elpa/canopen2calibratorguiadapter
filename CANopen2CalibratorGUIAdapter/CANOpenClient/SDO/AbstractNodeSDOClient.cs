﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using libEDSsharp;
using Utils.OD;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.SDO
{
    public abstract class AbstractNodeSDOClient
    {
        #region Methods

        public abstract Task<T> ReadAsync<T>(ushort index, byte subindex) where T : struct;

        public abstract Task WriteAsync<T>(ushort index, byte subindex, T value) where T : struct;

        public abstract Task<byte[]> ReadBytesAsync(ushort index, byte subindex);

        public abstract Task WriteBytesAsync(ushort index, byte subindex, byte[] data);

        public virtual Task<T> ReadAsync<T>(ODIndex index) where T : struct
            => ReadAsync<T>(index.Index, index.Subindex);

        public virtual Task<byte[]> ReadBytesAsync(ODIndex index)
            => ReadBytesAsync(index.Index, index.Subindex);

        public virtual Task WriteAsync<T>(ODIndex index, T v) where T : struct
            => WriteAsync(index.Index, index.Subindex, v);

        public virtual Task WriteBytesAsync(ODIndex settingsBackupIndex, byte[] data)
            => WriteBytesAsync(settingsBackupIndex.Index, settingsBackupIndex.Subindex, data);

        public async Task<string> ReadAsStringAsync(ODentry oDentry) => oDentry.datatype switch
        {
            DataType.BOOLEAN => (await ReadAsync<bool>(oDentry.Index, (byte)oDentry.Subindex)).ToString(CultureInfo.InvariantCulture),
            DataType.INTEGER8 => (await ReadAsync<sbyte>(oDentry.Index, (byte)oDentry.Subindex)).ToString(),
            DataType.INTEGER16 => (await ReadAsync<short>(oDentry.Index, (byte)oDentry.Subindex)).ToString(),
            DataType.INTEGER32 => (await ReadAsync<int>(oDentry.Index, (byte)oDentry.Subindex)).ToString(),
            DataType.UNSIGNED8 => (await ReadAsync<byte>(oDentry.Index, (byte)oDentry.Subindex)).ToString(),
            DataType.UNSIGNED16 => (await ReadAsync<ushort>(oDentry.Index, (byte)oDentry.Subindex)).ToString(),
            DataType.UNSIGNED32 => (await ReadAsync<uint>(oDentry.Index, (byte)oDentry.Subindex)).ToString(),
            DataType.REAL32 => (await ReadAsync<float>(oDentry.Index, (byte)oDentry.Subindex)).ToString(CultureInfo.InvariantCulture),
            DataType.VISIBLE_STRING => Encoding.ASCII.GetString(await ReadBytesAsync(oDentry.Index, (byte)oDentry.Subindex)).Trim('\0'),
            DataType.OCTET_STRING => (await ReadBytesAsync(oDentry.Index, (byte)oDentry.Subindex)).ToDumpString(),
            DataType.UNICODE_STRING => Encoding.UTF8.GetString(await ReadBytesAsync(oDentry.Index, (byte)oDentry.Subindex)).Trim('\0'),
            DataType.REAL64 => (await ReadAsync<double>(oDentry.Index, (byte)oDentry.Subindex)).ToString(CultureInfo.InvariantCulture),
            DataType.INTEGER64 => (await ReadAsync<long>(oDentry.Index, (byte)oDentry.Subindex)).ToString(),
            DataType.UNSIGNED64 => (await ReadAsync<ulong>(oDentry.Index, (byte)oDentry.Subindex)).ToString(),
            DataType.PDO_COMMUNICATION_PARAMETER => (await ReadAsync<uint>(oDentry.Index, (byte)oDentry.Subindex)).ToString(),
            DataType.PDO_MAPPING => (await ReadAsync<uint>(oDentry.Index, (byte)oDentry.Subindex)).ToString(),

            _ => $"{oDentry.datatype} not supported"
        };

        public async Task WriteStringAsync(ODentry oDentry, string value)
        {
            static byte[] adjustToIndexDataSize(byte[] input, int target_length)
            {
                if (input.Length <= target_length)
                {
                    var res = new byte[target_length];
                    Array.Copy(input, res, input.Length);
                    return res;
                }
                else
                {
                    return input.Take(target_length).ToArray();
                }
            }

            var Index = new ODIndex { Index = oDentry.Index, Subindex = (byte)oDentry.Subindex };

            switch (oDentry.datatype)
            {
                case DataType.BOOLEAN: await WriteAsync(Index, Convert.ToBoolean(value)); break;
                case DataType.INTEGER8: await WriteAsync(Index, Convert.ToSByte(value)); break;
                case DataType.INTEGER16: await WriteAsync(Index, Convert.ToInt16(value)); break;
                case DataType.INTEGER32: await WriteAsync(Index, Convert.ToInt32(value)); break;
                case DataType.INTEGER64: await WriteAsync(Index, Convert.ToInt64(value)); break;
                case DataType.UNSIGNED8: await WriteAsync(Index, Convert.ToByte(value)); break;
                case DataType.UNSIGNED16: await WriteAsync(Index, Convert.ToUInt16(value)); break;
                case DataType.UNSIGNED32: await WriteAsync(Index, Convert.ToUInt32(value)); break;
                case DataType.VISIBLE_STRING: 
                    await WriteBytesAsync(Index, adjustToIndexDataSize(Encoding.ASCII.GetBytes(value),
                        Encoding.ASCII.GetBytes(oDentry.defaultvalue).Length)); 
                    break;
                case DataType.OCTET_STRING: 
                    await WriteBytesAsync(Index, adjustToIndexDataSize(value.ParceDumpString(),
                        oDentry.defaultvalue.Length / 2)); // =00000000...0000 - bytes
                    break;
                case DataType.UNICODE_STRING: 
                    await WriteBytesAsync(Index, adjustToIndexDataSize(Encoding.UTF8.GetBytes(value),
                        Encoding.UTF8.GetBytes(oDentry.defaultvalue).Length));
                    break;
                case DataType.REAL32:
                    await WriteAsync(Index, float.Parse(value, CultureInfo.InvariantCulture)); 
                    break;
                case DataType.REAL64:
                    await WriteAsync(Index, double.Parse(value, CultureInfo.InvariantCulture));
                    break;
                case DataType.UNSIGNED64: await WriteAsync(Index, Convert.ToUInt64(value)); break;
                case DataType.PDO_COMMUNICATION_PARAMETER: await WriteAsync(Index, Convert.ToUInt32(value)); break;
                case DataType.PDO_MAPPING: await WriteAsync(Index, Convert.ToUInt32(value)); break;
                default: throw new NotSupportedException($"Index data type {oDentry.datatype} not supported");
            }
        }

        #endregion Methods
    }
}
