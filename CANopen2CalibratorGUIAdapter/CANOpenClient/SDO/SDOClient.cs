﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Ru.Sctbelpa.CanopenDriver;
using Utils;

using enValueType = Ru.Sctbelpa.CanopenDriver.SDOReadRequest.Types.enValueType;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.SDO
{
    public class SDOClient : AbstractSDOClient
    {
        #region Fields

        public static readonly Dictionary<Type, enValueType> TypeConversionTable
            = new Dictionary<Type, enValueType>
            {
                { typeof(byte), enValueType.ValueU8 },
                { typeof(ushort), enValueType.ValueU16 },
                { typeof(uint), enValueType.ValueU32 },
                { typeof(UInt64), enValueType.ValueU64 },

                { typeof(sbyte), enValueType.ValueS8 },
                { typeof(short), enValueType.ValueS16 },
                { typeof(int), enValueType.ValueS32 },
                { typeof(Int64), enValueType.ValueS64 },

                { typeof(bool), enValueType.ValueBool },
                { typeof(float), enValueType.ValueFloat },
                { typeof(double), enValueType.ValueDouble },
            };

        #endregion Fields

        #region Properties

        public Channel Chanel {  get; set; }

        #endregion Properties

        #region Methods

        private void LogTime(string method, long total, long self)
        {
            ILogger _logger = Log.CreateLogger<SDOClient>();
            var grpc_took = total - self;
            _logger.LogDebug($"{method}() took {self} (self) + {grpc_took} (grpc) ms");
        }

        public override async Task<T> ReadAsync<T>(byte nodeID, ushort index, byte subindex = 0) where T : struct
        {
            var stopwatch = Stopwatch.StartNew();

            var client = new Connection.ConnectionClient(Chanel);

            var result = await client.SDOReadIndexAsync(new SDOReadRequest
            {
                Index = new SDOIndex { Index = index, Subindex = subindex },
                TargetNodeID = nodeID,
                ValueType = TypeConversionTable[typeof(T)]
            });

            if (result == null)
            {
                throw new BackendCanError("Driver returns null result");
            }

            LogTime(System.Reflection.MethodBase.GetCurrentMethod().Name, stopwatch.ElapsedMilliseconds, result.ExecutuinTimeMs);

            switch (result.ResultCase)
            {
                case SDOReadResult.ResultOneofCase.Value:
                    return (result.Value.ValueCase) switch
                    {
                        SDOIndexValue.ValueOneofCase.ValueU8 => To<T>(result.Value.ValueU8),
                        SDOIndexValue.ValueOneofCase.ValueU16 => To<T>(result.Value.ValueU16),
                        SDOIndexValue.ValueOneofCase.ValueU32 => To<T>(result.Value.ValueU32),
                        SDOIndexValue.ValueOneofCase.ValueU64 => To<T>(result.Value.ValueU64),
                        SDOIndexValue.ValueOneofCase.ValueS8 => To<T>(result.Value.ValueS8),
                        SDOIndexValue.ValueOneofCase.ValueS16 => To<T>(result.Value.ValueS16),
                        SDOIndexValue.ValueOneofCase.ValueS32 => To<T>(result.Value.ValueS32),
                        SDOIndexValue.ValueOneofCase.ValueS64 => To<T>(result.Value.ValueS64),
                        SDOIndexValue.ValueOneofCase.ValueBool => To<T>(result.Value.ValueBool),
                        SDOIndexValue.ValueOneofCase.ValueFloat => To<T>(result.Value.ValueFloat),
                        SDOIndexValue.ValueOneofCase.ValueDouble => To<T>(result.Value.ValueDouble),
                        _ => throw new BackendCanError($"Driver returns unexpected value type {result.Value.ValueCase}"),
                    };

                case SDOReadResult.ResultOneofCase.Error:
                    throw new BackendCanError(result.Error.Message);
                default:
                    throw new BackendCanError("Driver returns empty result");
            }
        }

        public override async Task<byte[]> ReadBytesAsync(byte nodeID, ushort index, byte subindex = 0)
        {
            var stopwatch = Stopwatch.StartNew();

            var client = new Connection.ConnectionClient(Chanel);

            var result = await client.SDOReadIndexAsync(new SDOReadRequest
            {
                Index = new SDOIndex { Index = index, Subindex = subindex },
                TargetNodeID = nodeID,
                ValueType = enValueType.Bytes
            });

            if (result == null)
            {
                throw new BackendCanError("Driver returns null result");
            }

            LogTime(System.Reflection.MethodBase.GetCurrentMethod().Name, stopwatch.ElapsedMilliseconds, result.ExecutuinTimeMs);

            switch (result.ResultCase)
            {
                case SDOReadResult.ResultOneofCase.Value:
                    return (result.Value.ValueCase) switch
                    {
                        SDOIndexValue.ValueOneofCase.Bytes => result.Value.Bytes.ToByteArray(),
                        _ => throw new BackendCanError($"Driver returns unexpected value type {result.Value.ValueCase}"),
                    };

                case SDOReadResult.ResultOneofCase.Error:
                    throw new BackendCanError(result.Error.Message);
                default:
                    throw new BackendCanError("Driver returns empty result");
            }
        }

        public override async Task WriteAsync<T>(byte nodeID, ushort index, byte subindex, T value)
        {
            var stopwatch = Stopwatch.StartNew();

            var client = new Connection.ConnectionClient(Chanel);

            var Request = new SDOWriteRequest
            {
                DestIndex = new SDOIndex { Index = index, Subindex = subindex },
                TargetNodeID = nodeID,
                Value = new SDOIndexValue()
            };

            switch (value)
            {
                case byte b:
                    Request.Value.ValueU8 = b;
                    break;

                case ushort us:
                    Request.Value.ValueU16 = us;
                    break;

                case uint ui:
                    Request.Value.ValueU32 = ui;
                    break;

                case UInt64 ul:
                    Request.Value.ValueU64 = ul;
                    break;

                case sbyte sb:
                    Request.Value.ValueS8 = sb;
                    break;

                case short ss:
                    Request.Value.ValueS16 = ss;
                    break;

                case int i:
                    Request.Value.ValueS32 = i;
                    break;

                case long l:
                    Request.Value.ValueS64 = l;
                    break;

                case float f:
                    Request.Value.ValueFloat = f;
                    break;

                case double d:
                    Request.Value.ValueDouble = d;
                    break;

                case bool b:
                    Request.Value.ValueBool = b;
                    break;

                default:
                    throw new BackendCanError($"Unsupported write data type {typeof(T)}");
            }

            var result = await client.SDOWriteIndexAsync(Request);

            if (result == null)
            {
                throw new BackendCanError("Driver returns null result");
            }

            LogTime(System.Reflection.MethodBase.GetCurrentMethod().Name, stopwatch.ElapsedMilliseconds, result.ExecutuinTimeMs);

            if (result.Error != null)
            {
                throw new BackendCanError(result.Error.Message);
            }
        }

        public override async Task WriteBytesAsync(byte nodeID, ushort index, byte subindex, byte[] data)
        {
            var stopwatch = Stopwatch.StartNew();

            var client = new Connection.ConnectionClient(Chanel);

            var result = await client.SDOWriteIndexAsync(new SDOWriteRequest
            {
                DestIndex = new SDOIndex { Index = index, Subindex = subindex },
                TargetNodeID = nodeID,
                Value = new SDOIndexValue
                {
                    Bytes = Google.Protobuf.ByteString.CopyFrom(data)
                }
            });

            if (result == null)
            {
                throw new BackendCanError("Driver returns null result");
            }

            LogTime(System.Reflection.MethodBase.GetCurrentMethod().Name, stopwatch.ElapsedMilliseconds, result.ExecutuinTimeMs);

            if (result.Error != null)
            {
                throw new BackendCanError(result.Error.Message);
            }
        }

        private T To<T>(object v) => (T)Convert.ChangeType(v, typeof(T));

        #endregion Methods
    }
}