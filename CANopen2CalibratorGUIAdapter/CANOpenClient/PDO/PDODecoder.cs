﻿using System;
using System.Collections.Generic;
using System.Globalization;
using libEDSsharp;
using Utils.OD;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.PDO
{
    public static class PDODecoder
    {
        public static IEnumerable<KeyValuePair<ODIndex, object>> DecodeData(this PDOSlot slot, byte[] pdoData)
        {
            int offset = 0;
            foreach (var odentry in slot.Mapping)
            {
                yield return new KeyValuePair<ODIndex, object>(
                    key: new ODIndex
                    {
                        Index = odentry.Index,
                        Subindex = (byte)odentry.Subindex
                    },
                    value: DecodeValue(pdoData, offset, odentry.datatype)
                );
                offset += odentry.Sizeofdatatype();
            }
        }

        private static object DecodeValue(byte[] pdoData, int offset, DataType datatype)
            => (offset % 8 == 0)
                ? fastDecodeValue(pdoData, offset / 8, datatype)
                : presisionDecodeValue(pdoData, offset, datatype);

        private static object presisionDecodeValue(byte[] pdoData, int offset, DataType datatype)
            => "presisionDecodeValue not implemented yet!";

        private static object fastDecodeValue(byte[] pdoData, int byteOffset, DataType datatype)
            => datatype switch
            {
                DataType.UNKNOWN => "UNKNOWN",
                DataType.BOOLEAN => ((pdoData[byteOffset] & 0x80) != 0),
                DataType.INTEGER8 => (sbyte)pdoData[byteOffset],
                DataType.INTEGER16 => BitConverter.ToInt16(pdoData, byteOffset),
                DataType.INTEGER32 => BitConverter.ToInt32(pdoData, byteOffset),
                DataType.UNSIGNED8 => pdoData[byteOffset],
                DataType.UNSIGNED16 => BitConverter.ToUInt16(pdoData, byteOffset).ToString(),
                DataType.UNSIGNED32 => BitConverter.ToUInt32(pdoData, byteOffset).ToString(),
                DataType.REAL32 => BitConverter.ToSingle(pdoData, byteOffset).ToString(CultureInfo.InvariantCulture),
                DataType.REAL64 => BitConverter.ToDouble(pdoData, byteOffset).ToString(CultureInfo.InvariantCulture),
                DataType.INTEGER64 => BitConverter.ToInt64(pdoData, byteOffset).ToString(),
                DataType.UNSIGNED64 => BitConverter.ToUInt64(pdoData, byteOffset).ToString(),
                _ => $"{datatype} decoding not implemented",
            };
    }
}