﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using CANopen2CalibratorGUIAdapter.CANOpenClient.SDO;
using libEDSsharp;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.PDO
{
    public class PDOAccessor
    {
        private readonly AbstractNodeSDOClient SDOClient;
        private readonly EdsInfo EdsInfo;
        private readonly SemaphoreSlim mySemaphoreSlim;

        public List<PDOSlot> Pdoslots { get; set; } = new List<PDOSlot>();

        public PDOAccessor(AbstractNodeSDOClient SDOClient, EdsInfo edsinfo)
        {
            this.SDOClient = SDOClient;
            EdsInfo = edsinfo;
            mySemaphoreSlim = new SemaphoreSlim(1);
        }

        public async Task LoadConfig()
        {
            // https://stackoverflow.com/a/18257065
            await mySemaphoreSlim.WaitAsync();
            try
            {
                foreach (var slot in Pdoslots)
                {
                    var index = EdsInfo.GetIndex(slot.ConfigurationIndex);

                    slot.COB = await SDOClient.ReadAsync<uint>(index.Index, 1);

                    if (index.Containssubindex(2))
                    {
                        slot.transmissiontype = await SDOClient.ReadAsync<byte>(index.Index, 2);
                    }

                    if (index.Containssubindex(3))
                    {
                        slot.inhibit = await SDOClient.ReadAsync<ushort>(index.Index, 3);
                    }

                    if (index.Containssubindex(5))
                    {
                        slot.eventtimer = await SDOClient.ReadAsync<ushort>(index.Index, 5);
                    }

                    if (index.Containssubindex(6))
                    {
                        slot.syncstart = await SDOClient.ReadAsync<byte>(index.Index, 6);
                    }
                }
            }
            finally
            {
                mySemaphoreSlim.Release();
            }
        }

        public async Task LoadMapping(int slotNum)
        {
            var slot = Pdoslots[slotNum];
            var mapping = EdsInfo.Getobject(slot.MappingIndex);
            var append_to = slot.Mapping.Count;

            slot.Mapping.Clear();

            int totalsize = 0;
            await mySemaphoreSlim.WaitAsync();
            try
            {
                for (ushort subindex = 1; subindex <= mapping.subobjects.Count; subindex++)
                {
                    ODentry sub = mapping.Getsubobject(subindex);
                    if (sub == null)
                        continue;

                    //Decode the mapping

                    UInt32 data = await SDOClient.ReadAsync<uint>(mapping.Index, (byte)subindex);
                    if (data == 0)
                        continue;

                    byte datasize = (byte)(data & 0x000000FF);
                    UInt16 pdoindex = (UInt16)((data >> 16) & 0x0000FFFF);
                    byte pdosub = (byte)((data >> 8) & 0x000000FF);

                    totalsize += datasize;

                    //Console.WriteLine(string.Format("Mapping 0x{0:x4}/{1:x2} size {2}", pdoindex, pdosub, datasize));

                    //validate this against what is in the actual object mapped

                    ODentry maptarget;
                    if (pdosub == 0)
                    {
                        if (EdsInfo.tryGetODEntry(pdoindex, out maptarget) == false)
                        {
                            continue;
                            /*
                            Console.WriteLine("MAPPING FAILED");
                            //Critical PDO error
                            return;*/
                        }
                    }
                    else
                    {
                        maptarget = EdsInfo.GetIndex(pdoindex).Getsubobject(pdosub);
                    }

                    if (/*maptarget.Disabled == false && */ datasize == (maptarget.Sizeofdatatype()))
                    {
                        //mappingfail = false;
                    }
                    else
                    {
                        continue;
                        //Console.WriteLine(String.Format("MAPPING FAILED {0} != {1}", datasize, maptarget.Sizeofdatatype()));
                    }

                    slot.Mapping.Add(maptarget);
                }
            }
            finally
            {
                mySemaphoreSlim.Release();
            }

            // add empty values
            for (var i = totalsize; i <= append_to; ++i)
            {
                slot.Mapping.Add(new ODentry($"Mapped object {i:x}", slot.MappingIndex, (byte)i)
                {
                    datatype = DataType.UNSIGNED32,
                    defaultvalue = "0x00000000"
                });
            }
        }
    }
}