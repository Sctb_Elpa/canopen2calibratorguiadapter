﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using CANopen2CalibratorGUIAdapter.CANOpenClient.SDO;
using CANopen2CalibratorGUIAdapter.Models.TPDO;
using libEDSsharp;
using Microsoft.Extensions.Logging;
using Utils;
using Utils.OD;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.PDO
{
    public class TPDOManager : IDisposable
    {
        #region Fields

        public static readonly SortedDictionary<byte, string> TransferTypeNames = new SortedDictionary<byte, string>
        {
            {0,  "[0] Синхронный, не цикличный"},
            {1,  "[1] Каждый SYNC"},
            {254, "[254] По таймеру" },
            {255, "[255] По таймеру" },
        };

        public readonly IBackendClient Backend;
        private readonly byte NodeId;
        public readonly EdsInfo Eds;
        private const byte cobIDSubindex = 1;
        private const byte InhibitSubindex = 3;
        private const byte IventTimerSubindex = 5;
        private const byte SyncStartSubindex = 6;
        private const byte TransferTypeSubindex = 2;
        private readonly PDOHelper Halper;
        private readonly AbstractNodeSDOClient SDOClient;
        private readonly PDOAccessor TPDOAccessor;

        private ILogger _logger = Log.CreateLogger<TPDOManager>();
        private ConcurrentBag<CancellationTokenSource> cancellationTokens = new ConcurrentBag<CancellationTokenSource>();

        private Dictionary<ODentry, DateTime> PDORxTimestamps = new Dictionary<ODentry, DateTime>();

        #endregion Fields

        #region Delegates

        public delegate void PDOHandler(object sender, byte nodeId, uint cobId, KeyValuePair<global::Utils.OD.ODIndex, object> resault_pair);

        #endregion Delegates

        #region Events

        public event PDOHandler OnPDO;

        #endregion Events

        #region Constructors

        static TPDOManager()
        {
            for (byte i = 2; i <= 240; ++i)
            {
                TransferTypeNames.Add(i, $"[{i}] Каждые {i} SYNC'а(-ов)");
            }
        }

        public TPDOManager(byte nodeId, EdsInfo eds, AbstractNodeSDOClient SDOClient, IBackendClient Backend)
        {
            NodeId = nodeId;
            Eds = eds;
            Halper = eds.CreatePDOHalper();
            this.SDOClient = SDOClient;
            this.Backend = Backend;
            TPDOAccessor = new PDOAccessor(SDOClient, eds)
            {
                Pdoslots = Halper.pdoslots.Where(slot => slot.isTXPDO()).ToList()
            };
        }

        #endregion Constructors

        #region Methods

        public bool IsMapedAndReady(ODentry entry)
        {
            if (!PDORxTimestamps.Keys.Contains(entry))
            {
                return false;
            }
            var data_avalable = (DateTime.Now - PDORxTimestamps[entry]).TotalSeconds < 10;

            if (!data_avalable)
            {
                PDORxTimestamps.Remove(entry);
            }

            return data_avalable;
        }

        public static string TranferTypeToString(byte TransferTypeId) => TransferTypeNames[TransferTypeId];

        public void Dispose()
        {
            CancelAllPDOListeners();
        }

        public void GenerateMapping(ushort mappingIndex, IEnumerable<ODIndex> mapping)
        {
            void RestoreZeroMappings(IList<PDOSlot> slots, IList<ushort> append_to)
            {
                foreach (var (slot, ap_to) in slots.Zip(append_to))
                {
                    var map_index = Eds.GetIndex(slot.MappingIndex);
                    for (var i = (ushort)(slot.Mapping.Count + 1); i <= ap_to; ++i)
                    {
                        map_index.subobjects.Add(i,
                            new ODentry($"Mapped object {i:x}", slot.MappingIndex, (byte)i)
                            {
                                datatype = DataType.UNSIGNED32,
                                defaultvalue = "0x00000000"
                            });
                    }
                }
            }

            var slot = TPDOAccessor.Pdoslots.Find(s => s.MappingIndex == mappingIndex);
            var oldmapping = slot.Mapping;

            slot.Mapping = new List<ODentry>();

            try
            {
                mapping.WithIndex().ForEach(
                    element => slot.insertMapping(element.index, FindODentryByIndex(element.item))
                );

                var append_to = Halper.pdoslots.Select(
                    (slot) => (ushort)GetMappingSize(slot.MappingIndex)
                ).ToList();
                Halper.buildmappingsfromlists();

                // we mast restore all mappings after Halper.buildmappingsfromlists()
                RestoreZeroMappings(Halper.pdoslots, append_to);
            }
            catch (Exception ex)
            {
                // restore
                slot.Mapping = oldmapping;
                throw ex;
            }
        }

        public async Task<List<ODentry>> GetMapInfo(int slot)
        {
            await TPDOAccessor.LoadMapping(slot);
            return TPDOAccessor.Pdoslots[slot].Mapping;
        }

        public int GetMappingSize(ushort mappingIndex)
        {
            var count = Eds.GetIndex(mappingIndex).subobjects.Count();
            return count - 1;
        }

        public IList<uint> MappingValues(ushort mappingIndex)
            => Eds.GetIndex(mappingIndex).subobjects.Select(
                obj => EDSsharp.ConvertToUInt32(obj.Value.defaultvalue)).ToList();

        public async Task<IEnumerable<PDOSlot>> TPDOConfig()
        {
            await TPDOAccessor.LoadConfig();
            return TPDOAccessor.Pdoslots;
        }

        public IEnumerable<ODentry> TPDOMapable()
            => (from index in Eds.Indexes()
                where index.PDOMapping
                select index)
                .Concat(
                from index in Eds.Indexes()
                from subindex in index.subobjects.Values
                where subindex.PDOMapping
                select subindex
            );

        public async Task UpdateSubscription()
        {
            var _this = this;

            CancelAllPDOListeners();

            await TPDOAccessor.LoadConfig();
            foreach (var (slot, index) in TPDOAccessor.Pdoslots.WithIndex())
            {
                if (!slot.invalid)
                {
                    await TPDOAccessor.LoadMapping(index);

                    var cts = new CancellationTokenSource();
                    cancellationTokens.Add(cts);

                    var cob_id = slot.COB;
                    _logger.LogDebug($"Subscribing to PDO witn COB-ID={cob_id}, node-id={NodeId}...");

                    _ = Task.Run(async () =>
                        await Backend.SubscribeTPO(
                            CobId: cob_id,
                            callback: (cobid, data) => PDORessived(_this, slot, cobid, data),
                            CancellationToken: cts.Token)
                    );
                }
            }
        }

        public async Task WriteConfig(ushort configIndex, TPDOSlotConfig config)
        {
            // first, disable config
            var oldval = await SDOClient.ReadAsync<uint>(configIndex, cobIDSubindex);
            await SDOClient.WriteAsync(configIndex, cobIDSubindex, oldval | 0x80000000);

            // write config
            await SDOClient.WriteAsync(configIndex, TransferTypeSubindex, config.TransferTypeID);
            await SDOClient.WriteAsync(configIndex, InhibitSubindex, config.Inhibit);
            await SDOClient.WriteAsync(configIndex, IventTimerSubindex, config.IventTimer);
            await SDOClient.WriteAsync(configIndex, SyncStartSubindex, config.SyncStart);

            // enable, if needed
            if (config.Enabled)
            {
                await SDOClient.WriteAsync(configIndex, cobIDSubindex, config.CobId & ~0x80000000);
            }
        }

        public async Task Control(ushort configIndex, bool enable)
        {
            var oldval = await SDOClient.ReadAsync<uint>(configIndex, cobIDSubindex);
            var newval = enable ? oldval & ~0x80000000 : oldval | 0x80000000;
            await SDOClient.WriteAsync(configIndex, cobIDSubindex, newval);
        }

        public async Task WriteMapping(ushort mappingIndex)
        {
            var mapping_values = MappingValues(mappingIndex);

            // first, needs to disable current mapping
            await SDOClient.WriteAsync<byte>(mappingIndex, 0, 0);
            foreach (var (item, subindex) in mapping_values.WithIndex())
            {
                if (subindex > 0) // subindex 0 write last
                {
                    // write mapping
                    await SDOClient.WriteAsync(mappingIndex, (byte)subindex, item);
                }
            }
            // accept, set mapping count
            await SDOClient.WriteAsync(mappingIndex, 0, (byte)mapping_values.First());
        }

        public void CancelAllPDOListeners()
        {
            while (cancellationTokens.TryTake(out CancellationTokenSource cts))
            {
                cts.Cancel();
            }
        }

        private ODentry FindODentryByIndex(ODIndex index)
        {
            var res = Eds.GetIndex(index.Index)?.Getsubobject(index.Subindex);
            if (res == null)
            {
                throw new Exception($"Index \"{index}\" is not in Object dictionary");
            }
            if (res.PDOtype != PDOMappingType.optional && res.PDOtype != PDOMappingType.TPDO)
            {
                throw new Exception($"Index \"{index}\" can't be mapped to TPDO");
            }

            return res;
        }

        private static Task PDORessived(TPDOManager _this, PDOSlot slot, uint CobId, byte[] data)
        {
            var decoded_values = slot.DecodeData(data);

            _this._logger.LogDebug($"PDO With COB-ID={CobId}:\n" +
                string.Join('\n', decoded_values.Select(v => $"\t{v.Key} = {v.Value}"))
            );

            if (_this.OnPDO != null)
            {
                decoded_values.ForEach(v => {
                    var index = _this.Eds.GetIndex(v.Key.Index, v.Key.Subindex);
                    if (index != null) {
                        _this.SetProperStringValue(index, v.Value);

                        _this.PDORxTimestamps[index] = DateTime.Now;
                    }
                    _this.OnPDO.Invoke(_this, _this.NodeId, CobId, v);
                 });
            }

            return Task.CompletedTask;
        }

        private void SetProperStringValue(ODentry oDentry, object value)
        {
            if (value is string s)
            {
                oDentry.actualvalue = s;
            }
            else
            {
                string v = oDentry.datatype switch
                {
                    DataType.BOOLEAN => ((bool)value).ToString(CultureInfo.InvariantCulture),
                    DataType.INTEGER8 => ((sbyte)value).ToString(),
                    DataType.INTEGER16 => ((short)value).ToString(),
                    DataType.INTEGER32 => ((int)value).ToString(),
                    DataType.UNSIGNED8 => ((byte)value).ToString(),
                    DataType.UNSIGNED16 => ((ushort)value).ToString(),
                    DataType.UNSIGNED32 => ((uint)value).ToString(),
                    DataType.REAL32 => ((float)value).ToString(CultureInfo.InvariantCulture),
                    DataType.REAL64 => ((double)value).ToString(CultureInfo.InvariantCulture),
                    DataType.INTEGER64 => ((long)value).ToString(),
                    DataType.UNSIGNED64 => ((ulong)value).ToString(),
                    DataType.PDO_COMMUNICATION_PARAMETER => ((uint)value).ToString(),
                    DataType.PDO_MAPPING => ((uint)value).ToString(),

                    _ => $"{oDentry.datatype} not supported"
                };

                oDentry.actualvalue = v;
            }
        }

        #endregion Methods
    }
}