﻿using System.IO;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.Common
{
	public class BackendCanError : IOException
	{
		public BackendCanError(string err) : base(err) { }
	}
}