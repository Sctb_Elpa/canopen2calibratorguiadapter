﻿using System;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient.SDO;
using Utils;
using Utils.OD;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.Common
{
    public class BootloaderControllerV1 : IBootloaderController
    {
        #region Fields

        public static readonly ODIndex EncryptionMethodIndex = new ODIndex() { Index = 0x1F55, Subindex = 1 };
        public static readonly ODIndex IdentifiesTheApplocationIndex = new ODIndex() { Index = 0x1F56, Subindex = 1 };
        public static readonly ODIndex ProgramControlIndex = new ODIndex() { Index = 0x1F51, Subindex = 1 };
        public static readonly ODIndex ProgramDataIndex = new ODIndex() { Index = 0x1F50, Subindex = 1 };
        public static readonly ODIndex SettingsBackupIndex = new ODIndex() { Index = 0x1F5A, Subindex = 1 };

        protected readonly byte Node_id;
        protected readonly AbstractSDOClient SdoClient;

        #endregion Fields

        #region Constructors

        public BootloaderControllerV1(AbstractSDOClient sdoClient, byte node_id, Version version)
        {
            SdoClient = sdoClient;
            Node_id = node_id;
            Version = version;
        }

        #endregion Constructors

        #region Properties

        public Version Version { get; private set; }

        public virtual Task<bool> IsDuplicationSupported()
        {
            return Task.FromResult(false);
        }

        public async Task<Result<byte[]>> BackupSettings()
        {
            try
            {
                var result = await SdoClient.ReadBytesAsync(Node_id, SettingsBackupIndex);
                return Result.Ok(result);
            }
            catch (BackendCanError ex)
            {
                return Result.Fail<byte[]>(ex.Message);
            }
        }

        #endregion Properties

        #region Methods

        public async Task<string> GetApplicationIdentification()
        {
            try
            {
                uint value = await ReadIdentification();
                return $"0x{value:X8}";
            }
            catch (BackendCanError ex)
            {
                return $"Ошибка {ex.Message}";
            }
        }

        public async Task<bool> GetIsApplicationValid()
        {
            try
            {
                uint value = await ReadIdentification();
                return value != 0;
            }
            catch (BackendCanError)
            {
                return false;
            }
        }

        public async Task RestoreSettings(byte[] data) 
            => await SdoClient.WriteBytesAsync(Node_id, SettingsBackupIndex, data);

        public async Task StartApplication()
            => await SdoClient.WriteAsync<byte>(Node_id, ProgramControlIndex, 1);

        public async Task UploadFirmwareImage(byte[] firmwareData, bool IsEncrypted)
        {
            await SdoClient.WriteAsync(Node_id, EncryptionMethodIndex, IsEncrypted ? 1u : 0u);
            await SdoClient.WriteBytesAsync(Node_id, ProgramDataIndex, firmwareData);
        }

        private async Task<uint> ReadIdentification()
            => await SdoClient.ReadAsync<uint>(Node_id, IdentifiesTheApplocationIndex);

        public virtual Task StartReservKit() {
            throw new NotSupportedException();
        }

        #endregion Methods
    }
}