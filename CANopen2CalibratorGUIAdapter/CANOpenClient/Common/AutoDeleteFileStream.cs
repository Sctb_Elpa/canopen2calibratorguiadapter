﻿using System.IO;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.Common
{
	public class AutoDeleteFileStream : FileStream
	{
		public AutoDeleteFileStream(string path) : base(path, FileMode.Open, FileAccess.Read) { }

		public new void Dispose()
		{
			var f = Name;

			base.Dispose();

			if (File.Exists(f)) {
				File.Delete(f);
			}
		}
	}
}
