using System;
using System.Threading.Tasks;
using Utils;
using Utils.OD;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.Common
{
    public class BootloaderControllerV2 : BootloaderControllerV1
    {
        #region Fields

        public static readonly ODIndex ReservControlNumberOfElementsIndex 
            = new ODIndex() { Index = 0x1F52, Subindex = 0 };
        public static readonly ODIndex ReservControlIndex = new ODIndex() { Index = 0x1F52, Subindex = 1 };

        #endregion Fields

        #region Constructors

        public BootloaderControllerV2(AbstractSDOClient sdoClient, byte node_id, Version version)
            : base(sdoClient, node_id, version)
        {
        }

        #endregion Constructors

        #region Methods
        public async override Task<bool> IsDuplicationSupported()
        {
            try
            {
                _ = await SdoClient.ReadBytesAsync(Node_id, ReservControlNumberOfElementsIndex);
                return true;
            }
            catch (BackendCanError) {
                return false;
            }
        }

        public override Task StartReservKit() {
            return SdoClient.WriteAsync(Node_id, ReservControlIndex, (byte)1);
        }

        #endregion Methods
    }
}