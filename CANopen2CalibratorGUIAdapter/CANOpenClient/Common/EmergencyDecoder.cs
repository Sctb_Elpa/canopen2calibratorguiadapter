﻿using System.Collections.Generic;
using System.Linq;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.Common
{
    public static class EmergencyDecoder
    {
        private class EmergencyType
        {
            public readonly ushort ErrorClass;
            public readonly ushort ErrorClassMask;
            public readonly string Description;

            public EmergencyType(ushort ErrorClass, ushort ErrorClassMask = 0xFFFF, string Description = "UNKNOWN")
            {
                this.ErrorClass = ErrorClass;
                this.ErrorClassMask = ErrorClassMask;
                this.Description = Description;
            }
        }

        private static readonly EmergencyType[] ErrorTypes = new EmergencyType[] {
            new EmergencyType(0x0000, Description: "error Reset or No Error"),
            new EmergencyType(0x1000, 0xFF00, Description: "Generic Error"),
            new EmergencyType(0x2000, 0xFF00, Description: "Current"),
            new EmergencyType(0x2100, 0xFF00, Description: "Current, device input side"),
            new EmergencyType(0x2200, 0xFF00, Description: "Current inside the device"),
            new EmergencyType(0x2300, 0xFF00, Description: "Current, device output side"),
            new EmergencyType(0x3000, 0xFF00, Description: "Voltage"),
            new EmergencyType(0x3100, 0xFF00, Description: "Mains Voltage"),
            new EmergencyType(0x3200, 0xFF00, Description: "Voltage inside the device"),
            new EmergencyType(0x3300, 0xFF00, Description: "Output Voltage"),
            new EmergencyType(0x4000, 0xFF00, Description: "Temperature"),
            new EmergencyType(0x4100, 0xFF00, Description: "Ambient Temperature"),
            new EmergencyType(0x4200, 0xFF00, Description: "Device Temperature"),
            new EmergencyType(0x5000, 0xFF00, Description: "Device Hardware"),
            new EmergencyType(0x6000, 0xFF00, Description: "Device Software"),
            new EmergencyType(0x6100, 0xFF00, Description: "Internal Software"),
            new EmergencyType(0x6200, 0xFF00, Description: "User Software"),
            new EmergencyType(0x6300, 0xFF00, Description: "Data Set"),
            new EmergencyType(0x7000, 0xFF00, Description: "Additional Modules"),
            new EmergencyType(0x8000, 0xFF00, Description: "Monitoring"),
            new EmergencyType(0x8100, 0xFF00, Description: "Communication"),
            new EmergencyType(0x8110, Description: "CAN Overrun (Objects lost)"),
            new EmergencyType(0x8120, Description: "CAN in Error Passive Mode"),
            new EmergencyType(0x8130, Description: "Life Guard Error or Heartbeat Error"),
            new EmergencyType(0x8140, Description: "recovered from bus off"),
            new EmergencyType(0x8150, Description: "CAN-ID collision"),
            new EmergencyType(0x8200, 0xFF00, Description: "Protocol Error"),
            new EmergencyType(0x8210, Description: "PDO not processed due to length error"),
            new EmergencyType(0x8220, Description: "PDO length exceeded"),
            new EmergencyType(0x8230, Description: "DAM MPDO not processed, destination object not available"),
            new EmergencyType(0x8240, Description: "Unexpected SYNC data length"),
            new EmergencyType(0x8250, Description: "RPDO timeout"),
            new EmergencyType(0x8260, Description: "Unexpected TIME data length"),
            new EmergencyType(0x9000, 0xFF00, Description: "External Error"),
            new EmergencyType(0xF000, 0xFF00, Description: "Additional Functions"),
            new EmergencyType(0xFF00, 0xFF00, Description: "Device specific"),
            new EmergencyType(0x2310, Description: "DS401, Current at outputs too high (overload)"),
            new EmergencyType(0x2320, Description: "DS401, Short circuit at outputs"),
            new EmergencyType(0x2330, Description: "DS401, Load dump at outputs"),
            new EmergencyType(0x3110, Description: "DS401, Input voltage too high"),
            new EmergencyType(0x3120, Description: "DS401, Input voltage too low"),
            new EmergencyType(0x3210, Description: "DS401, Internal voltage too high"),
            new EmergencyType(0x3220, Description: "DS401, Internal voltage too low"),
            new EmergencyType(0x3310, Description: "DS401, Output voltage too high"),
            new EmergencyType(0x3320, Description: "DS401, Output voltage too low"),
        };

        public static string GetErrorDescription(ushort ErrorClass)
        {
            var res = (from t in ErrorTypes
                       let item = Difference(t, ErrorClass)
                       orderby System.Math.Abs(item.Key)
                       select item).FirstOrDefault();
            return res.Key == 0 ? res.Value : $"0x{ErrorClass:X}";
        }

        private static KeyValuePair<int, string> Difference(EmergencyType compObj, ushort errorClass)
        {
            var diff = (errorClass & compObj.ErrorClassMask).CompareTo(compObj.ErrorClass);
            return new KeyValuePair<int, string>(diff, compObj.ErrorClassMask == 0xffff
                ? compObj.Description
                : compObj.Description + $" [+{errorClass & ~compObj.ErrorClassMask}]");
        }
    }
}