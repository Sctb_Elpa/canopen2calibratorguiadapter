﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient.PDO;
using CANopen2CalibratorGUIAdapter.CANOpenClient.SDO;
using CANopen2CalibratorGUIAdapter.Utils;
using Utils.LSS;
using Utils.NMT;
using Utils.OD;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.Common
{
    public class DeviceState : IDisposable
    {
        #region Properties

        public IBootloaderController BootloaderController { get; set; }
        public EdsInfo Eds { get; set; }
        public bool IsKnownDevice => Eds != null;
        public DateTime LastHBTimestamp { get; set; }
        public UniqueLSSId LSSId { get; set; }
        public byte NodeID { get; set; }
        public AbstractNodeSDOClient SDOClient { get; set; }
        public NMTState State { get; set; }
        public TPDOManager TPDO { get; private set; }

        private Dictionary<string, object> PDOData = new Dictionary<string, object>();
        private DateTime pdo_timestamp;

        #endregion Properties

        #region Methods

        public void ConfigureTPDOManager(IBackendClient backend)
        {
            if (TPDO != null)
            {
                TPDO.Dispose();
                TPDO = null;
            }
            if (Eds != null)
            {
                TPDO = new TPDOManager(NodeID, Eds, SDOClient, backend);
                if (State == NMTState.Operatuional)
                {
                    var tpdo = TPDO;
                    Task.Run(async () =>
                    {
                        await Task.Delay(100);
                        await tpdo.UpdateSubscription();
                    });
                }
            }
        }

        public void Dispose() => TPDO?.Dispose();

        public string GetProductString()
        {
            if (Eds != null)
            {
                return Eds.ProductName;
            }

            var product_string = IDDB.GetDeviceNameByID(LSSId.VendorID, LSSId.ProductID);
            return !string.IsNullOrEmpty(product_string) ? product_string : $"0x{LSSId.ProductID:X8}";
        }

        public string GetVendorString()
        {
            if (Eds != null)
            {
                return Eds.VendorName;
            }

            var vendor_info = IDDB.GetVendorByID(LSSId.VendorID);
            return vendor_info.Valid ? vendor_info.ToString() : $"0x{LSSId.VendorID:X8}";
        }

        public object GetLastPDOData(ODIndex index)
        {
            if (PDOData.TryGetValue(index.ToString(), out object value))
            {
                return value;
            }
            else
            {
                return null;
            }
        }

        public void UpdatePDOData(DateTime timestamp, KeyValuePair<ODIndex, object> data)
        {
            PDOData[data.Key.ToString()] = data.Value;
            pdo_timestamp = timestamp;
        }

        #endregion Methods
    }
}