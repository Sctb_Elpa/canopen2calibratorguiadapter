﻿using System;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.Common
{
    public static class ByteDumpConverter
    {
        public static string ToDumpString(this byte[] input) => BitConverter.ToString(input);

        public static byte[] ParceDumpString(this string s)
        {
            int length = (s.Length + 1) / 3;
            byte[] arr1 = new byte[length];
            for (int i = 0; i < length; i++)
            {
                arr1[i] = Convert.ToByte(s.Substring(3 * i, 2), 16);
            }
            return arr1;
        }
    }
}
