﻿using System;
using System.Threading.Tasks;
using Utils;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.Common
{
    public interface IBootloaderController
    {
        Version Version { get; }

        Task<bool> IsDuplicationSupported();

        Task<string> GetApplicationIdentification();

        Task<bool> GetIsApplicationValid();

        Task StartApplication();

        Task<Result<byte[]>> BackupSettings();

        Task RestoreSettings(byte[] data);

        Task UploadFirmwareImage(byte[] firmwareData, bool IsEncrypted);

        Task StartReservKit();
    }
}