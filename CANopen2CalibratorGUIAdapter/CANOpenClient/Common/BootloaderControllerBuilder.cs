﻿using System;
using System.Threading.Tasks;
using Utils;
using Utils.OD;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.Common
{
	public class BootloaderControllerBuilder
	{
		#region Fields

		public static readonly ODIndex DeviceTypeIndex = new ODIndex() { Index = 0x1000 };

		public static readonly ODIndex ManufacturerSoftwareVersionIndex = new ODIndex() { Index = 0x100A };

		public static readonly ODIndex ProgrammDataIndex = new ODIndex() { Index = 0x1F50, Subindex = 0 };

		public static readonly uint BootloaderDeviceType = 1;

		#endregion Fields

		#region Methods

		public static async Task<IBootloaderController> Build(AbstractSDOClient sdoClient, byte node_id)
		{
			if (!(await EnsureIsbootloader(sdoClient, node_id)))
			{
				return null;
			}

			Version version;
			try
			{
				var v_str = await sdoClient.ReadUTF8String(node_id, ManufacturerSoftwareVersionIndex);
				version = new Version(v_str);
			}
			catch (Exception)
			{
				return null;
			}

			if (version.Major >= 2)
			{ 
				return new BootloaderControllerV2(sdoClient, node_id, version); 
			} else if (version.Major == 1) {
				return new BootloaderControllerV1(sdoClient, node_id, version); 
			} else {
				return null;
			}
        }

		private static async Task<bool> EnsureIsbootloader(AbstractSDOClient sdoClient, byte node_id)
		{
			try
			{
				if (await sdoClient.ReadAsync<uint>(node_id, DeviceTypeIndex) != BootloaderDeviceType)
				{
					return false;
				}

				return await sdoClient.ReadAsync<byte>(node_id, ProgrammDataIndex) >= 0;
			}
			catch (BackendCanError)
			{
				return false;
			}
		}

		#endregion Methods
	}
}