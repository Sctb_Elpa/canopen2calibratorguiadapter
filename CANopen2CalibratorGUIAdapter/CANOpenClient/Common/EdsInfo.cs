﻿using CANopen2CalibratorGUIAdapter.CANOpenClient.SDO;
using CANopen2CalibratorGUIAdapter.Utils;
using libEDSsharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;
using Utils.LSS;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient.Common
{
	public class EdsInfo
	{
		#region Fields

		private EDSsharp edsSharpObject = new EDSsharp();

		#endregion Fields

		#region Properties

		public string ProductName => edsSharpObject.di.ProductName;
		public string VendorName => edsSharpObject.di.VendorName;
		public string FileName => edsSharpObject.fi.FileName;
		public int MondatoryObjectsCount => edsSharpObject.md.objectlist.Count;
		public int OptionalObjectsCount => edsSharpObject.oo.objectlist.Count;
		public int ManufacturerObjectsCount => edsSharpObject.mo.objectlist.Count;
		public object NofTPDO => edsSharpObject.di.NrOfTXPDO;

		#endregion Properties

		#region Methods


		public static Result<EdsInfo> LoadFileByExt(string filename, string extension = null)
		{
			if (extension == null)
			{
				extension = Path.GetExtension(filename).ToLower();
			}
			var res = new EdsInfo();
            switch (extension)
			{
				case ".eds":case ".dcf":
					try
					{
						res.edsSharpObject.Loadfile(filename);
						return Result.Ok(res);
					}
					catch (Exception ex)
					{
						return Result.Fail<EdsInfo>(ex.Message);
					}
					break;
				case ".xdd":
					try
					{
                        var reader = new CanOpenXDD_1_1();
                        res.edsSharpObject = reader.ReadXML(filename);
                        return Result.Ok(res);
                    }
                    catch (Exception ex)
                    {
                        return Result.Fail<EdsInfo>(ex.Message);
                    }
					break;
                default:
					return Result.Fail<EdsInfo>($"Unsupported file type: \"{extension}\"");
			}
        }

        public static async Task<Result<EdsInfo>> TryLoad(UniqueLSSId LssID, AbstractSDOClient sdo, byte nodeID)
		{
			var detector = IDDB.TryGetEdsDetectorByDeviceID(LssID);
			if (detector.Failure)
			{
				return Result.Fail<EdsInfo>("File not found");
			}

			Result<string> file = await detector.Value.FindSpecificEds(sdo, nodeID);

			if (file.Failure)
			{
				return Result.Fail<EdsInfo>("File not found");
			}

			var file_with_path = IDDB.GetEdsFile(file.Value);

			return LoadFileByExt(file_with_path);
        }

		public IEnumerable<ODentry> Indexes() => edsSharpObject.ods.Values;

		public ODentry GetIndex(ushort index) => edsSharpObject.Getobject(index);

		public ODentry Getobject(ushort v) => edsSharpObject.Getobject(v);

		public static Result<EdsInfo> TryLoad(Stream edsstream)
		{
			string tempfile = Path.GetTempFileName();
			using (FileStream outputFileStream = new FileStream(tempfile, FileMode.Create))
			{
				edsstream.CopyTo(outputFileStream);
			}

			// detect if xml
			string ext;
			using (StreamReader edsfile = new StreamReader(tempfile))
            {
				var first_string = edsfile.ReadLine();
				if (first_string.Contains("xml")) {
					ext = ".xdd";
				} else
				{
					ext = ".eds";
				}
            }

            var res = LoadFileByExt(tempfile, ext);
            File.Delete(tempfile);
            return res;
		}

		public Stream DownloadStream()
		{
			var tempfile = string.IsNullOrEmpty(edsSharpObject.fi.FileName)
				? Path.GetTempFileName()
				: Path.Combine(Path.GetTempPath(), edsSharpObject.fi.FileName);

			edsSharpObject.Savefile(tempfile, InfoSection.Filetype.File_EDS);

			return new AutoDeleteFileStream(tempfile);
		}

		public bool tryGetODEntry(ushort pdoindex, out ODentry maptarget)
			=> edsSharpObject.tryGetODEntry(pdoindex, out maptarget);

		public PDOHelper CreatePDOHalper()
		{
			var res = new PDOHelper(edsSharpObject);
			res.build_PDOlists();
			return res;
		}

		public ODentry GetIndex(ushort index, byte subindex)
			=> (subindex == 0)
				? GetIndex(index)
				: GetIndex(index).Getsubobject(subindex);

		#endregion Methods
	}
}