﻿using System;
using System.Collections.Generic;
using System.Threading;
using CANopen2CalibratorGUIAdapter.Models;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient
{
    public abstract class AbstractEmergencyLog
    {
        #region Delegates

        public delegate void EmergencyEventHandler(object sender, EmergencyLogItem ev);

        #endregion Delegates

        #region Events

        public event EmergencyEventHandler OnEMCY;

        #endregion Events

        #region Properties

        public abstract CancellationToken CancellationToken { get; }

        public abstract IList<EmergencyLogItem> Messages { get; }

        #endregion Properties

        #region Methods

        public abstract void EMCYRessived(byte nodeId, DateTime dateTime, 
            uint errorRegister, uint emcyErrorCode, byte[] manufacturerSec);

        protected void FireEMCYEvent(EmergencyLogItem item) => OnEMCY?.Invoke(this, item);

        public abstract EmergencyLogItem GetMessage(int id);

        public abstract void Clear();

        #endregion Methods
    }
}