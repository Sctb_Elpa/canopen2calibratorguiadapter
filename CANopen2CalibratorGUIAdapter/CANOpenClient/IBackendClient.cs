﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.Models;
using Utils;
using Utils.LSS;
using Utils.NMT;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient
{
    public interface IBackendClient
    {
        #region Properties

        bool Connected { get; }
        int? ConnectionID { get; }
        DeviceDB Devices { get; }
        int? SpeedID { get; }
        bool SpeedSetupSupported { get; }

        #endregion Properties

        #region Methods

        Task<Result> ConnectAsync(int canConnection, int speed);

        Task<Result> DisconnectAsync();

        IDictionary<int, string> GetCanBusList();

        Task<Result<SYNCConfig>> GetSyncConfig();

        Task<IDictionary<int, string>> GetCanBusListAsync();

        AbstractEmergencyLog GetEmergencyLog();

        Task<Result> SetSyncConfig(SYNCConfig data);

        Task<Result> NMTCommand(byte nodeID, NMTCommandCode nMTCommandCode);

        Task<Result> LSSSwitchStateGlobal(bool ConfigurationState = false);

        Task<bool> IsNodeIDAvalaible(byte nodeID);

        Task StartFastScan(UniqueLSSId initialLssId, IFastScanReportListener progressListener);

        Task<Result<byte>> AssignNodeID(UniqueLSSId lss_id, byte? nodeID);

        Task<Result> LSSConfigureNodeBitrate(byte speedGrade);

        Task<Result> LSSStoreConfiguration();

        Task<Result> LSSSApplyBitrate();

        Task<Result> ChangeNetworkSpeed(byte speedGrade);

        Task SubscribeTPO(uint CobId, Func<ushort, byte[], Task> callback, CancellationToken CancellationToken);

        #endregion Methods
    }
}