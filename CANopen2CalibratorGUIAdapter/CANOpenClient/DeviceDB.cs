﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using CANopen2CalibratorGUIAdapter.CANOpenClient.SDO;
using Utils;
using Utils.NMT;

using ODIndex = Utils.OD.ODIndex;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient
{
    public class DeviceDB : IHeartbeatMonitor, IDisposable
    {
        #region Fields

        private readonly CancellationTokenSource cts = new CancellationTokenSource();

        private readonly ConcurrentDictionary<byte, DeviceState> Nodes = new ConcurrentDictionary<byte, DeviceState>();

        #endregion Fields

        #region Delegates

        public delegate void NodeEventDelegate(object sender, byte updatedNodeID, NMTState state);

        public delegate void PDORventDelegate(object sender, DateTime timestamp, byte nodeId, uint cobId,
            KeyValuePair<ODIndex, object> resault_pair);

        #endregion Delegates

        #region Events

        public event NodeEventDelegate OnNodeHeartbeat;

        public event NodeEventDelegate OnNodeStateChanged;

        public event PDORventDelegate OnPDORessived;

        #endregion Events

        #region Properties

        public CancellationToken CancellationToken => cts.Token;
        public AbstractSDOClient Sdoclient { get; set; } = null;

        #endregion Properties

        #region Indexers

        public DeviceState this[byte nodeID] => Nodes.GetValueOrDefault(nodeID);

        #endregion Indexers

        #region Methods

        public void Dispose()
        {
            Reset();
            cts.Cancel();
        }

        public List<DeviceState> GetAliveDeviceList() => Nodes.Select(v => v.Value).ToList();

        public async Task HBRessived(byte nodeID, NMTState state, IBackendClient backendClient, DateTime timestamp)
        {
            if (Nodes.TryGetValue(nodeID, out DeviceState devState))
            {
                if (state != NMTState.BootUp)
                {
                    // update
                    devState.LastHBTimestamp = timestamp;

                    bool state_updated = devState.State != state;

                    devState.State = state;

                    if (state_updated)
                    {
                        OnNodeStateChanged?.Invoke(this, nodeID, state);
                    }
                    else
                    {
                        OnNodeHeartbeat?.Invoke(this, nodeID, state);
                    }

                    return;
                }
                else
                {
                    // if boot up - need to create new node, so, remove existing
                    Remove(nodeID);
                }
            }

            var newNode = new DeviceState()
            {
                NodeID = nodeID,
                LastHBTimestamp = timestamp,
                State = state,
                SDOClient = new NodeSDOClient(Sdoclient, nodeID)
            };

            if (state == NMTState.BootUp)
            {
                await Task.Delay(TimeSpan.FromMilliseconds(50));
            }

            try
            {
                newNode.LSSId = await Sdoclient.ReadStandartLSSID(nodeID);

                var eds_result = await EdsInfo.TryLoad(newNode.LSSId, Sdoclient, nodeID);
                newNode.Eds = eds_result.Success ? eds_result.Value : null;
                if (newNode.Eds != null)
                {
                    newNode.ConfigureTPDOManager(backendClient);
                    newNode.TPDO.OnPDO += PDORessived;
                }

                newNode.BootloaderController = await BootloaderControllerBuilder.Build(Sdoclient, nodeID);
            }
            catch (BackendCanError)
            {
                // read failed, no new devie registed
                newNode.Dispose();
                return;
            }

            Nodes.TryAdd(nodeID, newNode);
            OnNodeStateChanged?.Invoke(this, nodeID, state);
        }

        public void Remove(byte nodeID)
        {
            if (Nodes.TryRemove(nodeID, out DeviceState n))
            {
                n?.Dispose();
            }
        }

        public void Reset()
        {
            Nodes.ForEach(n => n.Value?.Dispose());
            Nodes.Clear();
        }

        private void PDORessived(object sender, byte nodeId, uint cobId, KeyValuePair<ODIndex, object> resault_pair)
        {
            Nodes[nodeId]?.UpdatePDOData(DateTime.Now, resault_pair);
            OnPDORessived?.Invoke(this, DateTime.Now, nodeId, cobId, resault_pair);
        }

        #endregion Methods
    }
}