﻿using System.Threading.Tasks;
using Utils.LSS;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient
{
    public interface IFastScanReportListener
    {
        Task OnScanProgress(string fieldName, uint bitN);
        Task OnLssFinished(UniqueLSSId foundLssId);
        Task OnErrorMessage(string message);
    }
}