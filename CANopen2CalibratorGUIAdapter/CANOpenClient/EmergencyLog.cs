﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using CANopen2CalibratorGUIAdapter.CANOpenClient.Common;
using CANopen2CalibratorGUIAdapter.Models;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient
{
    public class EmergencyLog : AbstractEmergencyLog, IDisposable
    {
        #region Fields

        private readonly CancellationTokenSource cancellationToken = new CancellationTokenSource();
        private readonly ConcurrentBag<EmergencyLogItem> messages = new ConcurrentBag<EmergencyLogItem>();

        #endregion Fields

        #region Properties

        public override CancellationToken CancellationToken => cancellationToken.Token;
        public override IList<EmergencyLogItem> Messages => messages.OrderBy(i => i.Id).ToImmutableList();

        #endregion Properties

        #region Methods

        public override void Clear() => messages.Clear();

        public void Dispose() => cancellationToken.Cancel();

        public override void EMCYRessived(byte nodeId, DateTime Timestamp, uint errorRegister,
            uint emcyErrorCode, byte[] manufacturerSpec)
        {
            var res = new EmergencyLogItem
            {
                Id = messages.Count,
                NodeId = nodeId,
                Timestamp = Timestamp,
                ErrorRegister = errorRegister,
                EmcyErrorCode = (ushort)emcyErrorCode,
                ManufacturerSpec = manufacturerSpec.ToDumpString()
            };

            messages.Add(res);
            FireEMCYEvent(res);
        }

        public override EmergencyLogItem GetMessage(int id) => messages.Where(m => m.Id == id).First();

        #endregion Methods
    }
}