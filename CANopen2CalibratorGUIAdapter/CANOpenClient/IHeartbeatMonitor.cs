﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Utils.NMT;

namespace CANopen2CalibratorGUIAdapter.CANOpenClient
{
    public interface IHeartbeatMonitor
    {
        CancellationToken CancellationToken { get; }

        Task HBRessived(byte nodeID, NMTState state, IBackendClient backendClient, DateTime dateTime);

        void Reset();
    }
}
