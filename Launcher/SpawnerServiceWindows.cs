using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Launcher
{
    internal class SpawnerServiceWindows : AbstractSpawner, IHostedService
    {
        #region Constructors

        public SpawnerServiceWindows(Options options, ILogger<SpawnerServiceWindows> logger) : base(options, logger)
        {
            driver_port = FreePortTaker.FreeTcpPort();
        }

        #endregion Constructors

        #region Methods

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation("Starting Launcher");
            Spawn().Monitor();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.LogInformation("Stopping Launcher.");
            StopMonitoring();
            return Task.CompletedTask;
        }

        protected override Process SpawnCanDriver()
        {
            if (RemoteDriverSpecified)
            {
                return null;
            }

            var startInfo = new ProcessStartInfo
            {
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                FileName = $"../Backend/{platform_string}/BackendCanopenlib.exe",
                Arguments = $"--grpc-port {driver_port}",
                WorkingDirectory = $"../Backend/{platform_string}"
            };

            Logger.LogInformation($"Spawning backend process: {startInfo.FileName} {startInfo.Arguments}");

            return new Process{ StartInfo = startInfo };
        }

        #endregion Methods
    }
}