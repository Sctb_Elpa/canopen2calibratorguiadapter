﻿using CommandLine;
using Common;

namespace Launcher
{
    internal class Options
    {
        [Option("use-driver", Required = false, HelpText = "Use remote CANOpen driver HOST:PORT")]
        public string RemoteDriver { get; set; }

        [Option("web-port", Default = Defaults.DefaultWebPort, Required = false, HelpText = "Web interface http port")]
        public int WebPort { get; set; }

        [Option("grpc-port", Default = Defaults.DefaultgRPCPort, Required = false, HelpText = "gRPC port")]
        public int GRpcPort { get; set; }
    }
}