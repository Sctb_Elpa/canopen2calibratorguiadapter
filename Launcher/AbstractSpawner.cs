﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.Versioning;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Launcher
{
    internal abstract class AbstractSpawner : IDisposable
    {
        #region Fields

        protected int driver_port;

        protected readonly ILogger Logger;
        protected Options Options;

        protected static string platform_string;

        #endregion Fields

        #region Constructors

        static AbstractSpawner()
        {
            var framework = Assembly
                .GetEntryAssembly()?
                .GetCustomAttribute<TargetFrameworkAttribute>()?
                .FrameworkName;
            var re = new Regex("\\.NETCoreApp,Version=v(.+)");
            var m = re.Match(framework);

            if (m.Success)
            {
                platform_string = $"netcoreapp{m.Groups[1]}";
            }
            else
            {
                throw new Exception("Unknown frimework version " + framework);
            }
        }

        public AbstractSpawner(Options options, ILogger logger)
        {
            Options = options;
            Logger = logger;
        }

        #endregion Constructors

        #region Properties

        public Process AdapterProcess { get; private set; } = null;
        public Process CanDriverProcess { get; private set; } = null;
        public bool DriverSpawned { get; private set; } = false;
        protected bool RemoteDriverSpecified => !string.IsNullOrEmpty(Options.RemoteDriver);

        #endregion Properties

        #region Methods

        public void Dispose()
        {
            Logger.LogInformation("Closing remaning processes...");
            if (AdapterProcess != null && !AdapterProcess.HasExited)
            {
                AdapterProcess.Kill();
            }
            if (DriverSpawned && CanDriverProcess != null && !CanDriverProcess.HasExited)
            {
                CanDriverProcess.Kill();
            }
        }

        private void configureAndStartchildProcess(Process process, EventHandler onExit)
        {
            process.EnableRaisingEvents = true;
            process.Exited += onExit;
            process.OutputDataReceived += Childs_std_out;
            process.ErrorDataReceived += Child_std_err;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
        }

        // monitor pcocesses and relounch if crash
        public void Monitor()
        {
            Logger.LogInformation("Monitoring processes:");
            if (DriverSpawned)
            {
                configureAndStartchildProcess(CanDriverProcess, CanDriverProcess_Exited);
                Logger.LogInformation($"CanDriverProcess: {CanDriverProcess.Id}");
            }

            configureAndStartchildProcess(AdapterProcess, AdapterProcess_Exited);
            Logger.LogInformation($"AdapterProcess: {AdapterProcess.Id}");
        }

        private void Child_std_err(object sender, DataReceivedEventArgs e) 
            => Console.Error.WriteLine(e.Data);

        private void Childs_std_out(object sender, DataReceivedEventArgs e) 
            => Console.WriteLine(e.Data);

        // spawn processes
        public AbstractSpawner Spawn()
        {
            CanDriverProcess = SpawnCanDriver();
            DriverSpawned = CanDriverProcess != null;

            AdapterProcess = SpawnAdapterProcess();

            return this;
        }

        public void StopMonitoring()
        {
            if (DriverSpawned && CanDriverProcess != null && !CanDriverProcess.HasExited)
            {
                CanDriverProcess.EnableRaisingEvents = false;
                CanDriverProcess.Exited -= CanDriverProcess_Exited;
                CanDriverProcess.CancelErrorRead();
                CanDriverProcess.CancelOutputRead();
            }

            if (AdapterProcess != null && !AdapterProcess.HasExited)
            {
                AdapterProcess.EnableRaisingEvents = false;
                AdapterProcess.Exited -= AdapterProcess_Exited;
                AdapterProcess.CancelErrorRead();
                AdapterProcess.CancelOutputRead();
            }
        }

        protected Process SpawnAdapterProcess()
        {
            var remote_driver = RemoteDriverSpecified
                ? Options.RemoteDriver
                : $"localhost:{driver_port}";

            var stringbuilder = new StringBuilder();

            stringbuilder
                .Append($"--can-driver={remote_driver} ")
                .Append($"--web-port={Options.WebPort} ")
                .Append($"--grpc-port={Options.GRpcPort} ");

            var startInfo = new ProcessStartInfo
            {
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                FileName = $"../Frontend/{platform_string}/CANopen2CalibratorGUIAdapter",
                Arguments = stringbuilder.ToString(),
                WorkingDirectory = $"../Frontend/{platform_string}"
            };

            Logger.LogInformation($"Spawning frontend process: {startInfo.FileName} {startInfo.Arguments}");

            return new Process { StartInfo = startInfo };
        }

        protected abstract Process SpawnCanDriver();

        private void AdapterProcess_Exited(object obj, EventArgs ev)
        {
            if (obj is Process pr)
            {
                if (!pr.HasExited)
                {
                    pr.CancelOutputRead();
                    pr.CancelErrorRead();
                }
            }

            RunAsync(Task.Run(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(1));

                Logger.LogError($"AdapterProcess finished unexpectly ({ev}), restarting...");

                AdapterProcess = SpawnAdapterProcess();
                configureAndStartchildProcess(AdapterProcess, AdapterProcess_Exited);

                Logger.LogInformation($"New CanDriverProcess: {CanDriverProcess.Id}");
                return Task.CompletedTask;
            }));
        }

        private void CanDriverProcess_Exited(object obj, EventArgs ev)
        {
            if (obj is Process pr)
            {
                if (!pr.HasExited)
                {
                    pr.CancelOutputRead();
                    pr.CancelErrorRead();
                }
            }

            RunAsync(Task.Run(async () =>
            {
                await Task.Delay(TimeSpan.FromSeconds(1));

                Logger.LogError($"CanDriverProcess finished unexpectly ({ev}), restarting...");

                CanDriverProcess = SpawnCanDriver();
                configureAndStartchildProcess(CanDriverProcess, CanDriverProcess_Exited);

                Logger.LogInformation($"New CanDriverProcess: {CanDriverProcess.Id}");
                return Task.CompletedTask;
            }));
        }

        protected void RunAsync(Task task)
        {
            task.ContinueWith(t =>
            {
                Logger.LogError("Unexpected Error", t.Exception);
            }, TaskContinuationOptions.OnlyOnFaulted);
        }

        #endregion Methods
    }
}