﻿using System.Net;
using System.Net.Sockets;

namespace Launcher
{
    internal static class FreePortTaker
    {
        public static int FreeTcpPort()
        {
            TcpListener l = new TcpListener(IPAddress.Any, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            return port;
        }
    }
}