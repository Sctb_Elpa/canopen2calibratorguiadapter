﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using CommandLine;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Launcher
{
    internal class Program
    {
        public static async Task Main(string[] args)
        {
            await Parser.Default.ParseArguments<Options>(args).WithParsedAsync(o =>
            {
                // https://www.wintellect.com/creating-a-daemon-with-net-core-part-1/
                var builder = new HostBuilder()
                    .ConfigureAppConfiguration((hostingContext, config) =>
                    {
                        config.AddEnvironmentVariables();

                        if (args != null)
                        {
                            config.AddCommandLine(args);
                        }
                    })
                    .ConfigureServices((hostContext, services) =>
                    {
                        services.AddSingleton(o);
                        if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                        {
                            services.AddSingleton<IHostedService, SpawnerServiceLinux>();
                        }
                        else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                        {
                            services.AddSingleton<IHostedService, SpawnerServiceWindows>();
                        }
                        else
                        {
                            throw new ArgumentException($"Unknown platform {RuntimeInformation.OSDescription}!");
                        }
                    })
                    .ConfigureLogging((hostingContext, logging) =>
                    {
                        //logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                        logging.AddConsole();
                    });

                return builder.RunConsoleAsync();
            });
        }
    }
}