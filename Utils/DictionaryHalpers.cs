﻿using System;
using System.Collections.Generic;

namespace Utils
{
	public static class DictionaryHalpers
	{
		#region Methods

		public static void AddRangeOverride<TKey, TValue>(this Dictionary<TKey, TValue> dic, Dictionary<TKey, TValue> dicToAdd)
		{
			dicToAdd.ForEach(x => dic[x.Key] = x.Value);
		}

		public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
		{
			foreach (var item in source)
			{
				action(item);
			}
		}

		#endregion Methods
	}
}