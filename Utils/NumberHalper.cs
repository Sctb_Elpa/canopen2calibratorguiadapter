﻿using System.ComponentModel;

namespace Utils
{
	public static class NumberHalper
	{
		// https://stackoverflow.com/a/28541237
		public static T Number<T>(this string mystring)
		{
			var foo = TypeDescriptor.GetConverter(typeof(T));
			return (T)(foo.ConvertFromInvariantString(mystring));
		}
	}
}
