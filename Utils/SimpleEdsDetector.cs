﻿using System.Threading.Tasks;

namespace Utils
{
	internal class SimpleEdsDetector : IEdsDetector
	{
		private string filename;

		public SimpleEdsDetector(string filename)
		{ 
			this.filename = filename;
		}

		public Task<Result<string>> FindSpecificEds(AbstractSDOClient _sdo, byte _nodeID)
		{
			return Task.FromResult(Result.Ok(filename));
		}
	}
}