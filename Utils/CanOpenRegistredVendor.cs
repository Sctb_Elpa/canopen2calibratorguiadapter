﻿namespace Utils
{
	public class CanOpenRegistredVendor
	{
		#region Fields

		public string Departament;
		public uint VendorID;
		public string VendorName;

		#endregion Fields

		#region Constructors

		public CanOpenRegistredVendor()
		{
		}

		internal CanOpenRegistredVendor(uint VendorID, string VendorName, string Departament = null)
		{
			this.VendorID = VendorID;
			this.VendorName = VendorName;
			this.Departament = Departament;
		}

		public bool Valid => !string.IsNullOrEmpty(VendorName);

        #endregion Constructors

        #region Methods

        public override string ToString() 
			=> VendorName + (string.IsNullOrEmpty(Departament)
                ? ""
                : " (" + Departament + ")");

        #endregion Methods
    }
}