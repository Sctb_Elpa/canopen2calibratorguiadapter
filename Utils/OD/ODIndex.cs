﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Utils.OD
{
	public class ODIndex : IComparable<ODIndex>
	{
		#region Fields

		public ushort Index = 0;
		public byte Subindex = 0;

		#endregion Fields

		#region Constructors

		public ODIndex()
		{
		}

		public ODIndex(string str)
		{
			if (str.Contains("sub"))
			{
				var elements = str.Split(new string[] { "sub" }, StringSplitOptions.RemoveEmptyEntries);
				if (elements.Length != 2)
				{
					throw new ArgumentException("Index mast be XXXX[subYY] format");
				}

				Index = ushort.Parse(elements[0], System.Globalization.NumberStyles.HexNumber);
				Subindex = byte.Parse(elements[1]);
			}
			else
			{
				Index = ushort.Parse(str, System.Globalization.NumberStyles.HexNumber);
			}
		}

		#endregion Constructors

		#region Methods

		public int CompareTo([AllowNull] ODIndex other)
		{
			if (other == null) return 1;

			return other.Index == Index
				? Subindex.CompareTo(other.Subindex)
				: Index.CompareTo(other.Index);
		}

		public override string ToString() => $"{Index:X}sub{Subindex}";

		#endregion Methods
	}
}