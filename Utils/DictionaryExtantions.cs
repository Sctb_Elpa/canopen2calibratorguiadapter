﻿using System.Collections.Generic;

namespace Utils
{
	public static class DictionaryExtantions
	{
		#region Methods

		public static Map<T1, T2> ToMap<T1, T2>(this Dictionary<T1, T2> dict)
		{
			var result = new Map<T1, T2>();
            dict.ForEach(i => result.Add(i.Key, i.Value));
			return result;
		}

		#endregion Methods
	}
}