﻿using System.Collections.Generic;
using System.Linq;

namespace Utils
{
    public static class IEnumerableExt
    {
        // https://stackoverflow.com/a/39997157
        public static IEnumerable<(T item, int index)> WithIndex<T>(this IEnumerable<T> self)
            => self.Select((item, index) => (item, index));
    }
}
