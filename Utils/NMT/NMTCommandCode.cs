﻿namespace Utils.NMT
{
	public enum NMTCommandCode
	{
		GOTO_OPERTIONAL = 0,
		GOTO_PRE_OPERATIONAL = 1,
		GOTO_STOPPED = 2,

		RESET_NODE = 3,
		RESET_COMMUNICATION = 4,
	}
}