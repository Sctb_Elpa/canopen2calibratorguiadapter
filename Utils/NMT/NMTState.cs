﻿using System.ComponentModel;

namespace Utils.NMT
{
	public enum NMTState
	{
		[Description("Unknown")]
		Unknown = -1,

		[Description("BootUp")]
		BootUp = 0,

		[Description("Остановлен")]
		Stopped = 4,

		[Description("Работает")]
		Operatuional = 5,

		[Description("Ожидает запуска")]
		PreOperational = 0x7f
	}
}