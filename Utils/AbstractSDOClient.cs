﻿using System.Threading.Tasks;
using Utils.OD;

namespace Utils
{
    public abstract class AbstractSDOClient
    {
        #region Methods

        public abstract Task<T> ReadAsync<T>(byte nodeID, ushort index, byte subindex) where T : struct;

        public abstract Task WriteAsync<T>(byte nodeID, ushort index, byte subindex, T value) where T : struct;

        public abstract Task<byte[]> ReadBytesAsync(byte nodeID, ushort index, byte subindex);

        public abstract Task WriteBytesAsync(byte nodeID, ushort index, byte subindex, byte[] data);

        public virtual Task<T> ReadAsync<T>(byte nodeID, ODIndex index) where T : struct
            => ReadAsync<T>(nodeID, index.Index, index.Subindex);

        public virtual Task<byte[]> ReadBytesAsync(byte node_id, ODIndex index)
            => ReadBytesAsync(node_id, index.Index, index.Subindex);

        public virtual Task WriteAsync<T>(byte nodeID, ODIndex index, T v) where T : struct
            => WriteAsync(nodeID, index.Index, index.Subindex, v);

        public virtual Task WriteBytesAsync(byte node_id, ODIndex settingsBackupIndex, byte[] data)
            => WriteBytesAsync(node_id, settingsBackupIndex.Index, settingsBackupIndex.Subindex, data);

        #endregion Methods
    }
}