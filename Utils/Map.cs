﻿using System.Collections.Generic;

namespace Utils
{
	public class Map<T1, T2>
	{
		#region Fields

		private Dictionary<T1, T2> _forward = new Dictionary<T1, T2>();
		private Dictionary<T2, T1> _reverse = new Dictionary<T2, T1>();

		#endregion Fields

		#region Constructors

		public Map()
		{
			Forward = new Indexer<T1, T2>(_forward);
			Reverse = new Indexer<T2, T1>(_reverse);
		}

		#endregion Constructors

		#region Properties

		public Indexer<T1, T2> Forward { get; private set; }

		public Dictionary<T1, T2>.KeyCollection Keys => _forward.Keys;

		public Indexer<T2, T1> Reverse { get; private set; }

		public Dictionary<T1, T2>.ValueCollection Values => _forward.Values;

		#endregion Properties

		#region Methods

		public void Add(T1 t1, T2 t2)
		{
			_forward.Add(t1, t2);
			_reverse.Add(t2, t1);
		}

		public void Clear()
		{
			_forward.Clear();
			_reverse.Clear();
		}

		#endregion Methods

		#region Classes

		public class Indexer<T3, T4>
		{
			#region Fields

			private readonly Dictionary<T3, T4> _dictionary;

			#endregion Fields

			#region Constructors

			public Indexer(Dictionary<T3, T4> dictionary)
			{
				_dictionary = dictionary;
			}

			#endregion Constructors

			#region Indexers

			public T4 this[T3 index]
			{
				get { return _dictionary[index]; }
				set { _dictionary[index] = value; }
			}

			#endregion Indexers
		}

		#endregion Classes
	}
}