﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utils.OD;

namespace Utils
{
	internal class StringMapEdsDetector : IEdsDetector
	{
		public readonly ODIndex index;

		public Dictionary<string, string> Map { get; set; } = new Dictionary<string, string>();

		public StringMapEdsDetector(ODIndex index)
		{
			this.index = index;
		}

		public async Task<Result<string>> FindSpecificEds(AbstractSDOClient sdo, byte nodeID)
		{
			byte[] res;
			try
			{
				res = await sdo.ReadBytesAsync(nodeID, index);
			}
			catch (Exception e)
			{
				return Result.Fail<string>(e.Message);
			}

			var value = System.Text.Encoding.ASCII.GetString(res).Trim('\0');

			if (Map.TryGetValue(value, out var result))
			{
				return Result.Ok(result);
			}
			else
			{
				return Result.Ok(Map.Values.First());
			}
		}
	}
}