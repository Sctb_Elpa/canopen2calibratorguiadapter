﻿namespace Utils.LSS
{
	public class DeviceFeatureID
	{
		public uint VendorID;
		public uint ProductID;
		public uint? RevisionNumber;

		public DeviceFeatureID(uint VendorID, uint ProductID, uint? RevisionNumber = null)
		{
			this.VendorID = VendorID;
			this.ProductID = ProductID;
			this.RevisionNumber = RevisionNumber;
		}

		public int CompareTo(DeviceFeatureID lr)
		{
			if (VendorID != lr.VendorID || ProductID != lr.ProductID)
				return 0;

			if (!RevisionNumber.HasValue)
			{
				return 1; // неспецифицированный по ревизии похож на все но с весом 1
			}
			if (RevisionNumber.HasValue && !lr.RevisionNumber.HasValue)
			{
				return 0; // специфицированный по ревизии не похож на неспецифицированный
			}
			return RevisionNumber == lr.RevisionNumber
				? 2 : 0; // если ревизии сходятся, то вес такого равенства больше
		}
	}
}