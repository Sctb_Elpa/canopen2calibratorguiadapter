﻿using System.Threading.Tasks;

namespace Utils
{
	public interface IEdsDetector
	{
		Task<Result<string>> FindSpecificEds(AbstractSDOClient sdo, byte nodeID);
	}
}
