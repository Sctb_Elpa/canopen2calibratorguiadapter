﻿using System.Text;
using System.Threading.Tasks;
using Utils.LSS;
using Utils.OD;

namespace Utils
{
    public static class SDOClientExtentions
    {
        public static async Task<UniqueLSSId> ReadStandartLSSID(this AbstractSDOClient reader, byte nodeID)
        {
            const ushort LssIDIndex = 0x1018;

            const byte LssIDPID_SubIndex = 2;
            const byte LssIDRevicsion_SubIndex = 3;
            const byte LssIDSerial_SubIndex = 4;
            const byte LssIDVID_SubIndex = 1;

            return new UniqueLSSId
            {
                VendorID = await reader.ReadAsync<uint>(nodeID, LssIDIndex, LssIDVID_SubIndex),
                ProductID = await reader.ReadAsync<uint>(nodeID, LssIDIndex, LssIDPID_SubIndex),
                RevisionNumber = await reader.ReadAsync<uint>(nodeID, LssIDIndex, LssIDRevicsion_SubIndex),
                SerialNumber = await reader.ReadAsync<uint>(nodeID, LssIDIndex, LssIDSerial_SubIndex),
                KnownMask = UniqueLSSId.KnownBits.All
            };
        }

        public static async Task<string> ReadUTF8String(this AbstractSDOClient reader, byte nodeID, ODIndex index) 
            => Encoding.UTF8.GetString(await reader.ReadBytesAsync(nodeID, index));
    }
}