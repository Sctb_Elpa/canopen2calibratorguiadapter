[Setup]
AppName=CANopen2CalibratorGUIAdapter
AppVersion=1.1
DefaultDirName={autopf}\CANopen2CalibratorGUIAdapter
DefaultGroupName=CANopen2CalibratorGUIAdapter
WizardStyle=modern
Compression=lzma2
SolidCompression=yes
OutputDir=InnoSetup
OutputBaseFilename=CANopen2CalibratorGUIAdapter_InnoSetup
; "ArchitecturesInstallIn64BitMode=x64" requests that the install be
; done in "64-bit mode" on x64, meaning it should use the native
; 64-bit Program Files directory and the 64-bit view of the registry.
; On all other architectures it will install in "32-bit mode".
ArchitecturesInstallIn64BitMode=x64
; Note: We don't set ProcessorsAllowed because we want this
; installation to run on all architectures (including Itanium,
; since it's capable of running 32-bit code too).

[Tasks]
Name: desktopicon; Description: "Create a &Desktop icon"; Flags: unchecked
Name: startmenu; Description: "Create &Start menu icons"

[Files]
; Place all x64 files here
;Source: "MyProg-x64.exe"; DestDir: "{app}"; DestName: "MyProg.exe"; Check: Is64BitInstallMode
; Place all x86 files here, first one should be marked 'solidbreak'
;Source: "MyProg.exe"; DestDir: "{app}"; Check: not Is64BitInstallMode; Flags: solidbreak
; Place all common files here, first one should be marked 'solidbreak'
;Source: "MyProg.chm"; DestDir: "{app}"; Flags: solidbreak
;Source: "Readme.txt"; DestDir: "{app}"; Flags: isreadme

Source: "{#Src}\Backend\netcoreapp3.1\*"; Excludes: "*linux*, *osx*, *.pdb"; DestDir: "{app}\Backend\netcoreapp3.1"; Flags: ignoreversion createallsubdirs recursesubdirs;
Source: "{#Src}\Frontend\netcoreapp3.1\*"; Excludes: "*linux*, *osx*, *.pdb"; DestDir: "{app}\Frontend\netcoreapp3.1"; Flags: ignoreversion createallsubdirs recursesubdirs;
Source: "{#Src}\netcoreapp3.1\*"; DestDir: "{app}\Launcher"; Flags: ignoreversion createallsubdirs recursesubdirs;

[Icons]
Name: "{group}\CANopen2CalibratorGUIAdapter"; Filename: "{app}\Launcher\Launcher.exe"; Tasks: startmenu
Name: "{group}\Uninstall KalibratorGUI"; Filename: "{uninstallexe}"; Tasks: startmenu
Name: "{userdesktop}\CANopen2CalibratorGUIAdapter"; Filename: "{app}\Launcher\Launcher.exe"; Tasks: desktopicon